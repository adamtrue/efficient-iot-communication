package hevs.ecs.sim_com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ComSimApplication {

	public static void main(String[] args) {
        SpringApplication.run(ComSimApplication.class, args);
        ComServer server;

        // Get instance of the singleton server
        server = ComServer.getInstance();

        while(true)
        {
            try {
                server.start(23);
                while(server.connected())
                    server.readLinesFromServer();
                server.stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //----------------------------------
	    }
	}

}
