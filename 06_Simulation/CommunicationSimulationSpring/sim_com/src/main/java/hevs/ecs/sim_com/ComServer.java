package hevs.ecs.sim_com;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

// Class for simulating motor drivers, that will automatically send TCP messages
public class ComServer {

    private static ComServer single_instance;

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    private boolean connected = false;

    public static ComServer getInstance()
    {
        if(single_instance == null)
            single_instance = new ComServer();
        return single_instance;
    }

    public void start(int port) throws IOException {
        // Restart if necessary
        if(serverSocket != null)
            serverSocket = null;
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();

        connected = true;

        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public String readLinesFromServer()
    {
        String result = "";

        try {
            if(!clientSocket.isClosed()) {
                result = in.readLine();
                System.out.println(result);
                BufferedWriter w = new BufferedWriter(new FileWriter("src/main/resources/static/log.txt", true));
                w.append(result);
                w.close();
            }
        } catch (IOException e) {
            connected=false;
        }

        return result;
    }

    public void respondToClient(String l) {

    }

    public void sendLineToClient(String l)
    {
        if(out!= null)
            out.println(l);
        System.out.println("Sent to client : " + l);
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }

    public boolean connected()
    {
        if(!clientSocket.isConnected()) {
            connected = false;
        }

        return connected;
    }
}
