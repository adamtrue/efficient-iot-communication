package hevs.ecs.sim_com;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ComController {

    // Log the messages from the client
    public String log;
    static public String serverLog="";

    @GetMapping("/")
    public String index(Model model)//@RequestParam(required=true, name="motor", defaultValue="12") String motor, @RequestParam(required=true, name="command", defaultValue="rok") String command, Model model){
    {
        return "index";
    }

    /*
    @PostMapping("/")
    public String post(@RequestParam(required=true, name="motor", defaultValue="12") String motor, @RequestParam(required=true, name="command", defaultValue="rok") String command, @RequestParam(required=false, name="position", defaultValue="0") String position, Model model)
    {
            if (command.equals("rok")) {
                System.out.println("rok");
                DriverServer.getInstance().sendLineToClient(startCANNet);
            } else if (command.equals("preop-mot")) {
                System.out.println("preop-mot : " + motor);

                // Insert the motor id into the command
                String response = preopedMotor.replace("__", motor);
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("started-mot")) {
                System.out.println("started-mot : " + motor);

                // Insert the motor id into the command
                String response = startedMotor.replace("__", motor);
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mot-shutdown")) {
                System.out.println("mot-shutdown : " + motor);

                // Insert the motor id into the command
                String response = motorShutdown.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mot-started")) {
                System.out.println("mot-started : " + motor);

                // Insert the motor id into the command
                String response = motorStartedUp.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mot-enabled")) {
                System.out.println("mot-enabled : " + motor);

                // Insert the motor id into the command
                String response = motorOperational.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mot-fault")) {
                System.out.println("mot-fault : " + motor);

                // Insert the motor id into the command
                String response = motorFault.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mot-so-disabled")) {
                System.out.println("mot-so-disabled : " + motor);

                // Insert the motor id into the command
                String response = motorSoDis.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("home-started")) {
                System.out.println("home-started : " + motor);

                // Insert the motor id into the command
                String response = homeStarted.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("home-done")) {
                System.out.println("home-done : " + motor);

                // Insert the motor id into the command
                String response = homeDone.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("home-error")) {
                System.out.println("home-error : " + motor);

                // Insert the motor id into the command
                String response = homeError.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("mode-changed")) {
                System.out.println("mode-changed : " + motor);

                // Insert the motor id into the command
                String response = modeChanged.replace('_', motor.charAt(1));
                response = response.replace("mm", "06");
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("pos"))
            {
                System.out.println("Position indication : " + motor + ",  " + position);
                String response = "M 1 CSD 29" + motor.charAt(1) + " 0F 00";
                String numberHex = String.format("%08x", Integer.parseInt(position)).toUpperCase();

                for(int i=6; i>=0; i = i-2)
                {
                    response += " " + (numberHex.substring(i, i+2));
                }
                DriverServer.getInstance().sendLineToClient(response);
            } else if (command.equals("position-reached"))
            {
                String response = positionReached.replace('_', motor.charAt(1));
                DriverServer.getInstance().sendLineToClient(response);
            }

            model.addAttribute("motSelection", motor);
            model.addAttribute("commandSelection", command);

            return "index";
    }
    */
}
