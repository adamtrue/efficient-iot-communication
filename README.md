# Efficient IoT

This projects intends to develop a control and data channel communication infrastructure using recently emerged cellular LTE respectively LTE-M1 technology.

This project is the proposed Master's Thesis for Adam True, working under the supervision of Medard Rieder (HEVs) at Sion, Switzerland. The following is an idea of the Scope Statement in order to start the project. (This is not the official scope statement as one will specifically during the project)

## Scope Specifications

The core hardware tasks of this project are:

### Concerning the hardware

* Select an LTE modem
* Select an LTE-M1 modem
* Select an appropriate antenna system
* Build a prototype system using selected components
* Test prototype system for basic functionality

### Concerning the communication infrastructure

* Run an analysis in order to determine important requirements for the communication infrastructure of an IoT system
* Map requirements to a control and data channel communication model
* Develop a communication prototcol model and describe it in UML
* Develop an arbitration algorithm for LTE resp. LTE-M1 use

### Concerning the prototype

* Implement the proposed prototcol using an LTE and an LTE-M1 modem as well as an embedded system
* Test low bandwidth performance
* Test high bandwidth performance
* Optimal performance means: consume as little energy as possible per Byte transferred

### Concerning the reporting

* Document the entire code with comments and UML diagrams
* Write a final report
* Write a presentation

----

# To launch the example

## Setup

![General Schema](11_README_Images/general_schema.JPG)

## Launch the central app and Connect

1. Open Qt in **Administrator mode**. This is important because the EIoTC app will need administrator privileges in order to setup network interfaces.
2. Open the project at ``03_Development/PC_Central/App/app.pro``
3. Launch the project
4. Click on **Connect**

## Launch the EIoTC Project

1. Qt should be open in administrator mode
2. Open the project at ``03_Development/Software/Software/EIoTC/EIoTC.pro``
3. Launch the project and voila!

The EIoTC app should test by sending a small message via MQTT and a large message via UDP.
