/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for powerTask */
osThreadId_t powerTaskHandle;
const osThreadAttr_t powerTask_attributes = {
  .name = "powerTask",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for uartToPcTask */
osThreadId_t uartToPcTaskHandle;
const osThreadAttr_t uartToPcTask_attributes = {
  .name = "uartToPcTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 256 * 4
};
/* Definitions for adcHalfDone */
osMessageQueueId_t adcHalfDoneHandle;
const osMessageQueueAttr_t adcHalfDone_attributes = {
  .name = "adcHalfDone"
};
/* Definitions for adcDone */
osMessageQueueId_t adcDoneHandle;
const osMessageQueueAttr_t adcDone_attributes = {
  .name = "adcDone"
};
/* Definitions for tx1Done */
osMessageQueueId_t tx1DoneHandle;
const osMessageQueueAttr_t tx1Done_attributes = {
  .name = "tx1Done"
};
/* Definitions for rx1Received */
osMessageQueueId_t rx1ReceivedHandle;
const osMessageQueueAttr_t rx1Received_attributes = {
  .name = "rx1Received"
};
/* Definitions for tx2Done */
osMessageQueueId_t tx2DoneHandle;
const osMessageQueueAttr_t tx2Done_attributes = {
  .name = "tx2Done"
};
/* Definitions for rx2Received */
osMessageQueueId_t rx2ReceivedHandle;
const osMessageQueueAttr_t rx2Received_attributes = {
  .name = "rx2Received"
};
/* USER CODE BEGIN PV */
#define TX_SIZE 50
char tx_message[TX_SIZE];

// Buffer for ADC
#define ADC_BUFF_SIZE       5000 // TODO calculate Max size
int32_t                    AdcBuff[ADC_BUFF_SIZE];

uint32_t writingLTE = 0;

// Power measurement thingy
uint16_t power_measure_on = 0; // 0=off, 1=on

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);
void StartDefaultTask(void *argument);
void StartPowerTask(void *argument);
void StartUartTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of adcHalfDone */
  adcHalfDoneHandle = osMessageQueueNew (1, sizeof(uint16_t), &adcHalfDone_attributes);

  /* creation of adcDone */
  adcDoneHandle = osMessageQueueNew (1, sizeof(uint16_t), &adcDone_attributes);

  /* creation of tx1Done */
  tx1DoneHandle = osMessageQueueNew (1, sizeof(uint16_t), &tx1Done_attributes);

  /* creation of rx1Received */
  rx1ReceivedHandle = osMessageQueueNew (1, sizeof(uint16_t), &rx1Received_attributes);

  /* creation of tx2Done */
  tx2DoneHandle = osMessageQueueNew (1, sizeof(uint16_t), &tx2Done_attributes);

  /* creation of rx2Received */
  rx2ReceivedHandle = osMessageQueueNew (1, sizeof(uint16_t), &rx2Received_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of powerTask */
  powerTaskHandle = osThreadNew(StartPowerTask, NULL, &powerTask_attributes);

  /* creation of uartToPcTask */
  uartToPcTaskHandle = osThreadNew(StartUartTask, NULL, &uartToPcTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_MSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 16;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable MSI Auto calibration
  */
  HAL_RCCEx_EnableMSIPLLMode();
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.NbrOfConversion = 2;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_12CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_4BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_INPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LTE_nRESET_Pin|LTE_ON_OFF_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, nRF_nPOWon_Pin|LTE_Vref_nENABLE_Pin|LTE_DTR_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(BOOT0_GPIO_Port, BOOT0_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : nRF_Busy_Pin */
  GPIO_InitStruct.Pin = nRF_Busy_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(nRF_Busy_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LTE_nRESET_Pin LTE_ON_OFF_Pin */
  GPIO_InitStruct.Pin = LTE_nRESET_Pin|LTE_ON_OFF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : nRF_nPOWon_Pin LTE_DTR_Pin */
  GPIO_InitStruct.Pin = nRF_nPOWon_Pin|LTE_DTR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LTE_Vref_nENABLE_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LTE_Vref_nENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : BOOT0_Pin */
  GPIO_InitStruct.Pin = BOOT0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(BOOT0_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef *hadc)
{
    uint16_t value = 0x01;
    osMessageQueuePut(adcHalfDoneHandle, &value, 0U, 0U);
}
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
//    HAL_UART_Transmit_IT(&huart1, (AdcBuff+ADC_BUFF_SIZE*2), ADC_BUFF_SIZE*2);   // Buffer size is for 32 bit words so we multiply by 4 for 8 bit chars
    // then divide by 2 cause only half the buffer is filled
    uint16_t value = 0x01;
    osMessageQueuePut(adcDoneHandle, &value, 0U, 0U);
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
//    UNUSED(huart);
//    uint8_t buffer_mine[16] = "hey you cunts\r\n";
//    HAL_UART_Transmit_IT(&huart2, buffer_mine, 16);
    uint16_t value = 0x01;
    if(huart == &huart1) {
        osMessageQueuePut(tx1DoneHandle, &value, 0U, 0U);
    }
    else if(huart == &huart2) {
        osMessageQueuePut(tx2DoneHandle, &value, 0U, 0U);
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
//    UNUSED(huart);
//    uint8_t buffer_mine[16] = "hey you cunts\r\n";
//    HAL_UART_Transmit_IT(&huart2, buffer_mine, 16);
    uint16_t value = 0x01;
    if(huart == &huart1)
    {
        osMessageQueuePut(rx1ReceivedHandle, &value, 0U, 0U);
    }
    else if(huart == &huart2)
    {
        osMessageQueuePut(rx2ReceivedHandle, &value, 0U, 0U);
    }
}


/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
  uint8_t rx_buffer[16];
  osStatus_t status;
  uint16_t rx_notif;

  /* Infinite loop */
  for(;;)
  {
      if(writingLTE == 0)
      {
          HAL_UART_Receive_IT(&huart2, rx_buffer, 1);
          status = osMessageQueueGet(rx2ReceivedHandle, &rx_notif, NULL, osWaitForever);
          if(status == osOK)
          {
              switch(rx_buffer[0])
              {
                  case 'A':
                      // Switch on LTE Modem
                      HAL_GPIO_WritePin(GPIOA, LTE_ON_OFF_Pin, GPIO_PIN_RESET);
                      osDelay(6000);
                      HAL_GPIO_WritePin(GPIOA, LTE_ON_OFF_Pin, GPIO_PIN_SET);
                      //writingLTE = 1;
                      break;
                  case 'B':
                      // Switch on NRF Modem
                      HAL_GPIO_WritePin(GPIOB, nRF_nPOWon_Pin, GPIO_PIN_RESET);
                      break;
                  case 'C':
                      // Switch off NRF Modem
                      HAL_GPIO_WritePin(GPIOB, nRF_nPOWon_Pin, GPIO_PIN_SET);
                      break;
                  case 'P':
                      // Switch on power measurements
                      // buffer is uint32_t contains x2 16 bit values
                      HAL_ADC_Start_DMA(&hadc1, AdcBuff, ADC_BUFF_SIZE*2);
                      power_measure_on = 1;
                      break;
                  case  'S':
                      // Switch off power measurements
                      HAL_ADC_Stop_DMA(&hadc1);
                      power_measure_on = 0;
                      break;
              }
          }
      }/*
      else if(writingLTE == 1)
      {
          HAL_UART_Receive_IT(&huart2, rx_buffer, 16);
          status = osMessageQueueGet(rx2ReceivedHandle, &rx_notif, NULL, osWaitForever);
          if(status == osOK)
          {
            HAL_UART_Transmit_IT(&huart1, rx_buffer, 16);
            status = osMessageQueueGet(tx1DoneHandle, &rx_notif, NULL, osWaitForever);
            if(status == osOK)
            {
                HAL_UART_Receive_IT(&huart1, rx_buffer, 16);
                status = osMessageQueueGet(rx2ReceivedHandle, &rx_notif, NULL, osWaitForever);
                if(status == osOK)
                    HAL_UART_Transmit_IT(&huart2, rx_buffer, 16);
            }
          }

      }*/


    osDelay(1);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartPowerTask */
/**
* @brief Function implementing the powerTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPowerTask */
void StartPowerTask(void *argument)
{
  /* USER CODE BEGIN StartPowerTask */

  /* Infinite loop */
  for(;;)
  {
      if(power_measure_on)
      {
//          HAL_ADC_Start_DMA(&hadc1, AdcBuff, ADC_BUFF_SIZE);
      }
      osDelay(500);
  }
  /* USER CODE END StartPowerTask */
}

/* USER CODE BEGIN Header_StartUartTask */
/**
* @brief Function implementing the uartToPcTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartUartTask */
void StartUartTask(void *argument)
{
  /* USER CODE BEGIN StartUartTask */
  uint16_t adcGet;
  uint32_t nrf_average_int;
  float nrf_avg;
  uint32_t telit_average_int;
  float telit_avg;
  int nrf_countable = 0;
  int telit_countable = 0;
  osStatus_t status;

  uint32_t currentValue = 0;
  uint32_t currentNrfValue = 0;
  uint32_t currentTelitValue = 0;
  uint32_t telit_lowest = 0;
  uint32_t telit_highest = 0;

  uint32_t nrf_lowest = 0;
  uint32_t nrf_highest = 0;

  int i = 0;

  /* Infinite loop */
  for(;;)
  {
//      status = osMessageQueueGet(adcHalfDoneHandle, &adcGet, NULL, osWaitForever);

      // Wait for half the ADC queue to be done
      status = osMessageQueueGet(adcHalfDoneHandle, &adcGet, NULL, osWaitForever);
      if(status == osOK)
      {
          // Transform to printable hex
          for (i = 0; i < ADC_BUFF_SIZE/2; i+=1) {
              currentValue =  *( (AdcBuff) + i);
              currentNrfValue = currentValue & 0xFFF;
              // Value is negative so correct it
              /*if(currentNrfValue & 0x800)
              {
                  currentNrfValue = -(currentNrfValue&0x7FF);
              }*/
              nrf_average_int += currentNrfValue;
              currentTelitValue = (currentValue>>16) & 0xFFF;
              // Value is negative so correct it
              /*if(currentTelitValue & 0x800)
              {
                  currentTelitValue = -(currentTelitValue&0x7FF);
              }*/
              telit_average_int += currentTelitValue;
              nrf_countable += 1;
              telit_countable += 1;

              if(i == 0)
              {
                  nrf_lowest = (int32_t)currentNrfValue;
                  nrf_highest = (int32_t)currentNrfValue;
                  telit_lowest = (int32_t)currentTelitValue;
                  telit_highest = (int32_t)currentTelitValue;
              }
              else{
                  if(currentTelitValue < telit_lowest)
                  {
                      telit_lowest = currentTelitValue;
                  }
                  if(currentTelitValue > telit_highest)
                  {
                      telit_highest = currentTelitValue;
                  }
                  if(currentNrfValue < nrf_lowest)
                  {
                      nrf_lowest = currentNrfValue;
                  }
                  if(currentNrfValue > nrf_highest)
                  {
                      nrf_highest = currentNrfValue;
                  }
              }
          }

          // When the transmission is done
//          status = osMessageQueueGet(adcHalfDoneHandle, &adcGet, NULL, osWaitForever);
          status = osMessageQueueGet(adcDoneHandle, &adcGet, NULL, osWaitForever);
          if(status == osOK)
          {
              // Transform to printable hex
              for (i = ADC_BUFF_SIZE/2; i < ADC_BUFF_SIZE; i+=1) {
                  currentValue =  *( (AdcBuff) + i);
                  currentNrfValue = currentValue & 0xFFF;
                  // Value is negative so correct it
                  /*if(currentNrfValue & 0x800)
                  {
                      currentNrfValue = -(currentNrfValue&0x7FF);
                  }*/
                  nrf_average_int += currentNrfValue;
                  currentTelitValue = (currentValue>>16) & 0xFFF;
                  // Value is negative so correct it
                  /*if(currentTelitValue & 0x800)
                  {
                      currentTelitValue = -(currentTelitValue&0x7FF);
                  }*/
                  telit_average_int += currentTelitValue;
                  nrf_countable += 1;
                  telit_countable += 1;
                  /*currentValue =  *( (AdcBuff) + i);
                  nrf_average_int += currentValue & 0xFFF;
                  telit_average_int += (currentValue>>16) & 0xFFF;
                  nrf_countable += 1;
                  telit_countable += 1;*/
                  if(currentTelitValue < telit_lowest)
                  {
                      telit_lowest = currentTelitValue;
                  }
                  if(currentTelitValue > telit_highest)
                  {
                      telit_highest = currentTelitValue;
                  }
                  if(currentNrfValue < nrf_lowest)
                  {
                      nrf_lowest = currentNrfValue;
                  }
                  if(currentNrfValue > nrf_highest)
                  {
                      nrf_highest = currentNrfValue;
                  }
              }

              nrf_avg = (float) nrf_average_int/ (float) nrf_countable;
              telit_avg = (float) telit_average_int/ (float) telit_countable;
//              sprintf((char*)tx_message, "\r\nN:%i,%i,%i\r\nT:%i,%i,%i\r\n", (int) nrf_avg, (int)nrf_lowest, (int)nrf_highest, (int)telit_avg, (int)telit_lowest, (int)telit_highest);
              sprintf((char*)tx_message, "\r\nN:%06d\r\nT:%06d\r\n", (int) nrf_avg, (int)telit_avg);

              nrf_average_int = 0;
              nrf_countable = 0;
              telit_average_int = 0;
              telit_countable = 0;

              HAL_UART_Transmit_DMA(&huart2, tx_message, TX_SIZE);
              status = osMessageQueueGet(tx2DoneHandle, &adcGet, NULL, osWaitForever);
              if(status == osOK && power_measure_on == 1) {
                  //HAL_ADC_Start_DMA(&hadc1, AdcBuff, ADC_BUFF_SIZE);
              }
          }
      }
  }
  /* USER CODE END StartUartTask */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
