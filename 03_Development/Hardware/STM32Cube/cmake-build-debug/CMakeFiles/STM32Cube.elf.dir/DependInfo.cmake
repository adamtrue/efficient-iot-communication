# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/startup/startup_stm32l432xx.s" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/startup/startup_stm32l432xx.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32L432xx"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  )
set(CMAKE_DEPENDS_CHECK_C
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/freertos.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/freertos.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/main.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/main.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/stm32l4xx_hal_msp.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/stm32l4xx_hal_msp.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/stm32l4xx_hal_timebase_tim.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/stm32l4xx_hal_timebase_tim.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/stm32l4xx_it.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/stm32l4xx_it.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/syscalls.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/syscalls.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Core/Src/system_stm32l4xx.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Core/Src/system_stm32l4xx.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_spi_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_uart_ex.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2/cmsis_os2.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2/cmsis_os2.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/croutine.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/croutine.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/list.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/list.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/queue.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/queue.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/tasks.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/tasks.c.obj"
  "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/Middlewares/Third_Party/FreeRTOS/Source/timers.c" "C:/Users/adamt/Desktop/efficient-iot-communication/03_Development/Hardware/STM32Cube/cmake-build-debug/CMakeFiles/STM32Cube.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/timers.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32L432xx"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
