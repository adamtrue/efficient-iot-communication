var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuwz",
  1: "r",
  2: "r",
  3: "abcdefghijlmnoprstuwz",
  4: "aiprt",
  5: "abceilnprstu",
  6: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Macros"
};

