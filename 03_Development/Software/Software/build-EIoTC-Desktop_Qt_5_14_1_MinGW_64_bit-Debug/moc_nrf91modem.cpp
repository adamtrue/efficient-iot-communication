/****************************************************************************
** Meta object code from reading C++ file 'nrf91modem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../EIoTC/Modem/nrf91modem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'nrf91modem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NRF91Modem_t {
    QByteArrayData data[14];
    char stringdata0[146];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NRF91Modem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NRF91Modem_t qt_meta_stringdata_NRF91Modem = {
    {
QT_MOC_LITERAL(0, 0, 10), // "NRF91Modem"
QT_MOC_LITERAL(1, 11, 25), // "centralPublicFaceReceived"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 7), // "ip_addr"
QT_MOC_LITERAL(4, 46, 4), // "port"
QT_MOC_LITERAL(5, 51, 10), // "mqtt_event"
QT_MOC_LITERAL(6, 62, 7), // "message"
QT_MOC_LITERAL(7, 70, 9), // "mqtt_data"
QT_MOC_LITERAL(8, 80, 5), // "topic"
QT_MOC_LITERAL(9, 86, 4), // "data"
QT_MOC_LITERAL(10, 91, 10), // "nrfRunning"
QT_MOC_LITERAL(11, 102, 26), // "handlePeripheralPublicFace"
QT_MOC_LITERAL(12, 129, 2), // "ip"
QT_MOC_LITERAL(13, 132, 13) // "mqttTimerDone"

    },
    "NRF91Modem\0centralPublicFaceReceived\0"
    "\0ip_addr\0port\0mqtt_event\0message\0"
    "mqtt_data\0topic\0data\0nrfRunning\0"
    "handlePeripheralPublicFace\0ip\0"
    "mqttTimerDone"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NRF91Modem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   44,    2, 0x06 /* Public */,
       5,    1,   49,    2, 0x06 /* Public */,
       7,    2,   52,    2, 0x06 /* Public */,
      10,    0,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    2,   58,    2, 0x0a /* Public */,
      13,    0,   63,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    8,    9,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   12,    4,
    QMetaType::Void,

       0        // eod
};

void NRF91Modem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<NRF91Modem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->centralPublicFaceReceived((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->mqtt_event((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->mqtt_data((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->nrfRunning(); break;
        case 4: _t->handlePeripheralPublicFace((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->mqttTimerDone(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (NRF91Modem::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NRF91Modem::centralPublicFaceReceived)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (NRF91Modem::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NRF91Modem::mqtt_event)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (NRF91Modem::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NRF91Modem::mqtt_data)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (NRF91Modem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NRF91Modem::nrfRunning)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject NRF91Modem::staticMetaObject = { {
    QMetaObject::SuperData::link<GenericModem::staticMetaObject>(),
    qt_meta_stringdata_NRF91Modem.data,
    qt_meta_data_NRF91Modem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *NRF91Modem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NRF91Modem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NRF91Modem.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "GenericMQTTInterface"))
        return static_cast< GenericMQTTInterface*>(this);
    if (!strcmp(_clname, "GenericFTPInterface"))
        return static_cast< GenericFTPInterface*>(this);
    return GenericModem::qt_metacast(_clname);
}

int NRF91Modem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = GenericModem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void NRF91Modem::centralPublicFaceReceived(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NRF91Modem::mqtt_event(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void NRF91Modem::mqtt_data(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void NRF91Modem::nrfRunning()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
