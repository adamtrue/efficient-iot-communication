/****************************************************************************
** Meta object code from reading C++ file 'powercontrol.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../EIoTC/Hardware/powercontrol.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'powercontrol.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PowerControl_t {
    QByteArrayData data[13];
    char stringdata0[182];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PowerControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PowerControl_t qt_meta_stringdata_PowerControl = {
    {
QT_MOC_LITERAL(0, 0, 12), // "PowerControl"
QT_MOC_LITERAL(1, 13, 15), // "nrfDataReceived"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 4), // "data"
QT_MOC_LITERAL(4, 35, 17), // "telitDataReceived"
QT_MOC_LITERAL(5, 53, 22), // "serial_openPortConfirm"
QT_MOC_LITERAL(6, 76, 24), // "serial_closePortIndicate"
QT_MOC_LITERAL(7, 101, 20), // "serial_errorIndicate"
QT_MOC_LITERAL(8, 122, 20), // "eSerialPortErrorType"
QT_MOC_LITERAL(9, 143, 1), // "e"
QT_MOC_LITERAL(10, 145, 22), // "serial_messageIndicate"
QT_MOC_LITERAL(11, 168, 11), // "std::string"
QT_MOC_LITERAL(12, 180, 1) // "m"

    },
    "PowerControl\0nrfDataReceived\0\0data\0"
    "telitDataReceived\0serial_openPortConfirm\0"
    "serial_closePortIndicate\0serial_errorIndicate\0"
    "eSerialPortErrorType\0e\0serial_messageIndicate\0"
    "std::string\0m"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PowerControl[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   50,    2, 0x0a /* Public */,
       6,    0,   51,    2, 0x0a /* Public */,
       7,    1,   52,    2, 0x0a /* Public */,
      10,    1,   55,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void PowerControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PowerControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nrfDataReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->telitDataReceived((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->serial_openPortConfirm(); break;
        case 3: _t->serial_closePortIndicate(); break;
        case 4: _t->serial_errorIndicate((*reinterpret_cast< eSerialPortErrorType(*)>(_a[1]))); break;
        case 5: _t->serial_messageIndicate((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PowerControl::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PowerControl::nrfDataReceived)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (PowerControl::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PowerControl::telitDataReceived)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PowerControl::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_PowerControl.data,
    qt_meta_data_PowerControl,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PowerControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PowerControl::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PowerControl.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PowerControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void PowerControl::nrfDataReceived(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PowerControl::telitDataReceived(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
