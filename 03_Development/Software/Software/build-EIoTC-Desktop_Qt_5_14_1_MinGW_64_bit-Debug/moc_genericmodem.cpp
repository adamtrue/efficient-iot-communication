/****************************************************************************
** Meta object code from reading C++ file 'genericmodem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../EIoTC/Modem/genericmodem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'genericmodem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_GenericModem_t {
    QByteArrayData data[13];
    char stringdata0[195];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GenericModem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GenericModem_t qt_meta_stringdata_GenericModem = {
    {
QT_MOC_LITERAL(0, 0, 12), // "GenericModem"
QT_MOC_LITERAL(1, 13, 15), // "udpSocketOpened"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 14), // "udpSocketError"
QT_MOC_LITERAL(4, 45, 11), // "udpResponse"
QT_MOC_LITERAL(5, 57, 22), // "serial_openPortConfirm"
QT_MOC_LITERAL(6, 80, 24), // "serial_closePortIndicate"
QT_MOC_LITERAL(7, 105, 20), // "serial_errorIndicate"
QT_MOC_LITERAL(8, 126, 20), // "eSerialPortErrorType"
QT_MOC_LITERAL(9, 147, 4), // "type"
QT_MOC_LITERAL(10, 152, 22), // "serial_messageIndicate"
QT_MOC_LITERAL(11, 175, 11), // "std::string"
QT_MOC_LITERAL(12, 187, 7) // "message"

    },
    "GenericModem\0udpSocketOpened\0\0"
    "udpSocketError\0udpResponse\0"
    "serial_openPortConfirm\0serial_closePortIndicate\0"
    "serial_errorIndicate\0eSerialPortErrorType\0"
    "type\0serial_messageIndicate\0std::string\0"
    "message"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GenericModem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   52,    2, 0x0a /* Public */,
       6,    0,   53,    2, 0x0a /* Public */,
       7,    1,   54,    2, 0x0a /* Public */,
      10,    1,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 11,   12,

       0        // eod
};

void GenericModem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<GenericModem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->udpSocketOpened(); break;
        case 1: _t->udpSocketError(); break;
        case 2: _t->udpResponse(); break;
        case 3: _t->serial_openPortConfirm(); break;
        case 4: _t->serial_closePortIndicate(); break;
        case 5: _t->serial_errorIndicate((*reinterpret_cast< eSerialPortErrorType(*)>(_a[1]))); break;
        case 6: _t->serial_messageIndicate((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (GenericModem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GenericModem::udpSocketOpened)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (GenericModem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GenericModem::udpSocketError)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (GenericModem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GenericModem::udpResponse)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject GenericModem::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_GenericModem.data,
    qt_meta_data_GenericModem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *GenericModem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GenericModem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_GenericModem.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int GenericModem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void GenericModem::udpSocketOpened()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void GenericModem::udpSocketError()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void GenericModem::udpResponse()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
