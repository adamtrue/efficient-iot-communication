/****************************************************************************
** Meta object code from reading C++ file 'eiotcserialport.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../EIoTC/Modem/eiotcserialport.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'eiotcserialport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_EIoTCSerialPort_t {
    QByteArrayData data[17];
    char stringdata0[259];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_EIoTCSerialPort_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_EIoTCSerialPort_t qt_meta_stringdata_EIoTCSerialPort = {
    {
QT_MOC_LITERAL(0, 0, 15), // "EIoTCSerialPort"
QT_MOC_LITERAL(1, 16, 15), // "openPortConfirm"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 17), // "closePortIndicate"
QT_MOC_LITERAL(4, 51, 13), // "errorIndicate"
QT_MOC_LITERAL(5, 65, 20), // "eSerialPortErrorType"
QT_MOC_LITERAL(6, 86, 4), // "type"
QT_MOC_LITERAL(7, 91, 15), // "messageIndicate"
QT_MOC_LITERAL(8, 107, 11), // "std::string"
QT_MOC_LITERAL(9, 119, 7), // "message"
QT_MOC_LITERAL(10, 127, 24), // "dataTerminalReadyConfirm"
QT_MOC_LITERAL(11, 152, 15), // "openPortRequest"
QT_MOC_LITERAL(12, 168, 12), // "writeRequest"
QT_MOC_LITERAL(13, 181, 22), // "readIndicate_fromBelow"
QT_MOC_LITERAL(14, 204, 23), // "errorIndicate_fromBelow"
QT_MOC_LITERAL(15, 228, 28), // "QSerialPort::SerialPortError"
QT_MOC_LITERAL(16, 257, 1) // "e"

    },
    "EIoTCSerialPort\0openPortConfirm\0\0"
    "closePortIndicate\0errorIndicate\0"
    "eSerialPortErrorType\0type\0messageIndicate\0"
    "std::string\0message\0dataTerminalReadyConfirm\0"
    "openPortRequest\0writeRequest\0"
    "readIndicate_fromBelow\0errorIndicate_fromBelow\0"
    "QSerialPort::SerialPortError\0e"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_EIoTCSerialPort[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,
       3,    0,   65,    2, 0x06 /* Public */,
       4,    1,   66,    2, 0x06 /* Public */,
       7,    1,   69,    2, 0x06 /* Public */,
      10,    0,   72,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   73,    2, 0x0a /* Public */,
      12,    1,   74,    2, 0x0a /* Public */,
      12,    1,   77,    2, 0x0a /* Public */,
      13,    0,   80,    2, 0x0a /* Public */,
      14,    1,   81,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, QMetaType::QByteArray,    9,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,

       0        // eod
};

void EIoTCSerialPort::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<EIoTCSerialPort *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->openPortConfirm(); break;
        case 1: _t->closePortIndicate(); break;
        case 2: _t->errorIndicate((*reinterpret_cast< eSerialPortErrorType(*)>(_a[1]))); break;
        case 3: _t->messageIndicate((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 4: _t->dataTerminalReadyConfirm(); break;
        case 5: _t->openPortRequest(); break;
        case 6: _t->writeRequest((*reinterpret_cast< std::string(*)>(_a[1]))); break;
        case 7: _t->writeRequest((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 8: _t->readIndicate_fromBelow(); break;
        case 9: _t->errorIndicate_fromBelow((*reinterpret_cast< QSerialPort::SerialPortError(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (EIoTCSerialPort::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&EIoTCSerialPort::openPortConfirm)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (EIoTCSerialPort::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&EIoTCSerialPort::closePortIndicate)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (EIoTCSerialPort::*)(eSerialPortErrorType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&EIoTCSerialPort::errorIndicate)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (EIoTCSerialPort::*)(std::string );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&EIoTCSerialPort::messageIndicate)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (EIoTCSerialPort::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&EIoTCSerialPort::dataTerminalReadyConfirm)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject EIoTCSerialPort::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_EIoTCSerialPort.data,
    qt_meta_data_EIoTCSerialPort,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *EIoTCSerialPort::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *EIoTCSerialPort::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_EIoTCSerialPort.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int EIoTCSerialPort::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void EIoTCSerialPort::openPortConfirm()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void EIoTCSerialPort::closePortIndicate()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void EIoTCSerialPort::errorIndicate(eSerialPortErrorType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void EIoTCSerialPort::messageIndicate(std::string _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void EIoTCSerialPort::dataTerminalReadyConfirm()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
