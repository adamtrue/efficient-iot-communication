/****************************************************************************
** Meta object code from reading C++ file 'tc4modem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../EIoTC/Modem/tc4modem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tc4modem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TC4Modem_t {
    QByteArrayData data[11];
    char stringdata0[171];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TC4Modem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TC4Modem_t qt_meta_stringdata_TC4Modem = {
    {
QT_MOC_LITERAL(0, 0, 8), // "TC4Modem"
QT_MOC_LITERAL(1, 9, 28), // "peripheralPublicFaceReceived"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 2), // "ip"
QT_MOC_LITERAL(4, 42, 4), // "port"
QT_MOC_LITERAL(5, 47, 12), // "udpNoCarrier"
QT_MOC_LITERAL(6, 60, 27), // "udpSocketReadyForMultimedia"
QT_MOC_LITERAL(7, 88, 25), // "udpSocketReadyForMessages"
QT_MOC_LITERAL(8, 114, 17), // "cgcontrdpResponse"
QT_MOC_LITERAL(9, 132, 23), // "handleCentralPublicFace"
QT_MOC_LITERAL(10, 156, 14) // "multimediaDone"

    },
    "TC4Modem\0peripheralPublicFaceReceived\0"
    "\0ip\0port\0udpNoCarrier\0udpSocketReadyForMultimedia\0"
    "udpSocketReadyForMessages\0cgcontrdpResponse\0"
    "handleCentralPublicFace\0multimediaDone"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TC4Modem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   49,    2, 0x06 /* Public */,
       5,    0,   54,    2, 0x06 /* Public */,
       6,    0,   55,    2, 0x06 /* Public */,
       7,    0,   56,    2, 0x06 /* Public */,
       8,    0,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    2,   58,    2, 0x0a /* Public */,
      10,    0,   63,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void,

       0        // eod
};

void TC4Modem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TC4Modem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->peripheralPublicFaceReceived((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->udpNoCarrier(); break;
        case 2: _t->udpSocketReadyForMultimedia(); break;
        case 3: _t->udpSocketReadyForMessages(); break;
        case 4: _t->cgcontrdpResponse(); break;
        case 5: _t->handleCentralPublicFace((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->multimediaDone(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TC4Modem::*)(QString , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TC4Modem::peripheralPublicFaceReceived)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TC4Modem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TC4Modem::udpNoCarrier)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (TC4Modem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TC4Modem::udpSocketReadyForMultimedia)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (TC4Modem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TC4Modem::udpSocketReadyForMessages)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (TC4Modem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TC4Modem::cgcontrdpResponse)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TC4Modem::staticMetaObject = { {
    QMetaObject::SuperData::link<GenericModem::staticMetaObject>(),
    qt_meta_stringdata_TC4Modem.data,
    qt_meta_data_TC4Modem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TC4Modem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TC4Modem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TC4Modem.stringdata0))
        return static_cast<void*>(this);
    return GenericModem::qt_metacast(_clname);
}

int TC4Modem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = GenericModem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void TC4Modem::peripheralPublicFaceReceived(QString _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void TC4Modem::udpNoCarrier()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void TC4Modem::udpSocketReadyForMultimedia()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void TC4Modem::udpSocketReadyForMessages()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void TC4Modem::cgcontrdpResponse()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
