#ifndef MODEMTEST_H
#define MODEMTEST_H

#include <QVector>
#include <QIODevice>
#include <QTimer>

#include "XF/genericstatemachine.h"
#include "Hardware/energycalculator.h"
#include "Modem/nrf91modem.h"
#include "Modem/tc4modem.h"

typedef enum
{
    ST_TEST_INIT,
    ST_WAIT_FOR_NRF,
    ST_WAIT_FOR_TELIT,
    ST_TELIT_DONE,
    ST_NRF_MESSAGE,
    ST_TELIT_MESSAGE,
    ST_PRINT_TESTS
} eModemTestStateMachine;

class ModemTest: public QObject, public GenericStateMachine
{
    Q_OBJECT

public:
    ModemTest();

    void build();

    void processEvent(XFEventEnum event) override;
    void setCalculator(EnergyCalculator *calculator);

    void nrfStartMeasure();
    void nrfStopMeasure();
    void telitStartMeasure();
    void telitStopMeasure();

    void setUdpIoDevice(QIODevice *udpIoDevice);

    void setNrfModem(NRF91Modem *nrfModem);

    void setTc4Modem(TC4Modem *tc4Modem);

    void registerTelitTime();
    void registerNrfTime();

public slots:
    void timerDone();

private:
    EnergyCalculator* _calculator;

    QVector<double> _nrfEnergyPacketTimes;
    QVector<double> _telitEnergyPacketTimes;

//    QIODevice* _udpIoDevice;

    double _nrfValue1;
    double _nrfValue2;
    double _telitValue1;
    double _telitValue2;
    QString _message;

    QTimer _timer;

    eModemTestStateMachine _currentState;

    NRF91Modem* _nrfModem;
    TC4Modem* _tc4Modem;

    void wakeupTelit();
    void sendNrfMessage();
    void sendTelitMessage();
    void startTimer();
    void increaseMessageSize();
    void printTests();
};

#endif // MODEMTEST_H
