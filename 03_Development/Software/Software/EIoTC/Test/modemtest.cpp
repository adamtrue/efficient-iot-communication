#include "modemtest.h"

#include <QFile>
#include <QDebug>
#include <QDateTime>

#include "XF/xf.h"

#define TEST_TIMEOUT 500
#define MESSAGE_LENGTH_MAX 100000
#define MESSAGE_INCREMENT 20000


ModemTest::ModemTest(): _currentState(ST_TEST_INIT), _message("")
{

}

void ModemTest::build()
{
    QObject::connect(&_timer, &QTimer::timeout, this, &ModemTest::timerDone);
}

void ModemTest::processEvent(XFEventEnum event)
{
    eModemTestStateMachine oldState = _currentState;

    switch(event)
    {
    case evDefault:
        if(_currentState == ST_TEST_INIT)
        {
            _currentState = ST_WAIT_FOR_NRF;
        }
        break;
    case evMqttSubscribedDone:
        if(_currentState == ST_WAIT_FOR_NRF)
        {
            _currentState = ST_WAIT_FOR_TELIT;
        }
        break;
    case evUdpReadyForMessages:
        if(_currentState == ST_WAIT_FOR_TELIT)
        {
            _currentState = ST_TELIT_DONE;
        }
        break;
    case evStartTests:
        if(_currentState == ST_TELIT_DONE)
        {
            _currentState = ST_NRF_MESSAGE;
        }
        break;
    case evMqttPublished:
        if(_currentState == ST_NRF_MESSAGE)
        {
            _currentState = ST_TELIT_MESSAGE;
        }
        break;
    case evTestsTimeout:
        if(_currentState == ST_TELIT_MESSAGE)
        {
            _currentState = ST_NRF_MESSAGE;
        }
        break;
    case evTestsDone:
        if(_currentState == ST_TELIT_MESSAGE)
        {
            _currentState = ST_PRINT_TESTS;
        }
        break;

    default:
        break;
    }

    if(oldState != _currentState)
    {
        switch(_currentState)
        {
        case ST_WAIT_FOR_NRF:
            qDebug() << "ST_WAIT_FOR_NRF";
//            nrfStartMeasure();
            break;
        case ST_WAIT_FOR_TELIT:
            qDebug() << "ST_WAIT_FOR_TELIT";
//            nrfStopMeasure();
//            telitStartMeasure();
            wakeupTelit();
            break;
        case ST_TELIT_DONE:
            qDebug() << "ST_TELIT_DONE";
            XF::getInstance().pushEvent(evStartTests);
            break;
        case ST_NRF_MESSAGE:
            qDebug() << "ST_NRF_MESSAGE";
//            telitStopMeasure();
            increaseMessageSize();
//            nrfStartMeasure();
            sendNrfMessage();
            break;
        case ST_TELIT_MESSAGE:
            qDebug() << "ST_TELIT_MESSAGE";
 //           nrfStopMeasure();
 //           telitStartMeasure();
            sendTelitMessage();
            if(_message.length() < MESSAGE_LENGTH_MAX)
            {
                startTimer();
            }
            else
            {
                XF::getInstance().pushEvent(evTestsDone);
            }
            break;
        case ST_PRINT_TESTS:
            qDebug() << "ST_PRINT_TESTS";
//            telitStopMeasure();
            printTests();
            break;
        default:
            break;
        }

    }
}

void ModemTest::setCalculator(EnergyCalculator *calculator)
{
    _calculator = calculator;
}

void ModemTest::nrfStartMeasure()
{
    _nrfValue1 = _calculator->nrfAccumulator();
}

void ModemTest::nrfStopMeasure()
{
//    _nrfValue2 = _calculator->nrfAccumulator();
//    _nrfEnergy.append(_nrfValue2-_nrfValue1);
}

void ModemTest::telitStartMeasure()
{
    _telitValue1 = _calculator->telitAccumulator();
}

void ModemTest::telitStopMeasure()
{
//    _telitValue2 = _calculator->telitAccumulator();
//    _telitEnergy.append(_telitValue2-_telitValue1);
}

void ModemTest::setUdpIoDevice(QIODevice *udpIoDevice)
{
//    _udpIoDevice = udpIoDevice;
}

void ModemTest::timerDone()
{
    XF::getInstance().pushEvent(evTestsTimeout);
}

void ModemTest::setTc4Modem(TC4Modem *tc4Modem)
{
    _tc4Modem = tc4Modem;
}

void ModemTest::registerTelitTime()
{
    _telitEnergyPacketTimes.append(QDateTime::currentDateTime().time().msecsSinceStartOfDay());
}

void ModemTest::registerNrfTime()
{
    _nrfEnergyPacketTimes.append(QDateTime::currentDateTime().time().msecsSinceStartOfDay());
}

void ModemTest::setNrfModem(NRF91Modem *nrfModem)
{
    _nrfModem = nrfModem;
}

void ModemTest::wakeupTelit()
{
    XF::getInstance().pushEvent(evTelitWakeup);
}

void ModemTest::sendNrfMessage()
{
    _nrfModem->pushPublishReq("adamtopic/peripheral/data",_message);
}

void ModemTest::sendTelitMessage()
{
    QByteArray a = _message.toUtf8();
//    _udpIoDevice->write(a);
    _tc4Modem->udpSend(a.toStdString());
    registerTelitTime();
}

void ModemTest::startTimer()
{
    _timer.setSingleShot(true);
    _timer.start(TEST_TIMEOUT);
}

void ModemTest::increaseMessageSize()
{
    if(MESSAGE_INCREMENT >= 10)
    {
        // Just a string of 10 bytes
        for(int i=0; i<MESSAGE_INCREMENT/10; i++)
            _message.append("ABCDEFGHIK");
    }
    else{
        _message.append("Q");
    }
}

void ModemTest::printTests()
{
    QFile file("C:\\Users\\adamt\\Desktop\\OUTPUT\\out.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    out << "NRF Times\r\n";
    for (auto a: _nrfEnergyPacketTimes)
    {
        out << a << "\r\n";
    }

    out << "Telit Times\r\n";
    for (auto a: _telitEnergyPacketTimes)
    {
        out << a << "\r\n";
    }


}
