#ifndef GENERICMODEM_H
#define GENERICMODEM_H

#include <QObject>
#include <QTimer>
#include <QEventLoop>
#include "eiotcserialport.h"
#include "genericmqttinterface.h"
#include "genericftpinterface.h"
#include "Hardware/powercontrol.h"

// TODO control mechanism for baudrates
typedef enum
{
    brError,             // Availability
    br300 = 300,         // TC4
    br1200 = 1200,       // TC4 - nrf91
    br2400 = 2400,       // TC4 - nrf91
    br4800 = 4800,       // TC4 - nrf91
    br9600 = 9600,       // TC4 - nrf91
    br14400 = 14400,     // nrf91
    br19200 = 19200,     // T C4 - nrf91
    br38400 = 38400,     // TC4 - nrf91
    br57600 = 57600,     // TC4 - nrf91
    br115200 = 115200,   // TC4 - nrf91
    br230400 = 230400,   // TC4 - nrf91
    br460800 = 460800,   // TC4 - nrf91
    br921600 = 921600,   // TC4 - nrf91
    br1000000 = 1000000, // nrf91
    br3000000 = 3000000, // TC4
} eModemBaudrate;

/*
typedef enum
{
    AT_SET_REQUEST,
    AT_READ_REQUEST,
    AT_CHECK_REQUEST
} eModemRequestType;
*/

typedef enum
{
    MODEM_COPS_AUTOMATIC,
    MODEM_COPS_MANUAL,
    MODEM_COPS_DEREGISTER,
    MODEM_COPS_SET_ONLY,
    MODEM_COPS_MANUAL_AUTOMATIC_FALLBACK
} eModemCOPSRegistration;

typedef enum
{
    MODEM_COPS_GSM,
    MODEM_COPS_GSM_COMPACT,
    MODEM_COPS_UTRAN,
    MODEM_COPS_GSM_EGPRS,
    MODEM_COPS_UTRAN_HSDPA,
    MODEM_COPS_UTRAN_HSUPA,
    MODEM_COPS_UTRAN_HSDPA_HSUPA,
    MODEM_COPS_E_UTRAN
} eModemCOPSAccessTechnology;

/* This is a generic AT modem class for all modems */

class GenericModem : public QObject
{
    Q_OBJECT
public:
    explicit GenericModem(QObject *parent = nullptr);

    EIoTCSerialPort *serialPort() const;
    void setSerialPort(EIoTCSerialPort *serialPort);

    void startUp();
    void startUpSerial();
    virtual void connectLTE();

    // ----------- GENERIC COMMANDS -------------
    void modem_CFUN_onRequest();
    void modem_CFUN_offRequest();
    void modem_CFUN_stateRequest();

    void modem_CGSN_imeiRequest();
    void modem_CGMI_manufacturerRequest();
    void modem_CGMM_modelInfoRequest();
    void modem_CGMR_revisionInfoRequest();
    void modem_CEMODE_csPsModeRequest();
    void modem_CMEE_setResultCodeRequest(bool enable_disable); // Enable or disable result codes
    void modem_CMEE_resultCodeRequest();    // Get result code enable or not
    void modem_CGEREP_getTXUnsolicitedErrorsEnableRequest();
    void modem_CGEREP_setTXUnsolicitedErrorsEnableRequest(uint16_t mode);
    void modem_CGDCONT_getPDPInfoRequest();
    void modem_CGACT_getPDPActivatedRequest();
    void modem_CIND_setDeviceIndicators(uint16_t service, uint16_t roam, uint16_t message); // 0 is off, 1 is on
    void modem_CEREG_setEPSRegistrationResultCodesRequest(uint16_t mode);   // between 0 and 5 (5 included)
    void modem_CEREG_getEPSRegistrationStatusRequest();
    void modem_CESQ_setSignalQualityParametersRequest(uint16_t mode);
    void modem_CESQ_getSignalQualityParametersRequest();
//    void modem_COPS_
    void modem_CPIN_getPinUnlockTypeRequest();
    void modem_CPINR_getRemainingUnlocksRequest();
    void modem_CIMI_getIMSIRequest();
    void modem_COPS_setPolicyRequest(eModemCOPSRegistration reg, eModemCOPSAccessTechnology tech);
    void modem_COPS_setPolicyRequest(eModemCOPSRegistration reg, eModemCOPSAccessTechnology tech, QString operatorMMC);
    void modem_COPS_getPolicyRequest();

    void slm_versionRequest();
    void slm_commandListRequest();

    // TODO test today
    void sleepRequest();
    void idleRequest();

    // TODO test
    virtual void modem_changeBaudrateRequest(eModemBaudrate br);

    void _wait(uint32_t time);
    // -----------

// TODO, these should only be for the TC4Modem ...
    PowerControl *power() const;
    void setPower(PowerControl *power);

signals:
    void udpSocketOpened();
    void udpSocketError();
    void udpResponse();

public slots:
    void serial_openPortConfirm();
    void serial_closePortIndicate();
    void serial_errorIndicate(eSerialPortErrorType type);

    void serial_messageIndicate(std::string message);

protected:
    EIoTCSerialPort* _serialPort;
    bool _portOpen;

    void _transmit(std::string message);
    void _transmit(std::initializer_list<std::string> list);
    void _transmitBytes(QByteArray message);

    virtual void _debugTrace(std::string message);

    virtual void handleIncomingMessages(std::string message) = 0;

    PowerControl* _power;

private:
    QEventLoop _loop;
    QTimer _timer;

};

#endif // GENERICMODEM_H
