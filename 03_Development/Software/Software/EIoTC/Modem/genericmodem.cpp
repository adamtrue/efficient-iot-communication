#include "genericmodem.h"
#include <iostream>
#include <cstdarg>

#include <QDebug>

GenericModem::GenericModem(QObject *parent) : QObject(parent), _portOpen(false)
{

}

EIoTCSerialPort *GenericModem::serialPort() const
{
    return _serialPort;
}

void GenericModem::setSerialPort(EIoTCSerialPort *serialPort)
{
    _serialPort = serialPort;

    QObject::connect(_serialPort, SIGNAL(openPortConfirm()), this, SLOT(serial_openPortConfirm()));
    QObject::connect(_serialPort, SIGNAL(closePortIndicate()), this, SLOT(serial_closePortIndicate()));
    QObject::connect(_serialPort, SIGNAL(errorIndicate(eSerialPortErrorType)), this, SLOT(serial_errorIndicate(eSerialPortErrorType)));
    QObject::connect(_serialPort, SIGNAL(messageIndicate(std::string)), this, SLOT(serial_messageIndicate(std::string)));

//    _serialPort->setup();

}

void GenericModem::startUp()
{
    QObject::connect(&_timer, &QTimer::timeout, &_loop, &QEventLoop::quit);
}

void GenericModem::startUpSerial()
{
    _serialPort->openPortRequest();
}

void GenericModem::connectLTE()
{
    modem_CFUN_onRequest();
    _wait(1000);
    // TODO wait for response
    modem_CFUN_stateRequest();
    _wait(1000);
    modem_CGSN_imeiRequest();
    _wait(1000);
    // TODO wait for response ?
    modem_CGMI_manufacturerRequest();
    _wait(1000);
    modem_CGMM_modelInfoRequest();
    _wait(1000);
    modem_CGMR_revisionInfoRequest();
    _wait(1000);
    modem_CEMODE_csPsModeRequest(); // must be 2
    _wait(1000);
//    nrf_XCBAND_currentBandRequest();
    _wait(1000);
    modem_CMEE_resultCodeRequest();
    _wait(1000);
    modem_CMEE_setResultCodeRequest(true);
    _wait(1000);
//    nrf_CNEC_getRXUnsolicitedErrorRequest();
    _wait(1000);
//    nrf_CNEC_setRXUnsolicitedErrorRequest(CNEC_EPS_ALL_ERRORS_ENABLED);
    _wait(1000);
    modem_CGEREP_getTXUnsolicitedErrorsEnableRequest();
    _wait(1000);

    // --
    modem_CGDCONT_getPDPInfoRequest();
    _wait(1000);
    modem_CGACT_getPDPActivatedRequest();
    _wait(1000);
    modem_CGEREP_setTXUnsolicitedErrorsEnableRequest(1);
    _wait(1000);
    modem_CIND_setDeviceIndicators(1,1,1);
    _wait(1000);
    modem_CEREG_setEPSRegistrationResultCodesRequest(2);
    _wait(1000);
    modem_CEREG_getEPSRegistrationStatusRequest();
    _wait(1000);
    modem_CESQ_setSignalQualityParametersRequest(1);
    _wait(1000);
    modem_CESQ_getSignalQualityParametersRequest();
    _wait(1000);
//    nrf_XSIM_subscribeToXSIMRequest();
    _wait(1000);
//    nrf_XSIM_getStatusRequest();
    _wait(1000);
    modem_CPIN_getPinUnlockTypeRequest();
    _wait(1000);
    modem_CPINR_getRemainingUnlocksRequest();
    _wait(1000);
    modem_CIMI_getIMSIRequest();
    _wait(1000);

    // --
    modem_CGDCONT_getPDPInfoRequest(); // This time we should receive a CIND service response
    _wait(1000);
    modem_CGACT_getPDPActivatedRequest();
    _wait(1000);
    modem_COPS_setPolicyRequest(MODEM_COPS_SET_ONLY, MODEM_COPS_UTRAN, "22801");
    _wait(1000);
//    nrf_XCBAND_currentBandRequest();
    _wait(1000);
    modem_CGDCONT_getPDPInfoRequest(); // This time we should receive a CIND service response
    _wait(1000);
    modem_CGACT_getPDPActivatedRequest();
}

void GenericModem::modem_CFUN_onRequest()
{
    _transmit("AT+CFUN=1\r\n");
}

void GenericModem::modem_CFUN_offRequest()
{
    _transmit("AT+CFUN=0\r\n");
}

void GenericModem::modem_CFUN_stateRequest()
{
    _transmit("AT+CFUN?\r\n");
}

void GenericModem::modem_CGSN_imeiRequest()
{
    _transmit("AT+CGSN=1\r\n");
}

void GenericModem::modem_CGMI_manufacturerRequest()
{
    _transmit("AT+CGMI\r\n");
}

void GenericModem::modem_CGMM_modelInfoRequest()
{
    _transmit("AT+CGMM\r\n");
}

void GenericModem::modem_CGMR_revisionInfoRequest()
{
    _transmit("AT+CGMR\r\n");
}

void GenericModem::modem_CEMODE_csPsModeRequest()
{
    _transmit("AT+CEMODE=?\r\n");
}

void GenericModem::modem_CMEE_setResultCodeRequest(bool enable_disable)
{
    std::string mode = enable_disable? "1":"0";
    _transmit({"AT+CMEE=",mode,"\r\n"});
}

void GenericModem::modem_CMEE_resultCodeRequest()
{
    _transmit("AT+CMEE?\r\n");
}

void GenericModem::modem_CGEREP_getTXUnsolicitedErrorsEnableRequest()
{
    _transmit("AT+CGEREP?\r\n");
}

void GenericModem::modem_CGEREP_setTXUnsolicitedErrorsEnableRequest(uint16_t mode)
{
    _transmit({"AT+CGEREP=",QString::number(mode).toStdString(),"\r\n"});
}

void GenericModem::modem_CGDCONT_getPDPInfoRequest()
{
    _transmit("AT+CGDCONT?\r\n");
}

void GenericModem::modem_CGACT_getPDPActivatedRequest()
{
    _transmit("AT+CGACT?\r\n");
}

void GenericModem::modem_CIND_setDeviceIndicators(uint16_t service, uint16_t roam, uint16_t message)
{
    _transmit({"AT+CIND=",QString::number(service).toStdString(),",",QString::number(roam).toStdString(),",",QString::number(message).toStdString(),"\r\n"});
}

void GenericModem::modem_CEREG_setEPSRegistrationResultCodesRequest(uint16_t mode)
{
    _transmit({"AT+CEREG=",QString::number(mode).toStdString(),"\r\n"});
}

void GenericModem::modem_CEREG_getEPSRegistrationStatusRequest()
{
    _transmit("AT+CEREG?\r\n");
}

void GenericModem::modem_CESQ_setSignalQualityParametersRequest(uint16_t mode)
{
    _transmit({"AT%CESQ=",QString::number(mode).toStdString(),"\r\n"});
}

void GenericModem::modem_CESQ_getSignalQualityParametersRequest()
{
    _transmit("AT+CESQ?\r\n");
}

void GenericModem::modem_CPIN_getPinUnlockTypeRequest()
{
    _transmit("AT+CPIN?\r\n");
}

void GenericModem::modem_CPINR_getRemainingUnlocksRequest()
{
    _transmit("AT+CPINR=\"SIM PIN\"\r\n");
}

void GenericModem::modem_CIMI_getIMSIRequest()
{
    _transmit("AT+CIMI\r\n");
}

void GenericModem::modem_COPS_setPolicyRequest(eModemCOPSRegistration reg, eModemCOPSAccessTechnology tech, QString operatorMMC)
{
    _transmit({"AT+COPS=",QString::number(reg).toStdString(),",",QString::number(tech).toStdString(),",\"",operatorMMC.toStdString(),"\"\r\n"});
}

void GenericModem::modem_COPS_getPolicyRequest()
{
    _transmit("AT+COPS?\r\n");
}

void GenericModem::slm_versionRequest()
{
//    _serialPort->writeRequest("AT#XSLMVER\r\n");
    _transmit("AT#XSLMVER\r\n");
}

void GenericModem::slm_commandListRequest()
{
//    _serialPort->writeRequest("AT#XCLAC\r\n");
    _transmit("AT#XCLAC\r\n");
}

void GenericModem::sleepRequest()
{
//    _serialPort->writeRequest("AT#XSLEEP=1\r\n");
    _transmit("AT#XSLEEP=1\r\n");
}

void GenericModem::idleRequest()
{
    _transmit("AT#XSLEEP=0\r\n");
}

void GenericModem::modem_changeBaudrateRequest(eModemBaudrate br)
{
    /*
    std::string message = "AT#XSLMUART=" + QString::number(br).toStdString() + "\r\n";
    _serialPort->writeRequest(message);
    _serialPort->setBaudrate(br);*/
    _transmit({"AT+IPR=",QString::number(br).toStdString(),"\r\n"});
    _serialPort->setBaudrate(br); // TODO maybe deport this to the confirmation signal?
}

void GenericModem::serial_openPortConfirm()
{
    _portOpen = true;
}

void GenericModem::serial_closePortIndicate()
{
    _portOpen = false;
}

void GenericModem::serial_errorIndicate(eSerialPortErrorType type)
{
    _debugTrace("Serial port error: ");
    _debugTrace(QString::number(type).toStdString());
    _debugTrace("\r\n");
}

void GenericModem::serial_messageIndicate(std::string message)
{
    handleIncomingMessages(message);
    _debugTrace(message);
}

void GenericModem::_transmit(std::string message)
{
    _serialPort->writeRequest(message);
    _debugTrace(message);
}

void GenericModem::_transmit(std::initializer_list<std::string> list)
{
    std::string messageToSend;
    for (std::string m: list)
    {
        messageToSend += m;
    }

    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
}

void GenericModem::_transmitBytes(QByteArray message)
{
    _serialPort->writeRequest(message);
    _debugTrace(message.toStdString());
}

void GenericModem::_debugTrace(std::string message)
{
    std::cout << "GenericModem:: " << message;
}

PowerControl *GenericModem::power() const
{
    return _power;
}

void GenericModem::setPower(PowerControl *power)
{
    _power = power;
}

void GenericModem::_wait(uint32_t time)
{
    _timer.setInterval(time);
    _timer.setSingleShot(true);
    _timer.start(time);
    _loop.exec();
//    qDebug() << "Timer done";
}
