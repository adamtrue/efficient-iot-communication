#include "eiotcserialport.h"

#include <QSerialPortInfo>
#include <QString>
#include <QDebug>
#include <QTimer>
#include <QEventLoop>

#include <iostream>

EIoTCSerialPort::EIoTCSerialPort(QObject *parent, uint32_t port_number, uint32_t baudrate, bool buffering) : QObject(parent), _portNumber(port_number), _portOpen(false), _baudrate(baudrate), _buffering(buffering)
{
//    _serial = new QSerialPort();
//    _serial->setPort(port_number);
}

void EIoTCSerialPort::setPort(uint32_t port)
{
    _portNumber = port;
}

uint32_t EIoTCSerialPort::getPort()
{
    return _portNumber;
}

uint32_t EIoTCSerialPort::getBaudrate() const
{
    return _baudrate;
}

void EIoTCSerialPort::setBaudrate(const uint32_t &baudrate)
{
    _baudrate = baudrate;

    if (_portOpen)
    {
        _serial->setBaudRate(_baudrate);
    }
    else
    {
        emit errorIndicate(SERIAL_PORT_NOT_OPEN);
    }
}


void EIoTCSerialPort::openPortRequest()
{
    // Search for the port number in the available
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        // Looking for port
        // TODO: should search by full name
        if (_checkPortNumber(info.portName(),_portNumber))
        {
            _portOpen = true;

//            qDebug() << "Name : " << info.portName();
//            qDebug() << "Port " << _portNumber << " found!";
//            qDebug() << "Description : " << info.description();
//            qDebug() << "Manufacturer: " << info.manufacturer();

            QString s;
            QTextStream ts(&s);

            std::cout << "Name : " << info.portName().toStdString() << std::endl;
            std::cout << "Port " << _portNumber << " found!" << std::endl;
            std::cout << "Description : " << info.description().toStdString() << std::endl;
            std::cout << "Manufacturer: " << info.manufacturer().toStdString() << std::endl;


            // Set up the port
            _serial = new QSerialPort();
            _serial->setPort(info);
            _serial->setBaudRate(_baudrate);

            QObject::connect(_serial, SIGNAL(readyRead()), this, SLOT(readIndicate_fromBelow()));
            QObject::connect(_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(errorIndicate_fromBelow(QSerialPort::SerialPortError)));

            _serial->open(QIODevice::ReadWrite);
            _serial->setDataTerminalReady(true);
            break;
        }
    }


    // If the port is not found send an error indication
    if(!_portOpen)
    {
        emit errorIndicate(SERIAL_PORT_NOT_FOUND);
    }

    _waitForReadyToSend(1000);
}

void EIoTCSerialPort::writeRequest(std::string message)
{
    if (_portOpen)
    {
        QByteArray message_qbyte = QByteArray::fromStdString(message);
        _serial->write(message_qbyte);
    }
    else
    {
        emit errorIndicate(SERIAL_PORT_NOT_OPEN);
    }
}

void EIoTCSerialPort::writeRequest(QByteArray message)
{
    if (_portOpen)
    {
        _serial->write(message);
    }
    else
    {
        emit errorIndicate(SERIAL_PORT_NOT_OPEN);
    }
}

void EIoTCSerialPort::readIndicate_fromBelow()
{
    if(_buffering)
        _wait(500);
    QByteArray message = _serial->readAll();
    std::string message_char = message.toStdString();
    _debugTrace(message_char);
    emit messageIndicate(message_char);
}

void EIoTCSerialPort::errorIndicate_fromBelow(QSerialPort::SerialPortError e)
{
    // TODO emit error signal errorIndicate
    _debugTrace("EIoTCSerialPort: Error - ");
    _debugTrace(e==0?"no error":QString::number(e).toStdString());
    _debugTrace("\r\n");
}

QSerialPort *EIoTCSerialPort::getSerial() const
{
    return _serial;
}

void EIoTCSerialPort::setup()
{
    _serial = new QSerialPort();
    bool success = QObject::connect(_serial, &QSerialPort::readyRead, this, &EIoTCSerialPort::readIndicate_fromBelow);
    success = QObject::connect(_serial, &QSerialPort::errorOccurred, this, &EIoTCSerialPort::errorIndicate_fromBelow);
}

void EIoTCSerialPort::_wait(uint32_t millisecs)
{
    QTimer timer;
    QEventLoop loop;

    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(millisecs);
    loop.exec();
}

void EIoTCSerialPort::_waitForReadyToSend(int milisecs)
{
    QEventLoop loop;
    QTimer timer;

    QObject::connect(this, &EIoTCSerialPort::dataTerminalReadyConfirm, &loop, &QEventLoop::quit);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(milisecs);
    loop.exec();
}

void EIoTCSerialPort::_debugTrace(std::string message)
{
    //    std::cout << message;
}

bool EIoTCSerialPort::_checkPortNumber(QString port_name, int number)
{
    // Check number is in port name
    if(port_name.contains(QString::number(number)))
    {
        // If it is "6",
        // Make sure it isn't 16, 26, 36, etc
        for(int i=0; i<10; i++)
        {
           if(port_name.contains(QString::number(i)+QString::number(number)))
           {
                return false;
           }
        }
        return true;
    }
    return false;
}

