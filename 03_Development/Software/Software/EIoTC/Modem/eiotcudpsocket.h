#ifndef EIOTCUDPSOCKET_H
#define EIOTCUDPSOCKET_H

#include <QUdpSocket>

class EIoTCUdpSocket: public QUdpSocket
{
    Q_OBJECT

public:
    EIoTCUdpSocket();

    void setLocalAddressEIoTC(QString addr);
    void setLocalPortEIoTC(uint16_t port);
};

#endif // EIOTCUDPSOCKET_H
