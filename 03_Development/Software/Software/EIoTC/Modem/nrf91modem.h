#ifndef NRF91MODEM_H
#define NRF91MODEM_H

#include "genericmodem.h"

#include <QQueue>
#include <QTimer>

#define CNEC_EPS_MOBILITY_ERRORS_ENABLED 0x08 // corresponds to error code: CNEC_EMM
#define CNEC_EPS_SESSION_ERRORS_ENABLED 0x10  // corresponds to error code: CNEC_ESM
#define CNEC_EPS_ALL_ERRORS_ENABLED (CNEC_EPS_MOBILITY_ERRORS_ENABLED || CNEC_EPS_SESSION_ERRORS_ENABLED)

typedef struct
{
    QString topic;
    QString message;
} SimpleMqttMessage;

#ifdef EIOTC_TESTING
// Both the nRF modem and the modem test class are associated, forward declaration here
class ModemTest;
#endif

class NRF91Modem: public GenericModem, public GenericMQTTInterface, public GenericFTPInterface
{
    Q_OBJECT

public:
    NRF91Modem();

    virtual void connectLTE();

    // ------------ MQTT Interface ------------- //
    virtual void mqtt_connectRequest(std::string clientId, std::string url, uint32_t port);
    virtual void mqtt_disconnectRequest();

    virtual void mqtt_subscribeRequest(std::string topic, uint32_t qos);
    virtual void mqtt_unsubscribeRequest(std::string topic);
    virtual void mqtt_publishRequest(std::string topic, eMQTTDatatype datatype, std::string msg, uint32_t qos, bool retain);
    // ------------ MQTT Interface ------------ //

    // ------------ FTP Interface ------------- //
    virtual void ftp_openRequest(std::string username, std::string password, std::string hostname, uint32_t port);
    virtual void ftp_closeRequest();
    virtual void ftp_statusRequest();

    // Operational modes
    virtual void ftp_asciiModeRequest();
    virtual void ftp_binaryModeRequest();

    // File manipulation specifics
    virtual void ftp_pwdRequest();
    virtual void ftp_cdRequest(std::string folder);
    virtual void ftp_lsRequest(std::string options, std::string folder);
    virtual void ftp_mkdirRequest(std::string folder);
    virtual void ftp_rmdirRequest(std::string folder);
    virtual void ftp_infoRequest(std::string file);
    virtual void ftp_renameRequest(std::string oldName, std::string newName);
    virtual void ftp_deleteRequest(std::string file);
    virtual void ftp_getRequest(std::string file);
    // ------------ FTP Interface ------------- //

    virtual void modem_changeBaudrateRequest(eModemBaudrate br);

    void nrf_XCBAND_currentBandRequest();
    void nrf_CNEC_getRXUnsolicitedErrorRequest();
    void nrf_CNEC_setRXUnsolicitedErrorRequest(uint16_t cnec_mode);
    void nrf_XSIM_subscribeToXSIMRequest();
    void nrf_XSIM_unsubscribeFromXSIMRequest();
    void nrf_XSIM_getStatusRequest();

    void pushTopic(QString topic);
    void popAndSubscribeToTopic();
    void checkSubscriptionsDone();
    void handleMqttData();
//    void sendControlAck();
//    void dataPublish();
//    void publishFace();
    void turnOnTimerForConnection();
    void turnOnTimer();
    void emitNrfRunning();

    void pushPublishReq(QString topic, QString message);
    void pushPublishRx(QString topic, QString message);

#ifdef EIOTC_TESTING
    void setModemTest(ModemTest *modemTest);
#endif

public slots:
    void handlePeripheralPublicFace(QString ip, int port);
    void mqttTimerDone();

signals:
    void centralPublicFaceReceived(QString ip_addr, int port);

    void mqtt_event(QString message);
    void mqtt_data(QString topic, QString data);

    void nrfRunning();

private:
    virtual void _debugTrace(std::string message);
    virtual void handleIncomingMessages(std::string message);

    QString _telitPublicIp;
    int _telitPort;

    QQueue<QString> _topicsToSubscribe;

    QQueue<SimpleMqttMessage> _rxQueue;
    QQueue<SimpleMqttMessage> _txQueue;

    QTimer* _timer;
#ifdef EIOTC_TESTING
    ModemTest* _modemTest;
#endif
};

#endif // NRF91MODEM_H
