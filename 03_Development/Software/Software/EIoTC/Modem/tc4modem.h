#ifndef TC4MODEM_H
#define TC4MODEM_H

#include "genericmodem.h"
#include "eiotcudpsocket.h"
#include "Stun/stunmessage.h"

class TC4Modem: public GenericModem
{
    Q_OBJECT
public:
    TC4Modem();

    virtual void connectLTE();

    /*
    void stunOpenUdpSocket();
    void stunSendRequest();
    void stunParse();
    */
    void sendStunRequest();

    void openUdpSocket(std::string hostname, int port, int sourcePort);
    void openUdpSocketWithCentral();
    void closeUdpConnection();
    void closeUdpSocket();
    void udpSend(std::string message);
    void holePunch();
    void holePunchAck();
    void udpSocketAvailable();
    void publicPeripheralFace();
    void shutdown();
    void transmitUdpSocket(std::string message);

    EIoTCSerialPort* getSerialPort();
/*
signals:
    void udpSocketOpened();
    void udpSocketError();
    void udpResponse();
*/

    bool getUdpSocketOpen() const;
    void setUdpSocketOpen(bool udpSocketOpen);

    EIoTCUdpSocket *getUdpSocket() const;

public slots:
    void handleCentralPublicFace(QString ip, int port);
    void multimediaDone();

signals:
    void peripheralPublicFaceReceived(QString ip, int port);
    void udpNoCarrier();
    void udpSocketReadyForMultimedia();
    void udpSocketReadyForMessages();
    void cgcontrdpResponse();

private:
    virtual void _debugTrace(std::string message);
    void _waitForUdpSocketToOpen();
    void _waitForUdpResponse();
    void _waitForNoCarrier();
    void _waitForCGCONTRDPResponse();
    virtual void handleIncomingMessages(std::string message);
    void handleIncomingUdp();
    void stunRequest(std::string hostname, int port, int sourcePort, StunMessage* stunMessage);

    void _getIPInterfaceData();
    void _runWindowsInterfaceScript();

    QByteArray udpResponseBytes;

    bool _udpSocketOpen;

    QString udpPublicIp;
    uint16_t udpPublicPort;
    uint16_t udpLocalPort;

    QString centralIp;
    uint16_t centralPort;

    // Information to use this device as an IP interface for the PC
    // Using the NCM USB profile
    QString ifIPAddress;
    QString ifGWAddress;
    QString ifDNSAddress;
    int ifPrefixLength;

    EIoTCUdpSocket* _udpSocket;
};

#endif // TC4MODEM_H
