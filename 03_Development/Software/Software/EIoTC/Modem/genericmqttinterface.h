#ifndef GENERICMQTTINTERFACE_H
#define GENERICMQTTINTERFACE_H

#include <QObject>
#include <QString>
#include <stdint.h>

// TODO integrate mqtt responses

typedef enum
{
    MQTT_HEX,
    MQTT_PLAIINTEXT,
    MQTT_JSON,
    MQTT_HTML,
    MQTT_OMA_TLV
} eMQTTDatatype;

class GenericMQTTInterface
{

public:
    virtual void mqtt_connectRequest(std::string clientId, std::string url, uint32_t port) = 0;
    virtual void mqtt_disconnectRequest() = 0;

    virtual void mqtt_subscribeRequest(std::string topic, uint32_t qos) = 0;
    virtual void mqtt_unsubscribeRequest(std::string topic) = 0;
    virtual void mqtt_publishRequest(std::string topic, eMQTTDatatype datatype, std::string msg, uint32_t qos, bool retain) = 0;


};

#endif // GENERICMQTTINTERFACE_H
