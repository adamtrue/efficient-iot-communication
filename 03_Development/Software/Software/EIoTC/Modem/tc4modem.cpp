#include "tc4modem.h"

#include <iostream>

#include <QApplication>
#include <QEventLoop>
#include <QTimer>
#include <QProcess>

#include "XF/xf.h"

TC4Modem::TC4Modem(): ifPrefixLength(8)
{
    _udpSocketOpen = false;
}

void TC4Modem::connectLTE()
{
    _power->telitOn();
    _wait(8000);    // takes 5 seconds to swtch on

    _wait(30000);    // Wait for driver to activate
    startUpSerial();
    _wait(5000);

    // Check modem is on
    _transmit("AT+CFUN?\r\n");
    _wait(1000);
    _transmit("AT+CMEE=2\r\n");
    _wait(1000);
//    _transmit("AT+COPS=0,2,\"22801\",2\r\n");
//    _transmit("AT+COPS=1,2,\"22812\"\r\n");
//    _transmit("AT+COPS=1,2\r\n");//,\"22801\"\r\n");
//    _wait(1000);
//    _transmit("AT#SGACT=1,1\r\n");
    _transmit("AT#NCM=2,1\r\n");
    _wait(5000);

    _getIPInterfaceData();
    _runWindowsInterfaceScript();
    _udpSocketOpen = true;
    XF::getInstance().pushEvent(evTelitConnected);
}

void TC4Modem::sendStunRequest()
{
    QProcess process;
    QStringList myArgs;

    // Arguments for STUN request
    myArgs.append("stun.solnet.ch");
    myArgs.append(QString::number(3478));
    myArgs.append("--localaddr");
    myArgs.append(ifIPAddress);

    // Make the stun request
    process.start("stunclient.exe", myArgs);
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    output.squeeze();

    // Get the different elements in the response
    QRegExp separator("\n|\r\n|:");
    QStringList separated = output.split(separator);

    uint16_t local_port = separated[4].toInt();
    QString mapped_address = separated[6];
    uint16_t mapped_port = separated[7].toInt();

    // Assign these to the values in class
    udpPublicIp = mapped_address;
    udpPublicPort = mapped_port;
    udpLocalPort = local_port;

    // TODO add stunresponse event
    XF::getInstance().pushEvent(evStunResponse);
}
/*
void TC4Modem::stunOpenUdpSocket()
{
    uint16_t sourcePort = 54814;
    openUdpSocket("stun.solnet.ch",3478, sourcePort);
}

void TC4Modem::stunSendRequest()
{
    StunMessage* message = new StunMessage(BindingRequest);
    message->setTransactionId(0x0231ee5,0x202111ff,0xfff0c0ce);
    StunChangeRequestAttribute* attr = new StunChangeRequestAttribute();
    attr->setChangeIp(false);
    attr->setChangePort(false);
    message->addAttribute(attr, true);
    _transmitBytes(message->exportAsBytes());
}

void TC4Modem::stunParse()
{
    StunMessage* response = StunMessage::parseFromBytes(udpResponseBytes);
    IpTuple tuple = response->getPublicDataFromResponse();
    udpPublicIp = tuple.ipAddress;
    udpPublicPort = tuple.port;

    // Check validity of IPv4 address, TODO make this condition better
    if(udpPublicIp.length() < 7)
    {
        XF::getInstance().pushEvent(evStunError);
    }
    else
    {
        XF::getInstance().pushEvent(evStunResponseGood);
    }
}

void TC4Modem::stunRequest(std::string hostname, int port, int sourcePort, StunMessage *stunMessage)
{
    openUdpSocket(hostname, port, sourcePort);
    _waitForUdpSocketToOpen();

    if(_udpSocketOpen)
    {
        _transmitBytes(stunMessage->exportAsBytes());
        _waitForUdpResponse();
        StunMessage* response = StunMessage::parseFromBytes(udpResponseBytes);
        IpTuple tuple = response->getPublicDataFromResponse();
        udpPublicIp = tuple.ipAddress;
        udpPublicPort = tuple.port;
        // This is to close the udp socket
        _transmit("+++");
        _wait(1000);
        std::cout << "My public face is :" << udpPublicIp.toStdString() << ":" << QString::number(udpPublicPort).toStdString() << std::endl;
        emit peripheralPublicFaceReceived(udpPublicIp, udpPublicPort);
    }
}
*/

void TC4Modem::_getIPInterfaceData()
{
//    +CGCONTRDP: 1,5,"shared.m2m.ch.mnc001.mcc228.gprs","10.1.46.84.255.0.0.0","10.1.
//    46.85","193.5.23.1","193.5.23.1","0.0.0.0","0.0.0.0"

    _transmit("AT+CGCONTRDP=1\r\n");
    _waitForCGCONTRDPResponse();
}

void TC4Modem::_runWindowsInterfaceScript()
{
    QProcess process;
    QStringList myArgs;

    // Arguments for STUN request
    process.setWorkingDirectory("C:\\Users\\adamt\\Desktop\\efficient-iot-communication\\03_Development\\Software\\Software\\EIoTC\\Scripts");
    myArgs.append("/C");
    myArgs.append("C:\\Users\\adamt\\Desktop\\efficient-iot-communication\\03_Development\\Software\\Software\\EIoTC\\Scripts\\telit_setup.bat");
    myArgs.append(ifIPAddress);
    myArgs.append(QString::number(ifPrefixLength));
    myArgs.append(ifGWAddress);
    myArgs.append(ifDNSAddress);

    // Make the stun request
    process.start("cmd.exe", myArgs);
    process.waitForFinished();
    _wait(1000);    // wait to connect

    qDebug() << "NW interface Finished";
}

EIoTCUdpSocket *TC4Modem::getUdpSocket() const
{
    return _udpSocket;
}

bool TC4Modem::getUdpSocketOpen() const
{
    return _udpSocketOpen;
}

void TC4Modem::setUdpSocketOpen(bool udpSocketOpen)
{
    _udpSocketOpen = udpSocketOpen;
}

void TC4Modem::_debugTrace(std::string message)
{
    std::cout << "TC4Modem:: " << message;
}

void TC4Modem::openUdpSocket(std::string hostname, int port, int sourcePort)
{
    // Open up a udp socket to a host
    _transmit({"AT#SD=1,1,",QString::number(port).toStdString(),",\""+hostname+"\",0,",QString::number(sourcePort).toStdString(),"\r\n"});
}

void TC4Modem::openUdpSocketWithCentral()
{
    // TODO better source port management
    // TODO make this an actual UDPsocket
//    _transmit({"AT#SD=1,1,",QString::number(centralPort).toStdString(),",\""+centralIp.toStdString()+"\",0,54814\r\n"});
    _udpSocket = new EIoTCUdpSocket();
//    _udpSocket->setLocalPortEIoTC(udpLocalPort);
//    _udpSocket->setLocalAddressEIoTC(ifIPAddress);
    QObject::connect(_udpSocket, &EIoTCUdpSocket::readyRead, this, &TC4Modem::handleIncomingUdp);
    _udpSocket->bind(QHostAddress(ifIPAddress), udpLocalPort);
    _udpSocket->connectToHost(centralIp, centralPort);
//    _udpSocket->open(QIODevice::ReadWrite);
    XF::getInstance().pushEvent(evTelitUdpOpen);
}

void TC4Modem::closeUdpConnection()
{
    // TODO make this an acutal UDP socket
/*    _wait(3000);
    _transmit({"+++"});*/
    _udpSocket->close();
}

void TC4Modem::closeUdpSocket()
{
    _transmit({"AT#SH=1\r\n"});
}


void TC4Modem::udpSend(std::string message)
{
    if(_udpSocketOpen)
    {
        transmitUdpSocket(message);
    }
}

void TC4Modem::holePunch()
{
    transmitUdpSocket("HOLEPUNCH");

    // TODO this is a hack to make measurements when holepunching doesn't work!
    XF::getInstance().pushEvent(evHolePunchReceived);
    XF::getInstance().pushEvent(evHolePunchAckReceived);
}

void TC4Modem::holePunchAck()
{
    transmitUdpSocket("HOLEPUNCH_ACK");
}

void TC4Modem::udpSocketAvailable()
{
//    emit udpSocketReadyForMultimedia();
    XF::getInstance().pushEvent(evUdpReadyForMessages);
    emit udpSocketReadyForMessages();
}

void TC4Modem::publicPeripheralFace()
{
    emit peripheralPublicFaceReceived(udpPublicIp, udpPublicPort);
}

void TC4Modem::shutdown()
{
    _transmit({"AT#SHDN\r\n"});
}

void TC4Modem::transmitUdpSocket(std::string message)
{
    if(_udpSocket->isOpen())
    {
        _debugTrace("UDP SENT : " + message);
        _udpSocket->write(message.c_str());//TODO maybe , message.length()
        _udpSocket->flush();
    }
}

EIoTCSerialPort* TC4Modem::getSerialPort()
{
    return _serialPort;
}

void TC4Modem::handleCentralPublicFace(QString ip, int port)
{
    centralIp = ip.simplified();
    centralPort = port;
    XF::getInstance().pushEvent(evCentralFaceReceived);
}


void TC4Modem::multimediaDone()
{
    XF::getInstance().pushEvent(evUdpOff);
}

void TC4Modem::_waitForUdpSocketToOpen()
{
    QEventLoop loop;
    QObject::connect(this, &TC4Modem::udpSocketOpened, &loop, &QEventLoop::quit);
    QObject::connect(this, &TC4Modem::udpSocketError, &loop, &QEventLoop::quit);
    loop.exec();
}

void TC4Modem::_waitForUdpResponse()
{
    QEventLoop loop;
    QObject::connect(this, &TC4Modem::udpResponse, &loop, &QEventLoop::quit);
    loop.exec();
}

void TC4Modem::_waitForNoCarrier()
{
    QEventLoop loop;
    QObject::connect(this, &TC4Modem::udpNoCarrier, &loop, &QEventLoop::quit);
    loop.exec();
}

void TC4Modem::_waitForCGCONTRDPResponse()
{
    QEventLoop loop;
    QObject::connect(this, &TC4Modem::cgcontrdpResponse, &loop, &QEventLoop::quit);
    loop.exec();
}

void TC4Modem::handleIncomingMessages(std::string message)
{
    QString qmessage = QString::fromStdString(message);
//    std::cout << qmessage.toStdString();

    // TODO make this a containment check not an equality
    if( qmessage.contains("CONNECT") )
    {
        _udpSocketOpen = true;
//        emit udpSocketOpened();
//        XF::getInstance().pushEvent(evTelitUdpOpen);
    }
    // TODO this is not an error message
    else if( qmessage.contains("ERROR") )
    {
        _udpSocketOpen = false;
//        XF::getInstance().pushEvent(evStunError);
        emit udpSocketError();
    }

    else if (qmessage.contains("NO CARRIER"))
    {
        emit udpNoCarrier();
    }
    else if(qmessage.contains("HOLEPUNCH_ACK"))
    {
        XF::getInstance().pushEvent(evHolePunchAckReceived);
    }
    else if(qmessage.contains("HOLEPUNCH"))
    {
        XF::getInstance().pushEvent(evHolePunchReceived);
    }
    // Udp messages from host
    else if(qmessage.contains("!\x12"))  // STUN magic cookie  //if(_udpSocketOpen)
    {
        udpResponseBytes = QByteArray::fromStdString(message);
//        emit udpResponse();
        XF::getInstance().pushEvent(evStunResponse);
    }
    else if(qmessage.contains("CGCONTRDP"))
    {
        // List of IP information is separated by commas and speech marks
        QRegExp r(",|\"");
        QStringList l = qmessage.split(r);

        // The IP address and mask are given at same time: a.a.a.a.m.m.m.m
        // (a is ip address and m is mask)
        // We must split these up
        QString ip_and_mask = l.at(6);
        QStringList ip_numbers = ip_and_mask.split(".");
        ifIPAddress = "";
        for(int i=0; i<4; i++)
        {
            ifIPAddress.append(ip_numbers.at(i));
            if(i!=3)
                ifIPAddress.append(".");
        }

        ifGWAddress = l.at(9);
        ifDNSAddress = l.at(12);

        emit cgcontrdpResponse();
    }
    else if(qmessage.contains("OK"))
    {
        XF::getInstance().pushEvent(evTelitOk);
    }
}

void TC4Modem::handleIncomingUdp()
{
    QString qmessage = _udpSocket->readAll();

    if(qmessage.contains("HOLEPUNCH_ACK"))
    {
        XF::getInstance().pushEvent(evHolePunchAckReceived);
    }
    else if(qmessage.contains("HOLEPUNCH"))
    {
        XF::getInstance().pushEvent(evHolePunchReceived);
    }
}
