#include "nrf91modem.h"

#include <iostream>

#include "XF/xf.h"

#ifdef EIOTC_TESTING
#include "Test/modemtest.h"
#endif

#define NRF_MQTT_TIMEOUT_ON_CONNECT 10000 // in miliseconds
#define NRF_MQTT_TIMER 500 // handle timer in miliseconds

#define MAX_MQTT_MESSAGE 280 // in bytes

NRF91Modem::NRF91Modem()
{

}

void NRF91Modem::connectLTE()
{
    _timer = new QTimer();
    QObject::connect(_timer, &QTimer::timeout, this, &NRF91Modem::mqttTimerDone);

    // TODO: NEED TO TAKE THE RESPONSES FROM THESE FUNCTIONS AND MAKE SURE THEY ONLY DO WHAT IS NECESSARY
    //  for example CGEREP answer is confirmed so no need to set it after

    _power->nrfOn();
    _wait(1000);

    modem_CFUN_onRequest();
    _wait(100);
    // TODO wait for response
    modem_CFUN_stateRequest();
    _wait(100);
    modem_CGSN_imeiRequest();
    _wait(100);
    // TODO wait for response ?
    modem_CGMI_manufacturerRequest();
    _wait(100);
    modem_CGMM_modelInfoRequest();
    _wait(100);
    modem_CGMR_revisionInfoRequest();
    _wait(100);
    modem_CEMODE_csPsModeRequest(); // must be 2
    _wait(100);
    nrf_XCBAND_currentBandRequest();
    _wait(100);
    modem_CMEE_resultCodeRequest();
    _wait(100);
    modem_CMEE_setResultCodeRequest(true);
    _wait(100);
    nrf_CNEC_getRXUnsolicitedErrorRequest();
    _wait(100);
    nrf_CNEC_setRXUnsolicitedErrorRequest(CNEC_EPS_ALL_ERRORS_ENABLED);
    _wait(100);
    modem_CGEREP_getTXUnsolicitedErrorsEnableRequest();
    _wait(100);

    // --
    modem_CGDCONT_getPDPInfoRequest();
    _wait(100);
    modem_CGACT_getPDPActivatedRequest();
    _wait(100);
    modem_CGEREP_setTXUnsolicitedErrorsEnableRequest(1);
    _wait(100);
    modem_CIND_setDeviceIndicators(1,1,1);
    _wait(100);
    modem_CEREG_setEPSRegistrationResultCodesRequest(2);
    _wait(100);
    modem_CEREG_getEPSRegistrationStatusRequest();
    _wait(100);
    modem_CESQ_setSignalQualityParametersRequest(1);
    _wait(100);
    modem_CESQ_getSignalQualityParametersRequest();
    _wait(100);
    nrf_XSIM_subscribeToXSIMRequest();
    _wait(100);
    nrf_XSIM_getStatusRequest();
    _wait(100);
    modem_CPIN_getPinUnlockTypeRequest();
    _wait(100);
    modem_CPINR_getRemainingUnlocksRequest();
    _wait(100);
    modem_CIMI_getIMSIRequest();
    _wait(100);

    // --
    modem_CGDCONT_getPDPInfoRequest(); // This time we should receive a CIND service response
    _wait(100);
    modem_CGACT_getPDPActivatedRequest();
    _wait(100);
    modem_COPS_setPolicyRequest(MODEM_COPS_MANUAL,MODEM_COPS_UTRAN,"22801");
    _wait(2000);
    nrf_XCBAND_currentBandRequest();
    _wait(100);
    modem_CGDCONT_getPDPInfoRequest(); // This time we should receive a CIND service response
    _wait(100);
    modem_CGACT_getPDPActivatedRequest();
    _wait(100);

    XF::getInstance().pushEvent(evMqttConnect);
}

void NRF91Modem::mqtt_connectRequest(std::string clientId, std::string url, uint32_t port)
{/*
    std::string messageToSend = "AT#XMQTTCON=1,\""+clientId+"\",\""+url+"\","+QString::number(port).toStdString()+"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
*/
    _transmit({"AT#XMQTTCON=1,\"",clientId,"\",\"",url,"\",",QString::number(port).toStdString(),"\r\n"});
}

void NRF91Modem::mqtt_disconnectRequest()
{
    /*
    std::string messageToSend = "AT#XMQTTCON=0\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */

    _transmit("AT#XMQTTCON=0\r\n");
}

void NRF91Modem::mqtt_subscribeRequest(std::string topic, uint32_t qos)
{
    /*
    std::string messageToSend = "AT#XMQTTSUB=\""+topic+"\","+QString::number(qos).toStdString()+"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit({"AT#XMQTTSUB=\"",topic,"\",",QString::number(qos).toStdString(),"\r\n"});
}

void NRF91Modem::mqtt_unsubscribeRequest(std::string topic)
{
    /*
    std::string messageToSend = "AT#XMQTTUNSUB=\""+topic+"\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit({"AT#XMQTTUNSUB=\"",topic,"\"\r\n"});
}

void NRF91Modem::mqtt_publishRequest(std::string topic, eMQTTDatatype datatype, std::string msg, uint32_t qos, bool retain)
{
    std::string retain_str = retain? "1":"0";
    _transmit({"AT#XMQTTPUB=\"",topic,"\",",QString::number(datatype).toStdString(),",\"",msg,"\",",QString::number(qos).toStdString(),",",retain_str,"\r\n"});
}

void NRF91Modem::ftp_openRequest(std::string username, std::string password, std::string hostname, uint32_t port)
{
    /*
    std::string messageToSend = "AT#XFTP=\"open\",\""+username+"\",\""+password+"\",\""+ hostname + "\"," + QString::number(port).toStdString() + "\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit({"AT#XFTP=\"open\",\"",username,"\",\"",password,"\",\"",hostname,"\",",QString::number(port).toStdString(),"\r\n"});
}

void NRF91Modem::ftp_closeRequest()
{
    /*
    std::string messageToSend = "AT#XFTP=\"close\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit("AT#XFTP=\"close\"\r\n");
}

void NRF91Modem::ftp_statusRequest()
{
    /*
    std::string messageToSend = "AT#XFTP=\"status\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit("AT#XFTP=\"status\"\r\n");
}

void NRF91Modem::ftp_asciiModeRequest()
{
    /*
    std::string messageToSend = "AT#XFTP=\"ascii\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit("AT#XFTP=\"ascii\"\r\n");
}

void NRF91Modem::ftp_binaryModeRequest()
{
    /*
    std::string messageToSend = "AT#XFTP=\"binary\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit("AT#XFTP=\"binary\"\r\n");
}

void NRF91Modem::ftp_pwdRequest()
{
    /*
    std::string messageToSend = "AT#XFTP=\"pwd\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit("AT#XFTP=\"pwd\"\r\n");
}

void NRF91Modem::ftp_cdRequest(std::string folder)
{
    /*
    std::string messageToSend = "AT#XFTP=\"cd\",\"" + folder +"\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit({"AT#XFTP=\"cd\",\"",folder,"\"\r\n"});
}

void NRF91Modem::ftp_lsRequest(std::string options, std::string folder)
{
    /*
    std::string messageToSend = "AT#XFTP=\"cd\",\"" + folder +"\"\r\n";
    _serialPort->writeRequest(messageToSend);
    _debugTrace(messageToSend);
    */
    _transmit({"AT#XFTP=\"ls\",\"",folder,"\"\r\n"});
}

void NRF91Modem::ftp_mkdirRequest(std::string folder)
{
    _transmit({"AT#XFTP=\"mkdir\",\"",folder,"\"\r\n"});
}

void NRF91Modem::ftp_rmdirRequest(std::string folder)
{
    _transmit({"AT#XFTP=\"rmdir\",\"",folder,"\"\r\n"});
}

void NRF91Modem::ftp_infoRequest(std::string file)
{
    _transmit({"AT#XFTP=\"info\",\"",file,"\"\r\n"});
}

void NRF91Modem::ftp_renameRequest(std::string oldName, std::string newName)
{
    _transmit({"AT#XFTP=\"rename\",\"", oldName, "\",\"", newName,"\"\r\n"});
}

void NRF91Modem::ftp_deleteRequest(std::string file)
{
    _transmit({"AT#XFTP=\"delete\",\"",file,"\"\r\n"});
}

void NRF91Modem::ftp_getRequest(std::string file)
{
    _transmit({"AT#XFTP=\"get\",\"",file,"\"\r\n"});
}

void NRF91Modem::modem_changeBaudrateRequest(eModemBaudrate br)
{
    _transmit({"AT#XSLMUART=",QString::number(br).toStdString(),"\r\n"});
    _serialPort->setBaudrate(br); // TODO maybe deport this to the confirmation signal?
}

void NRF91Modem::nrf_XCBAND_currentBandRequest()
{
    _transmit("AT%XCBAND?\r\n");
}

void NRF91Modem::nrf_CNEC_getRXUnsolicitedErrorRequest()
{
    _transmit("AT+CNEC?\r\n");
}

void NRF91Modem::nrf_CNEC_setRXUnsolicitedErrorRequest(uint16_t cnec_mode)
{
    _transmit({"AT+CNEC=",QString::number(cnec_mode).toStdString(),"\r\n"});
}

void NRF91Modem::nrf_XSIM_subscribeToXSIMRequest()
{
    _transmit("AT%XSIM=1\r\n");
}

void NRF91Modem::nrf_XSIM_unsubscribeFromXSIMRequest()
{
    _transmit("AT%XSIM=0\r\n");
}

void NRF91Modem::nrf_XSIM_getStatusRequest()
{
    _transmit("AT%XSIM?\r\n");
}

void NRF91Modem::pushTopic(QString topic)
{
    _topicsToSubscribe.enqueue(topic);
}

void NRF91Modem::popAndSubscribeToTopic()
{
    if(_topicsToSubscribe.isEmpty())
    {
        XF::getInstance().pushEvent(evMqttSubscribedDone);
    }
    else
    {
        QString nextTopic = _topicsToSubscribe.dequeue();
        mqtt_subscribeRequest(nextTopic.toStdString(), 0);
    }
}

void NRF91Modem::checkSubscriptionsDone()
{
    if(_topicsToSubscribe.isEmpty())
    {
        XF::getInstance().pushEvent(evMqttSubscribedDone);
    }
    else
    {
        XF::getInstance().pushEvent(evMqttSubscribedNotDone);
    }
}

void NRF91Modem::handleMqttData()
{
    while(!_rxQueue.empty())
    {
        SimpleMqttMessage mqtt_message = _rxQueue.dequeue();
        QString poppedTopic = mqtt_message.topic;
        QString poppedMessage = mqtt_message.message;

        if(poppedTopic.contains("adamtopic/central/public_face"))
        {
            QStringList list = poppedMessage.split(":");
            QString ip_address = list.at(0);
            int port = list.at(1).toInt();

    //        _debugTrace("Public address done: "+ip_address.toStdString()+":"+QString::number(port).toStdString());

            emit centralPublicFaceReceived(ip_address, port);   // This transmits it to Telit modem

            XF::getInstance().pushEvent(evCentralFaceReceived);
//            XF::getInstance().pushEvent(evMqttMsgDone);
        }

        //else if(poppedTopic.contains("adamtopic/peripheral/control"))
        else if(poppedTopic.contains("adamtopic/peripheral/control"))
        {
            if(poppedMessage.contains("STREAM0"))
            {
                XF::getInstance().pushEvent(evUdpOff);
//                XF::getInstance().pushEvent(evMqttMsgDone);
            }
            else if(poppedMessage.contains("STREAM1"))
            {
                XF::getInstance().pushEvent(evTelitWakeup);
//                XF::getInstance().pushEvent(evMqttMsgDone);
            }
            // TOOD finish
            else if(poppedMessage.contains("something"))
            {
                // TODO do the action
                // doAction()
//                XF::getInstance().pushEvent(evMqttActionDone);
            }
            else if(poppedMessage.contains("DATA"))
            {
//                XF::getInstance().pushEvent(evMqttDataRequest);
            }
        }
    }

    while(!_txQueue.empty())
    {
        SimpleMqttMessage mqtt_message = _txQueue.dequeue();
        QString poppedTopic = mqtt_message.topic;
        QString poppedMessage = mqtt_message.message;

        // Perform segmentation
        if(poppedMessage.length() > MAX_MQTT_MESSAGE)
        {
            // Plus one because it will round down and we want all the message
            uint16_t amount_of_splits = poppedMessage.length() / MAX_MQTT_MESSAGE + 1;
            QString split_message;

            // Calculate and Send each segment of the string
            for(int i=0; i<amount_of_splits; i+=1)
            {
                split_message = poppedMessage;   // = operator is overloaded by QString

                // Remove the end of the string
                split_message.remove( (i+1) * MAX_MQTT_MESSAGE, split_message.length());
                // Remove the start of the string
                split_message.remove(0,i*MAX_MQTT_MESSAGE);

                // Segmentation is as follows, separated into MAX_MQTT size packets

                // start | -seg- |  end         |
                // ----- | XXXXX | ----- | -----|

                mqtt_publishRequest(poppedTopic.toStdString(), MQTT_PLAIINTEXT, split_message.toStdString(), 0, false);
                _wait(10);
            }
        }
        else
        {
            mqtt_publishRequest(poppedTopic.toStdString(), MQTT_PLAIINTEXT, poppedMessage.toStdString(), 0, false);
        }
#ifdef EIOTC_TESTING
        if(poppedTopic == "adamtopic/peripheral/data")
            _modemTest->registerNrfTime();
#endif

        _wait(500);
        XF::getInstance().pushEvent(evMqttPublished);
    }

    XF::getInstance().pushEvent(evMqttHandled);
}

void NRF91Modem::turnOnTimerForConnection()
{
    _timer->setSingleShot(true);
    _timer->start(NRF_MQTT_TIMEOUT_ON_CONNECT);
}

void NRF91Modem::turnOnTimer()
{
    _timer->setSingleShot(true);
    _timer->start(NRF_MQTT_TIMER);
}

void NRF91Modem::emitNrfRunning()
{
    emit nrfRunning();
}

/*
void NRF91Modem::sendControlAck()
{
    mqtt_publishRequest("adamtopic/peripheral/control_ack", MQTT_PLAIINTEXT, "ACK", 0, false);
}

void NRF91Modem::dataPublish()
{
    mqtt_publishRequest("adamtopic/peripheral/data", MQTT_PLAIINTEXT, "DATA", 0, false);
}

void NRF91Modem::publishFace()
{
    mqtt_publishRequest("adamtopic/peripheral/public_face", MQTT_PLAIINTEXT, _telitPublicIp.toStdString()+":"+QString::number(_telitPort).toStdString(), 0, false);
}*/

void NRF91Modem::pushPublishReq(QString topic, QString message)
{
    SimpleMqttMessage msg = {topic, message};
    _txQueue.enqueue(msg);
}

void NRF91Modem::pushPublishRx(QString topic, QString message)
{
    SimpleMqttMessage msg = {topic, message};
    _rxQueue.enqueue(msg);
}

void NRF91Modem::handlePeripheralPublicFace(QString ip, int port)
{
//    QString msg = ip + ":" + QString::number(port);
//    mqtt_publishRequest("adamtopic/peripheral/public_face", MQTT_PLAIINTEXT, msg.toStdString(), 0, false);
    _telitPort = port;
    _telitPublicIp = ip;
    pushPublishReq("adamtopic/peripheral/public_face", _telitPublicIp+":"+QString::number(_telitPort));
    //    XF::getInstance().pushEvent(evPublishFace);
}

void NRF91Modem::mqttTimerDone()
{
    XF::getInstance().pushEvent(evMqttTimeout);
}

void NRF91Modem::_debugTrace(std::string message)
{
    std::cout << message;
}

void NRF91Modem::handleIncomingMessages(std::string message)
{
    // TODO
    QString m = QString::fromStdString(message);
    if(m.contains("#XMQTTMSG"))
    {
        QStringList lines = m.split("#XMQTTMSG");
        QStringList lines2 = lines.at(1).split("\r\n");
        SimpleMqttMessage simple_msg;
        simple_msg.topic = lines2.at(1);
        simple_msg.message = lines2.at(2);

        // Add message to stack
        _rxQueue.enqueue(simple_msg);
    }
    else if(m.contains("#XMQTTEVT"))
    {
        if(m.contains("0,0"))
        {
            XF::getInstance().pushEvent(evMqttConnected);
        }
        else if(m.contains("1,0"))
        {
            XF::getInstance().pushEvent(evMqttDisconnected);
        }
        else if(m.contains("7,0"))
        {
            XF::getInstance().pushEvent(evMqttSubscribeConfirm);
        }
    }
    else if(m.contains("OK"))
    {
//        XF::getInstance().pushEvent(evNrfOk);
    }
    else if(m.contains("ERROR"))
    {
        XF::getInstance().pushEvent(evMqttError);
    }
}

#ifdef EIOTC_TESTING
void NRF91Modem::setModemTest(ModemTest *modemTest)
{
    _modemTest = modemTest;
}
#endif

