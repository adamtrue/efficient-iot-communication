#ifndef EIOTCSERIALPORT_H
#define EIOTCSERIALPORT_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include <QEventLoop>

typedef enum
{
    SERIAL_NO_ERROR,
    SERIAL_PORT_NOT_OPEN,
    SERIAL_PORT_NOT_FOUND,
    SERIAL_PORT_DISCONNECTED
} eSerialPortErrorType;

class EIoTCSerialPort : public QObject
{
    Q_OBJECT
public:
    explicit EIoTCSerialPort(QObject *parent = nullptr, uint32_t port_number = 3, uint32_t baudrate = 115200, bool buffering=true);

    void setPort(uint32_t port);
    uint32_t getPort();

    uint32_t getBaudrate() const;
    void setBaudrate(const uint32_t &baudrate);

    QSerialPort *getSerial() const;

    void setup();

public slots:
    void openPortRequest();
    void writeRequest(std::string message);
    void writeRequest(QByteArray message);

    void readIndicate_fromBelow();
    void errorIndicate_fromBelow(QSerialPort::SerialPortError e);

signals:
    void openPortConfirm();
    void closePortIndicate();
    void errorIndicate(eSerialPortErrorType type);

    void messageIndicate(std::string message);

    void dataTerminalReadyConfirm();

private:
    QSerialPort* _serial;
    uint32_t _portNumber;
    bool _portOpen;
    uint32_t _baudrate;
    bool _buffering;

    void _wait(uint32_t millisecs);
    void _waitForReadyToSend(int milisecs);
    void _debugTrace(std::string message);

    bool _checkPortNumber(QString port_name, int number);
};

#endif // EIOTCSERIALPORT_H
