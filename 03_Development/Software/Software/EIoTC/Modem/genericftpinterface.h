#ifndef GENERICFTPINTERFACE_H
#define GENERICFTPINTERFACE_H

#include <QString>
#include <stdint.h>

// TODO integrate FTP official responses

class GenericFTPInterface
{
public:
    // On/Off
    // TODO <sec_tag>
    virtual void ftp_openRequest(std::string username, std::string password, std::string hostname, uint32_t port) = 0;
    virtual void ftp_closeRequest() = 0;
    virtual void ftp_statusRequest() = 0;

    // Operational modes
    virtual void ftp_asciiModeRequest() = 0;
    virtual void ftp_binaryModeRequest() = 0;

    // File manipulation specifics
    virtual void ftp_pwdRequest() = 0;
    virtual void ftp_cdRequest(std::string folder) = 0;
    virtual void ftp_lsRequest(std::string options, std::string folder) = 0;
    virtual void ftp_mkdirRequest(std::string folder) = 0;
    virtual void ftp_rmdirRequest(std::string folder) = 0;
    virtual void ftp_infoRequest(std::string file) = 0;
    virtual void ftp_renameRequest(std::string oldName, std::string newName) = 0;
    virtual void ftp_deleteRequest(std::string file) = 0;
    virtual void ftp_getRequest(std::string file) = 0;
//    virtual void ftp_putRequest(std::string file, std::string datatype, std::string data) = 0;

};

#endif // GENERICFTPINTERFACE_H
