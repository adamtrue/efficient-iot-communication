#include "eiotcudpsocket.h"

EIoTCUdpSocket::EIoTCUdpSocket()
{

}

void EIoTCUdpSocket::setLocalAddressEIoTC(QString addr)
{
    setLocalAddress(QHostAddress(addr));
}

void EIoTCUdpSocket::setLocalPortEIoTC(uint16_t port)
{
    setLocalPort(port);
}
