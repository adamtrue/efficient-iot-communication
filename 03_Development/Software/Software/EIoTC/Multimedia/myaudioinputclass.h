#ifndef MYAUDIOINPUTCLASS_H
#define MYAUDIOINPUTCLASS_H

#include <QObject>
#include <QAudioInput>
#include <QIODevice>
#include <QFile>
#include <QDebug>
#include <QTimer>

class MyAudioInputClass: public QObject
{
    Q_OBJECT

public:
    MyAudioInputClass();

    void setIoDevice(QIODevice *ioDevice);

    void initAudioRecordDevice();

signals:
    void multimediaFinished();

public slots:
    void startRecording();
    void stopRecording();
    void handleStateChanged(QAudio::State newState);

private:
    QAudioInput* _audio;
    QIODevice* _ioDevice;
};

#endif // MYAUDIOINPUTCLASS_H
