#include "myaudioinputclass.h"

#include <QAudioDeviceInfo>

MyAudioInputClass::MyAudioInputClass()
{

}

void MyAudioInputClass::initAudioRecordDevice()
{
    QAudioFormat format;
    // Set up the desired format, for example:
    format.setSampleRate(8000);
    format.setChannelCount(1);
    format.setSampleSize(8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::UnSignedInt);

    const auto deviceInfos = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);
    QAudioDeviceInfo myDevice;
    for (const QAudioDeviceInfo &deviceInfo : deviceInfos)
    {
        qDebug() << "Device name: " << deviceInfo.deviceName();

        if(deviceInfo.deviceName().contains("Micro"))
        {
            myDevice = deviceInfo;
        }

        qDebug() << "Device format: " << deviceInfo.preferredFormat();
    }

    _audio = new QAudioInput(myDevice, myDevice.preferredFormat(), this);
    connect(_audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleStateChanged(QAudio::State)));
}


void MyAudioInputClass::startRecording()
{
    QTimer::singleShot(10000, this, SLOT(stopRecording()));
    _ioDevice->blockSignals(true);  // Block signals during media transmit
    _audio->start(_ioDevice);
    // Records audio for 3000ms
}

void MyAudioInputClass::stopRecording()
{
    _audio->stop();
    _ioDevice->blockSignals(false);
//    _ioDevice->close();
//    delete _audio;

    emit multimediaFinished();

//    qDebug() << "Stopped recording";
}

void MyAudioInputClass::handleStateChanged(QAudio::State newState)
{
    switch (newState)
    {
        case QAudio::StoppedState:
            if (_audio->error() != QAudio::NoError) {
                // Error handling
            } else {
                // Finished recording
            }
            break;

        case QAudio::ActiveState:
            // Started recording - read from IO device
            break;

        default:
            // ... other cases as appropriate
            break;
    }
}

void MyAudioInputClass::setIoDevice(QIODevice *ioDevice)
{
    _ioDevice = ioDevice;
}

