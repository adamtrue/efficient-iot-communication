#ifndef XF_H
#define XF_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QQueue>

#include "genericstatemachine.h"

// Define the repeat period of the XF, in miliseconds
#define XF_PERIOD 700

class XF : public QObject
{
    Q_OBJECT

private:
    explicit XF(QObject *parent = nullptr);

public:
    static XF& getInstance();
    void start();
    void addStateMachine(GenericStateMachine* sm);
    void pushEvent(XFEventEnum e);

signals:

public slots:
    void executeXF();

private:
    QTimer* _timer;
    QVector<GenericStateMachine*> _stateMachines;
    QQueue<XFEventEnum> _events;
};

#endif // XF_H
