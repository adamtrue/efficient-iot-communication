#include "xf.h"

XF::XF(QObject *parent) : QObject(parent)
{

}

XF &XF::getInstance()
{
    static XF xf;

    return xf;
}


void XF::start()
{
    _timer = new QTimer();
    _timer->setInterval(XF_PERIOD);
    QObject::connect(_timer, &QTimer::timeout, &(getInstance()), &XF::executeXF);
    _timer->start(XF_PERIOD);
}

void XF::addStateMachine(GenericStateMachine *sm)
{
    _stateMachines.append(sm);
}

void XF::pushEvent(XFEventEnum e)
{
    _events.enqueue(e);
}

void XF::executeXF()
{
    if(!_events.isEmpty())
    {
        XFEventEnum e = _events.dequeue();

        for(auto s:_stateMachines)
        {
            s->processEvent(e);
        }
    }
}
