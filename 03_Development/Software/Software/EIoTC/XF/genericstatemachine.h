#ifndef GENERICSTATEMACHINE_H
#define GENERICSTATEMACHINE_H

#include "xfevent.h"

class GenericStateMachine
{
public:

    virtual void processEvent(XFEventEnum event) = 0;
};

#endif // GENERICSTATEMACHINE_H
