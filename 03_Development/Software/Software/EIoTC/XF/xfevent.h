#ifndef XFEVENT_H
#define XFEVENT_H

#include "genericstatemachine.h"

typedef enum
{
    evDefault,
    evTelitWakeup,
    evTelitConnected,
    evTelitUdpOpen,
    evTelitOk,
    evStunResponse,
    evCentralFaceReceived,
    evHolePunchReceived,
    evHolePunchAckReceived,
    evUdpReadyForMessages,
    evUdpOff,
    evTelitShutdown,
    evNrfWakeup,
    evNrfOk,
    evMqttConnect,
    evMqttConnected,
    evMqttDisconnected,
    evMqttError,
    evMqttSubscribeConfirm,
    evMqttSubscribedDone,
    evMqttSubscribedNotDone,
    evMqttMsg,
    evMqttNewData,
    evMqttDataRequest,
    evMqttActionDone,
    evMqttMsgDone,
    evPublishFace,
    evMqttTimeout,
    evMqttHandled,
    evMqttPublished,
    evStartTests,
    evTestsTimeout,
    evTestsDone
} XFEventEnum;


#endif // XFEVENT_H
