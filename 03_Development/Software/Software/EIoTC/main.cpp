#include <QApplication>
#include <QTimer>
#include <QEventLoop>

#include <iostream>

//#include "Modem/eiotcserialport.h"
#include "eiotcfactory.h"
#include "Modem/genericmqttinterface.h"
#include "Stun/stunchangerequestattribute.h"

void wait(uint32_t time)
{
    QTimer timer;
    QEventLoop loop;

    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(time);
    loop.exec();
}

void mqtt_test(NRF91Modem* modem)
{
    wait(5000);
    modem->mqtt_connectRequest("clientId-MyRandomClientIDDDD","broker.hivemq.com", 1883);
    wait(5000);
    modem->mqtt_subscribeRequest("adamtopic/seventeen",0);
    wait(5000);
    modem->mqtt_publishRequest("adamtopic/eighteen",MQTT_PLAIINTEXT, "hello my test", 0, false);
    wait(5000);
    modem->mqtt_unsubscribeRequest("adamtopic/seventeen");
    wait(5000);
    modem->mqtt_disconnectRequest();
}

void ftp_test(NRF91Modem* modem)
{
    wait(5000);
    modem->ftp_openRequest("demo","password","test.rebex.net", 21);
    wait(5000);
    modem->ftp_lsRequest("","");
    wait(5000);
    modem->ftp_cdRequest("pub/example");
    wait(5000);
    modem->ftp_lsRequest("","");
}

#ifdef STUN_TESTS
void stunMessageTest(TC4Modem* modem)
{
    uint16_t sourcePort = 54814;
    uint16_t destPort = 61842;

    StunMessage* message = new StunMessage(BindingRequest);
    message->setTransactionId(0x1231005,0x20201122,0xeeffcaca);
    StunChangeRequestAttribute* attr = new StunChangeRequestAttribute();
    attr->setChangeIp(false);
    attr->setChangePort(false);
    message->addAttribute(attr, true);
    modem->stunRequest("stun.solnet.ch",3478,sourcePort, message);
    modem->_wait(60000);    // Wait for 2 minutes for me to establish connection
    modem->openUdpSocket("153.109.2.89", destPort, sourcePort);


    int i=0;
    QEventLoop loop;
    QTimer timer;
    timer.setInterval(5000);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    while(1)
    {
        modem->udpSend("World " + QString::number(i).toStdString());
        i++;
        timer.start();
        loop.exec();
    }
}

void udpHolePunchingTest(NRF91Modem* nrf_modem, TC4Modem* tc4_modem)
{
    wait(5000);
    nrf_modem->mqtt_connectRequest("clientId-MyRandomClientIDDDD","broker.hivemq.com", 1883);
    wait(5000);
    nrf_modem->mqtt_subscribeRequest("hevs_ch/eiotc/udp_holepunching/pc_face",0);
/*    wait(5000);
    nrf_modem->mqtt_publishRequest("hevs_ch/eiotc/udp_holepunching/modem_face",MQTT_PLAIINTEXT, "hello my test", 0, false);
    wait(5000);
    nrf_modem->mqtt_unsubscribeRequest("adamtopic/seventeen");
    wait(5000);
    nrf_modem->mqtt_disconnectRequest();
*/

    wait(5000);


    uint16_t sourcePort = 54814;
    uint16_t destPort = 61842;

    StunMessage* message = new StunMessage(BindingRequest);
    message->setTransactionId(0x0231ee5,0x202111ff,0xfff0c0ce);
    StunChangeRequestAttribute* attr = new StunChangeRequestAttribute();
    attr->setChangeIp(false);
    attr->setChangePort(false);
    message->addAttribute(attr, true);
    tc4_modem->stunRequest("stun.solnet.ch",3478,sourcePort, message);
    tc4_modem->_wait(60000);    // Wait for 2 minutes for me to establish connection

    /*
    int i=0;
    QEventLoop loop;
    QTimer timer;
    timer.setInterval(5000);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    while(1)
    {
        tc4_modem->udpSend("World " + QString::number(i).toStdString());
        i++;
        timer.start();
        loop.exec();
    }*/
}
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    EIoTCFactory* factory = new EIoTCFactory();

    factory->build();
    factory->link();

    Arbiter* arbiter = factory->arbiter();

    factory->startUp();
    factory->waitNrfRunning();

    // Test small messages
    arbiter->sendMessage("Small Test", true, false);
    wait(1000);

    // Test big messages
    QString myString;
    for(int i=0; i<ARBITER_THRESHOLD; i++)
    {
        myString.append("ab");
    }
    arbiter->sendMessage(myString, false, false);
    wait(1000);

    // Test buffering messages
    for(int i=0; i<ARBITER_THRESHOLD; i++)
    {
        arbiter->sendMessage("a", false, false);
    }

    wait(1000);
/*
    NRF91Modem* nrf_modem = factory->nrfModem();
    TC4Modem* tc4_modem = factory->tc4Modem();

//    NRF91Modem* modem = factory->nrfModem();
//    modem->connectLTE();
//    mqtt_test(modem);
//    ftp_test(modem);

//    factory->tc4Modem()->connectLTE();
//    stunMessageTest(factory->tc4Modem());

//    nrf_modem->connectLTE();
//    tc4_modem->connectLTE();
//    udpHolePunchingTest(nrf_modem, tc4_modem);


    nrf_modem->connectLTE();
    tc4_modem->connectLTE();
    nrf_modem->mqtt_connectRequest("Bloofy1721", "broker.hivemq.com", 1883);

    wait(1000);

    nrf_modem->mqtt_subscribeRequest("adamtopic/peripheral/control",0);
    wait(1000);
    nrf_modem->mqtt_subscribeRequest("adamtopic/central/public_face",0);

    wait(1000);
*/

    return a.exec();
}
