#ifndef STUNATTRIBUTE_H
#define STUNATTRIBUTE_H

#include <stdint.h>

#include <QByteArray>
#include <QVector>

typedef enum
{
    StunAttrNone,
    StunMappedIp,
    StunResponseAddr,
    StunChangeReq,
    StunSourceAddress,
    StunChangedAddress,
    StunUsername,
    StunPassword,
    StunMessageIntegrity,
    StunErrorCode,
    StunUnknownAttr = 0x000a,
    StunReflectedFrom = 0x000b,
}
StunAttributeType;

class StunAttribute
{
public:
    StunAttribute(StunAttributeType type, uint16_t length);
    QByteArray exportAsBytes();

    uint16_t attrLength() const;
    void setAttrLength(const uint16_t &attrLength);

    StunAttributeType attrType() const;
    void setAttrType(const StunAttributeType &attrType);

    void addByte(uint8_t byte);

    QVector<uint8_t> attrBytes() const;

protected:
    StunAttributeType _attrType;
    uint16_t _attrLength;
    QVector<uint8_t> _attrBytes;
};

#endif // STUNATTRIBUTE_H
