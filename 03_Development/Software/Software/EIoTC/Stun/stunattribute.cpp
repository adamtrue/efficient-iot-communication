#include "stunattribute.h"

StunAttribute::StunAttribute(StunAttributeType type, uint16_t length): _attrType(type), _attrLength(length)
{
}

QByteArray StunAttribute::exportAsBytes()
{
    QByteArray result;

    // Attribute type
    result.append(_attrType>>8);
    result.append(_attrType&0xff);
    // Attribute length
    result.append(_attrLength>>8);
    result.append(_attrLength&0xff);

    // Attribute data
    for (auto i: _attrBytes)
    {
        result.append(i);
    }


    return result;
}

uint16_t StunAttribute::attrLength() const
{
    return _attrLength;
}

void StunAttribute::setAttrLength(const uint16_t &attrLength)
{
    _attrLength = attrLength;
}

StunAttributeType StunAttribute::attrType() const
{
    return _attrType;
}

void StunAttribute::setAttrType(const StunAttributeType &attrType)
{
    _attrType = attrType;
}

void StunAttribute::addByte(uint8_t byte)
{
    _attrBytes.append(byte);
}

QVector<uint8_t> StunAttribute::attrBytes() const
{
    return _attrBytes;
}
