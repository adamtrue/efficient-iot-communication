#include "stunmessage.h"

#include <iostream>

StunMessage::StunMessage(StunMessageType type): _messageType(type)
{
    _messageLength = 0;
}

QByteArray StunMessage::exportAsBytes()
{
    QByteArray result;
    // Message type
    result.append(_messageType>>8);
    result.append(_messageType&0xff);
    // Message length
    result.append(_messageLength>>8);
    result.append(_messageLength&0xff);
    // Magic cookie
    result.append(0x21);
    result.append(0x12);
    result.append(0xa4);
    result.append(0x42);

    // TODO TRANSIT ID
    for(int i=0; i<3; i++)
    {
        result.append(_transactionId[i]>>24);
        result.append((_transactionId[i]>>16) & 0xff);
        result.append((_transactionId[i]>>8) & 0xff);
        result.append(_transactionId[i] & 0xff);
    }

    // Attributes
    for (auto i:_attributes)
    {
        result.append(i->exportAsBytes());
    }

    return result;
}

void StunMessage::addAttribute(StunAttribute *attr, bool incrementLength)
{
    _attributes.append(attr);

    // Message length includes everything after transaction id

    // Attribute length + 2 bytes for attribute type and 2 bytes for attribute length
    if(incrementLength)
        _messageLength += attr->attrLength()+4;
}

void StunMessage::setTransactionId(uint32_t high, uint32_t mid, uint32_t low)
{
    _transactionId[0] = high;
    _transactionId[1] = mid;
    _transactionId[2] = low;
}

StunMessage *StunMessage::parseFromBytes(QByteArray bytes)
{
    // First we get the messageType
    uint16_t length = bytes.length();
    uint16_t messageType = 0;
    // Message type
    messageType = ((uint16_t)bytes[0])<<8;
    messageType|= bytes[1];

    StunMessage* result = new StunMessage((StunMessageType)messageType);

    // Message length
    uint16_t messageLength = 0;
    messageLength = ((uint16_t)bytes[2])<<8;
    messageLength|= bytes[3];
    result->setMessageLength(messageLength);

    // Magic cookie, takes up bytes [4-7]
    /*result.append(0x21);
    result.append(0x12);
    result.append(0xa4);
    result.append(0x42);*/

    // Transaction ID
    uint32_t transitId[] = {0,0,0};
    uint8_t index = 8;
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<4; j++)
        {
            transitId[i] |= bytes[index] << (24-j*8);
            index++;
        }
    }
    result->setTransactionId(transitId[0], transitId[1], transitId[2]);

    // Parse the attributes
    uint8_t currentAttrIndex = index; // index should be 20 here
    uint8_t totalLength = index + messageLength;

    uint16_t attrType = 0;
    uint16_t attrLength = 0;

    // Pull out all attributes
    while(currentAttrIndex < totalLength)
    {
        // 2 bytes at start of attribute for type
        attrType = ((uint16_t)bytes[currentAttrIndex])<<8;
        attrType |= bytes[currentAttrIndex + 1];
        // 2 bytes for length
        attrLength = ((uint16_t)bytes[currentAttrIndex + 2])<<8;
        attrLength |= bytes[currentAttrIndex + 3];
        StunAttribute* attr = new StunAttribute((StunAttributeType)attrType, attrLength);

        // Go through all the rest of the bytes
        for(int j=0; j<attrLength; j++)
        {
            attr->addByte(bytes[currentAttrIndex+4+j]);
        }

        // Add the attribute to current message
        // Don't increment the length because we have parsed the length
        result->addAttribute(attr, false);

        // Go to next attribute, 2 bytes for type and length
        currentAttrIndex += attrLength+4;
    }

    return result;
}

uint16_t StunMessage::messageLength() const
{
    return _messageLength;
}

void StunMessage::setMessageLength(const uint16_t &messageLength)
{
    _messageLength = messageLength;
}

IpTuple StunMessage::getPublicDataFromResponse()
{
    QString ipAddress;
    uint16_t port;

    // Look for mapped address attributes
    for(auto a:_attributes)
    {
        // If there is a mapped address update values
        if(a->attrType() == StunMappedIp)
        {
            // If the address is not Ipv4 TODO throw error
            if(a->attrBytes()[1] != 0x1)
            {
                std::cout << "NOT IPv4 : there is a problem"<<std::endl;
                return {"",0};
            }
            // Get port
            port = 0;
            port= a->attrBytes()[2]<<8;
            port |= a->attrBytes()[3];

            // Get ip address
            ipAddress = QString::number(a->attrBytes()[4])+"."+
                    QString::number(a->attrBytes()[5])+"." +
                    QString::number(a->attrBytes()[6])+"." +
                    QString::number(a->attrBytes()[7]);

            return {ipAddress, port};
        }
    }
    return {"",0};
}
