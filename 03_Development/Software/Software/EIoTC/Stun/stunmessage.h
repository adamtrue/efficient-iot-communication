#ifndef STUNMESSAGE_H
#define STUNMESSAGE_H

#include <stdint.h>
#include "stunattribute.h"

#include <QByteArray>
#include <QVector>


#define STUN_ERROR 0x0010
#define STUN_RESPONSE 0x0100
#define STUN_BINDING_REQUEST 0x0001
#define STUN_SECRET_REQUEST 0x0002

typedef enum
{
    BindingRequest = STUN_BINDING_REQUEST,                                      // 0x0001
    SecretRequest = STUN_SECRET_REQUEST,                                        // 0x0002
    BindingResponse = STUN_RESPONSE | STUN_BINDING_REQUEST,                     // 0x0101
    SecretResponse = STUN_RESPONSE | STUN_SECRET_REQUEST,                       // 0x0102
    BindingErrorResponse = STUN_RESPONSE | STUN_ERROR | STUN_BINDING_REQUEST,   // 0x0111
    SecretErrorResponse = STUN_RESPONSE | STUN_ERROR | STUN_SECRET_REQUEST      // 0x0112
} StunMessageType;

typedef struct
{
    QString ipAddress;
    uint16_t port;
} IpTuple;

class StunMessage
{
public:
    StunMessage(StunMessageType type);

    QByteArray exportAsBytes();
    // When creating a message, set incrementLength to true, when parsing a message set it to false
    void addAttribute(StunAttribute* attr, bool incrementLength);
    void setTransactionId(uint32_t high, uint32_t mid, uint32_t low);

    static StunMessage* parseFromBytes(QByteArray bytes);

    uint16_t messageLength() const;
    void setMessageLength(const uint16_t &messageLength);

    IpTuple getPublicDataFromResponse();

private:
    StunMessageType _messageType;
    uint16_t _messageLength;
    //    const uint32_t _messageCookie = 0x2112a442;
    uint32_t _transactionId[3];
    QVector<StunAttribute*> _attributes;
};

#endif // STUNMESSAGE_H
