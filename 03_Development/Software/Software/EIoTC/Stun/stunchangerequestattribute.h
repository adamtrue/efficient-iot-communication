#ifndef STUNCHANGEREQUESTATTRIBUTE_H
#define STUNCHANGEREQUESTATTRIBUTE_H

#include <QByteArray>
#include "stunattribute.h"

class StunChangeRequestAttribute: public StunAttribute
{
public:
    StunChangeRequestAttribute();

    bool changeIp() const;
    void setChangeIp(bool changeIp);

    bool changePort() const;
    void setChangePort(bool changePort);

private:
    bool _changeIp;
    bool _changePort;

    void _updateBytes();
};

#endif // STUNCHANGEREQUESTATTRIBUTE_H
