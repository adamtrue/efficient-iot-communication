#include "stunchangerequestattribute.h"

StunChangeRequestAttribute::StunChangeRequestAttribute(): StunAttribute(StunChangeReq, 4)
{
    _changeIp = false;
    _changePort = false;
    // This type of message has 4 bytes
    _attrBytes.append(0x00);
    _attrBytes.append(0x00);
    _attrBytes.append(0x00);
    _attrBytes.append(0x00);
}

bool StunChangeRequestAttribute::changeIp() const
{
    return _changeIp;
}

void StunChangeRequestAttribute::setChangeIp(bool changeIp)
{
    _changeIp = changeIp;
    _updateBytes();
}

bool StunChangeRequestAttribute::changePort() const
{
    return _changePort;
}

void StunChangeRequestAttribute::setChangePort(bool changePort)
{
    _changePort = changePort;
    _updateBytes();
}

void StunChangeRequestAttribute::_updateBytes()
{
    if(_changeIp)
    {
        // RFC 3489 - "Change ip" is bit 29 (large byte first so 0 instead of 1)
        _attrBytes[0] = 0x00 | 0x20;
    }
    if(_changePort)
    {
        // RFC 3489 - "Change port" is bit 30
        _attrBytes[0] = 0x00 | 0x40;
    }
}
