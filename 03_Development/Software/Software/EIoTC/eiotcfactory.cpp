#include "eiotcfactory.h"

#include "XF/xf.h"

EIoTCFactory::EIoTCFactory(QObject *parent) : QObject(parent)
{

}

void EIoTCFactory::build()
{
    // Build the different serial port interfaces
    _nrfSerialPort = new EIoTCSerialPort(nullptr, 6);
    _tc4SerialPort = new EIoTCSerialPort(nullptr, 31);
    _hardwareSerialPort = new EIoTCSerialPort(nullptr, 10, 115200, false);

    // Build modem interfaces
    _nrfModem = new NRF91Modem();
    _tc4Modem = new TC4Modem();

    _powerControl = new PowerControl();

    // Build state machines
    _nrfSM = new NrfStateMachine();
    _telitSM = new TelitStateMachine();

    // Build multimedia device
//    _audioRecorder = new MyAudioInputClass();

    // Build GUI
    _powerChartGUI = new PowerGUI();
    _powerData = new PowerData();

    _energyCalculator = new EnergyCalculator();

#ifdef EIOTC_TESTING
    _modemTest = new ModemTest();
#endif

    _arbiter = new Arbiter(ARBITER_THRESHOLD);

    _waitTilStartLoop = new QEventLoop();
}

void EIoTCFactory::link()
{
    _nrfModem->setSerialPort(_nrfSerialPort);
    _tc4Modem->setSerialPort(_tc4SerialPort);
    _powerControl->setSerialPort(_hardwareSerialPort);

    _nrfModem->setPower(_powerControl);
    _tc4Modem->setPower(_powerControl);

    _nrfSM->setModem(_nrfModem);
    _telitSM->setModem(_tc4Modem);

    _powerControl->setGui(_powerChartGUI);
    _powerControl->setData(_powerData);
    _powerChartGUI->setData(_powerData);
    _powerData->setEnergyCalculator(_energyCalculator);
    _powerChartGUI->setEnergyCalculator(_energyCalculator);
//    _modemTest->setCalculator(_energyCalculator);
#ifdef EIOTC_TESTING
    _modemTest->setNrfModem(_nrfModem);
    _modemTest->setTc4Modem(_tc4Modem);
    _nrfModem->setModemTest(_modemTest);
#endif
    _arbiter->setNrfModem(_nrfModem);
    _arbiter->setTc4Modem(_tc4Modem);

    QObject::connect(_nrfModem, SIGNAL(mqtt_data(QString, QString)), _nrfModem, SLOT(handle_mqtt_data(QString, QString)));
    QObject::connect(_nrfModem, &NRF91Modem::centralPublicFaceReceived, _tc4Modem, &TC4Modem::handleCentralPublicFace);
    QObject::connect(_tc4Modem, &TC4Modem::peripheralPublicFaceReceived, _nrfModem, &NRF91Modem::handlePeripheralPublicFace);
//    QObject::connect(_tc4Modem, &TC4Modem::udpSocketReadyForMultimedia, _audioRecorder, &MyAudioInputClass::startRecording);
//    QObject::connect(_audioRecorder, &MyAudioInputClass::multimediaFinished, _tc4Modem, &TC4Modem::multimediaDone);
    QObject::connect(_nrfModem, &NRF91Modem::nrfRunning, _waitTilStartLoop, &QEventLoop::quit);
//    QObject::connect(_powerControl, &PowerControl::nrfDataReceived, _powerChartGUI, &PowerGUI::receiveNrfData);
//    QObject::connect(_powerControl, &PowerControl::telitDataReceived, _powerChartGUI, &PowerGUI::receiveTelitData);
}

void EIoTCFactory::startUp()
{
    _nrfModem->startUp();
    _nrfModem->startUpSerial();
    _tc4Modem->startUp();
//    _tc4Modem->startUpSerial();
    _powerControl->startUp();

    _nrfModem->pushTopic("adamtopic/peripheral/control");
    _nrfModem->pushTopic("adamtopic/central/public_face");

//    _audioRecorder->setIoDevice(_tc4SerialPort->getSerial());
//    _modemTest->setUdpIoDevice(_tc4SerialPort->getSerial());
//    _audioRecorder->initAudioRecordDevice();
#ifdef EIOTC_TESTING
    _modemTest->build();
#endif
    _powerChartGUI->build();
    _powerControl->powerMeasureOnRequest();

    XF::getInstance().addStateMachine(_nrfSM);
    XF::getInstance().addStateMachine(_telitSM);

#ifdef EIOTC_TESTING
    XF::getInstance().addStateMachine(_modemTest);
#endif

    XF::getInstance().pushEvent(evDefault);
    XF::getInstance().pushEvent(evNrfWakeup);
    XF::getInstance().start();
}

NRF91Modem *EIoTCFactory::nrfModem() const
{
    return _nrfModem;
}

void EIoTCFactory::setNrfModem(NRF91Modem *nrfModem)
{
    _nrfModem = nrfModem;
}

TC4Modem *EIoTCFactory::tc4Modem() const
{
    return _tc4Modem;
}

void EIoTCFactory::setTc4Modem(TC4Modem *tc4Modem)
{
    _tc4Modem = tc4Modem;
}

void EIoTCFactory::waitNrfRunning()
{
    _waitTilStartLoop->exec();
}

Arbiter *EIoTCFactory::arbiter() const
{
    return _arbiter;
}
