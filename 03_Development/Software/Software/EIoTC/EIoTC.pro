QT += core
QT += widgets
#QT += charts
QT += gui
QT += serialport
QT += multimedia
QT += testlib

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += EIOTC_TESTING

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += eiotcfactory.h \
           Hardware/energycalculator.h \
           Hardware/hardware_constants.h \
           Hardware/powercontrol.h \
           Hardware/powerdata.h \
           Hardware/powergui.h \
           Modem/eiotcudpsocket.h \
           Modem/genericftpinterface.h \
           Modem/genericmodem.h \
           Modem/eiotcserialport.h \
           Modem/genericmqttinterface.h \
           Modem/nrf91modem.h \
           Modem/tc4modem.h \
           Multimedia/myaudioinputclass.h \
           StateMachines/nrfstatemachine.h \
           StateMachines/telitstatemachine.h \
           Stun/stunattribute.h \
           Stun/stunchangerequestattribute.h \
           Stun/stunmessage.h \
           Test/modemtest.h \
           XF/genericstatemachine.h \
           XF/xf.h \
           XF/xfevent.h \
           arbiter.h

SOURCES += \
        Hardware/energycalculator.cpp \
        Hardware/powercontrol.cpp \
        Hardware/powerdata.cpp \
        Hardware/powergui.cpp \
        Modem/eiotcudpsocket.cpp \
        Modem/nrf91modem.cpp \
        Modem/tc4modem.cpp \
        Multimedia/myaudioinputclass.cpp \
        StateMachines/nrfstatemachine.cpp \
        StateMachines/telitstatemachine.cpp \
        Stun/stunattribute.cpp \
        Stun/stunchangerequestattribute.cpp \
        Stun/stunmessage.cpp \
        Test/modemtest.cpp \
        XF/xf.cpp \
        arbiter.cpp \
        main.cpp \
        eiotcfactory.cpp \
        Modem/genericmodem.cpp \
        Modem/eiotcserialport.cpp

RC_FILE=res.rc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
