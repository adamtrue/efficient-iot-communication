#ifndef ARBITER_H
#define ARBITER_H

#include <QString>
#include <QVector>
#include <QEventLoop>
#include "Modem/nrf91modem.h"
#include "Modem/tc4modem.h"

class Arbiter
{
public:
    Arbiter(uint32_t threshold);

    void sendMessage(QString message, bool urgent, bool multimedia);

    NRF91Modem *nrfModem() const;
    void setNrfModem(NRF91Modem *nrfModem);

    TC4Modem *tc4Modem() const;
    void setTc4Modem(TC4Modem *tc4Modem);

    uint32_t threshold() const;
    void setThreshold(const uint32_t &threshold);

private:
    NRF91Modem* _nrfModem;
    TC4Modem* _tc4Modem;
    uint32_t _threshold;
    QEventLoop* _telitWaitLoop;
    QVector<QString> _messageBuffer;

    void _waitTelitWakeup();
};

#endif // ARBITER_H
