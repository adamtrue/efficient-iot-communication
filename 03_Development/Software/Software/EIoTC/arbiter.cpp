#include "arbiter.h"
#include "XF/xf.h"

Arbiter::Arbiter(uint32_t threshold): _threshold(threshold)
{

}

void Arbiter::sendMessage(QString message, bool urgent, bool multimedia)
{
    if(urgent)
    {
        if(multimedia)
        {
            // TODO deal with multimedia
        }
        else
        {
            _nrfModem->pushPublishReq("adamtopic/peripheral/data", message);
        }
    }
    else
    {
        if(message.length() < _threshold)
        {
            _messageBuffer.append(message);

            // Check if the message buffer reaches the threshold
            uint32_t size = 0;

            for(auto m: _messageBuffer)
            {
                size += m.length();
            }

            if(size >= _threshold)
            {
                XF::getInstance().pushEvent(evTelitWakeup);
                _waitTelitWakeup();
                for(auto m: _messageBuffer)
                {
                    _tc4Modem->udpSend(m.toStdString());
                }
                _messageBuffer.clear();
                XF::getInstance().pushEvent(evTelitShutdown);
            }
        }
        else
        {
            XF::getInstance().pushEvent(evTelitWakeup);
            _waitTelitWakeup();
            _tc4Modem->udpSend(message.toStdString());
            XF::getInstance().pushEvent(evTelitShutdown);
        }
    }
}

NRF91Modem *Arbiter::nrfModem() const
{
    return _nrfModem;
}

void Arbiter::setNrfModem(NRF91Modem *nrfModem)
{
    _nrfModem = nrfModem;
}

TC4Modem *Arbiter::tc4Modem() const
{
    return _tc4Modem;
}

void Arbiter::setTc4Modem(TC4Modem *tc4Modem)
{
    _telitWaitLoop = new QEventLoop();
    _tc4Modem = tc4Modem;
    QObject::connect(_tc4Modem, &TC4Modem::udpSocketReadyForMessages, _telitWaitLoop, &QEventLoop::quit);
}

uint32_t Arbiter::threshold() const
{
    return _threshold;
}

void Arbiter::setThreshold(const uint32_t &threshold)
{
    _threshold = threshold;
}

void Arbiter::_waitTelitWakeup()
{
    _telitWaitLoop->exec();
}
