#ifndef EIOTCFACTORY_H
#define EIOTCFACTORY_H

#include <QObject>

#include "Modem/eiotcserialport.h"
#include "Modem/nrf91modem.h"
#include "Modem/tc4modem.h"

#include "StateMachines/nrfstatemachine.h"
#include "StateMachines/telitstatemachine.h"

#include "Multimedia/myaudioinputclass.h"

#include "Hardware/powercontrol.h"
#include "Hardware/powergui.h"
#include "Hardware/powerdata.h"
#include "Hardware/energycalculator.h"

#include "Test/modemtest.h"

#include "arbiter.h"

#define ARBITER_THRESHOLD 13250

class EIoTCFactory : public QObject
{
    Q_OBJECT
public:
    explicit EIoTCFactory(QObject *parent = nullptr);

    void build();
    void link();
    void startUp();

    NRF91Modem *nrfModem() const;
    void setNrfModem(NRF91Modem *nrfModem);

    TC4Modem *tc4Modem() const;
    void setTc4Modem(TC4Modem *tc4Modem);

    void waitNrfRunning();

    Arbiter *arbiter() const;

signals:

private:
    EIoTCSerialPort* _nrfSerialPort;
    EIoTCSerialPort* _tc4SerialPort;
    EIoTCSerialPort* _hardwareSerialPort;

    NRF91Modem* _nrfModem;
    TC4Modem* _tc4Modem;

    NrfStateMachine* _nrfSM;
    TelitStateMachine* _telitSM;

    MyAudioInputClass* _audioRecorder;

    PowerControl* _powerControl;
    PowerGUI* _powerChartGUI;
    PowerData* _powerData;
    EnergyCalculator* _energyCalculator;

#ifdef EIOTC_TESTING
    ModemTest* _modemTest;
#endif

    Arbiter* _arbiter;

    QEventLoop* _waitTilStartLoop;
};

#endif // EIOTCFACTORY_H
