#ifndef POWERDATA_H
#define POWERDATA_H

#include <QVector>
#include "energycalculator.h"

// Maximum 500 points of data for both modems
#define MAX_DATA_SIZE 500

class PowerData
{
public:
    PowerData();

    void nrfAppend(int data);
    void nrfRepeatLast();
    void telitAppend(int data);
    void telitRepeatLast();

    QVector<double> nrfData() const;
    QVector<double> telitData() const;

    EnergyCalculator *energyCalculator() const;
    void setEnergyCalculator(EnergyCalculator *energyCalculator);

private:
    QVector<double> _nrfData;
    QVector<double> _telitData;

    double _translateValue(int value, bool is_nrf);

    EnergyCalculator* _energyCalculator;
};

#endif // POWERDATA_H
