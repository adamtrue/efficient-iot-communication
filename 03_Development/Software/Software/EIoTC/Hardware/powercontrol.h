#ifndef POWERCONTROL_H
#define POWERCONTROL_H

#include "Modem/eiotcserialport.h"

#include "powergui.h"
#include "powerdata.h"

class PowerControl: public QObject
{
    Q_OBJECT

public:
    PowerControl();

    EIoTCSerialPort *serialPort() const;
    void setSerialPort(EIoTCSerialPort *serialPort);

    void startUp();

    void nrfOn();
    void nrfOff();
    void telitOn();
    void powerMeasureOnRequest();
    void powerMeasureOffRequest();

    PowerGUI *gui() const;
    void setGui(PowerGUI *gui);

    PowerData *data() const;
    void setData(PowerData *data);

public slots:
    void serial_openPortConfirm();
    void serial_closePortIndicate();
    void serial_errorIndicate(eSerialPortErrorType e);
    void serial_messageIndicate(std::string m);

signals:
    void nrfDataReceived(int data);
    void telitDataReceived(int data);

private:
    EIoTCSerialPort* _serialPort;
    PowerGUI* _gui;
    PowerData* _data;
    bool _portOpen;

    void _wait(int time);
};

#endif // POWERCONTROL_H
