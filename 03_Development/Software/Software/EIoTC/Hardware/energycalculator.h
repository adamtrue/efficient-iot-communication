#ifndef ENERGYCALCULATOR_H
#define ENERGYCALCULATOR_H

class EnergyCalculator
{
public:
    EnergyCalculator();

    void addNrfValue(float val);
    void addTelitValue(float val);

    double nrfAccumulator() const;
    double telitAccumulator() const;

private:
    double _nrfAccumulator;
    double _telitAccumulator;

    double _calculateEnergy(double value, double time);

};

#endif // ENERGYCALCULATOR_H
