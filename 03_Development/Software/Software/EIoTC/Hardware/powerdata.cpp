#include "powerdata.h"

#include "hardware_constants.h"

PowerData::PowerData()
{
    for(int i=0; i<MAX_DATA_SIZE; i++)
    {
        _nrfData.append(0);
        _telitData.append(0);
    }
}

void PowerData::nrfAppend(int data)
{
    double power = _translateValue(data, true);
    _nrfData.append(power);
    if(_nrfData.size() > MAX_DATA_SIZE)
    {
        _nrfData.remove(0);
    }
}

void PowerData::nrfRepeatLast()
{
    float last = _nrfData.at(_nrfData.length()-1);
    _energyCalculator->addNrfValue(last);
    _nrfData.append(last);
    if(_nrfData.size() > MAX_DATA_SIZE)
    {
        _nrfData.remove(0);
    }
}

void PowerData::telitAppend(int data)
{
    float power = _translateValue(data, false);
    _telitData.append(power);
    if(_telitData.size() > MAX_DATA_SIZE)
    {
        _telitData.remove(0);
    }
}

void PowerData::telitRepeatLast()
{
    float last = _telitData.at(_telitData.length()-1);
    _energyCalculator->addTelitValue(last);
    _telitData.append(last);
    if(_telitData.size() > MAX_DATA_SIZE)
    {
        _telitData.remove(0);
    }
}

double PowerData::_translateValue(int value, bool is_nrf)
{
    int correctedValue;
    double voltage;
    double correctedVoltage;
    double shuntVoltage;
    double current;
    double power;

    if(is_nrf)
    {
        correctedValue = value+NRF_ADC_VALUE_STATIC_CORRECTION;
        voltage = correctedValue * V_REF / ADC_MAX_VALUE_UNSIGNED;
        correctedVoltage = voltage - V_REF/2.0;
        shuntVoltage = correctedVoltage/NRF_AMP + NRF_VOLTAGE_STATIC_CORRECTION;
        current = shuntVoltage/NRF_RESISTANCE + NRF_STATIC_CURRENT_CORRECTION;
        power = current * NRF_VOLTAGE;
        _energyCalculator->addNrfValue(power);
    }
    else
    {
        correctedValue = value+TELIT_ADC_VALUE_STATIC_CORRECTION;
        voltage = correctedValue * V_REF / ADC_MAX_VALUE_UNSIGNED - TELIT_VOLTAGE_STATIC_CORRECTION;
        correctedVoltage = voltage - V_REF/2.0;
        shuntVoltage = correctedVoltage/TELIT_AMP + TELIT_VOLTAGE_STATIC_CORRECTION;
        current = shuntVoltage/(TELIT_RESISTANCE);
        power = current * TELIT_VOLTAGE;
        _energyCalculator->addTelitValue(power);
    }

    return power;
}

EnergyCalculator *PowerData::energyCalculator() const
{
    return _energyCalculator;
}

void PowerData::setEnergyCalculator(EnergyCalculator *energyCalculator)
{
    _energyCalculator = energyCalculator;
}

QVector<double> PowerData::nrfData() const
{
    return _nrfData;
}

QVector<double> PowerData::telitData() const
{
    return _telitData;
}

