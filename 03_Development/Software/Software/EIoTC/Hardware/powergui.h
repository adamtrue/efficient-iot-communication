#ifndef POWERGUI_H
#define POWERGUI_H

#include "powerdata.h"

#include <QMainWindow>
//#include <QChartView>
//#include <QLineSeries>
#include <QTimer>
//#include <QValueAxis>

// in milliseconds
#define REFRESH_TIME 100
/*
#define EIOTC_MAX_Y_AXIS 2047

#define EIOTC_MIN_Y_AXIS -2048
*/


class PowerGUI : public QMainWindow
{
    Q_OBJECT
public:
    explicit PowerGUI(QWidget *parent = nullptr);

    void build();

    PowerData *data() const;
    void setData(PowerData *data);

    EnergyCalculator *energyCalculator() const;
    void setEnergyCalculator(EnergyCalculator *energyCalculator);

signals:

public slots:
    void updateChart();

private:
    PowerData* _data;
/*
    QtCharts::QLineSeries* _nrfSeries;
    QtCharts::QLineSeries* _telitSeries;
    QtCharts::QChartView* _chartView;
    QtCharts::QValueAxis* x_axis;
    QtCharts::QValueAxis* y_axis;*/
    bool _started;

    float _nrfCount;
    float _telitCount;

    QTimer* _timer;

    EnergyCalculator* _energyCalculator;
};

#endif // POWERGUI_H
