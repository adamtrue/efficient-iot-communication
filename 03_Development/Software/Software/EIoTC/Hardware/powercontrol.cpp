#include "powercontrol.h"

#include "hardware_constants.h"

#include <iostream>
#include <QEventLoop>
#include <QTimer>

PowerControl::PowerControl(): _portOpen(false)
{

}

EIoTCSerialPort *PowerControl::serialPort() const
{
    return _serialPort;
}

void PowerControl::setSerialPort(EIoTCSerialPort *serial)
{
    _serialPort = serial;

    QObject::connect(_serialPort, SIGNAL(openPortConfirm()), this, SLOT(serial_openPortConfirm()));
    QObject::connect(_serialPort, SIGNAL(closePortIndicate()), this, SLOT(serial_closePortIndicate()));
    QObject::connect(_serialPort, SIGNAL(errorIndicate(eSerialPortErrorType)), this, SLOT(serial_errorIndicate(eSerialPortErrorType)));
    QObject::connect(_serialPort, SIGNAL(messageIndicate(std::string)), this, SLOT(serial_messageIndicate(std::string)));
}

void PowerControl::startUp()
{
    _serialPort->openPortRequest();
}

void PowerControl::nrfOn()
{
    _serialPort->writeRequest(QByteArray("B"));
}

void PowerControl::nrfOff()
{
    _serialPort->writeRequest(QByteArray("C"));
}

void PowerControl::telitOn()
{
    _serialPort->writeRequest(QByteArray("A"));
}

void PowerControl::powerMeasureOnRequest()
{
//    _serialPort->writeRequest(QByteArray("P")); // Start power request
}

void PowerControl::powerMeasureOffRequest()
{
    _serialPort->writeRequest(QByteArray("S"));
}

void PowerControl::serial_openPortConfirm()
{
    _portOpen = true;
}

void PowerControl::serial_closePortIndicate()
{
    _portOpen = false;
}

void PowerControl::serial_errorIndicate(eSerialPortErrorType e)
{
    std::cout << "Hardware serial error: " << e << std::endl;
}

void PowerControl::serial_messageIndicate(std::string m)
{
    QString msg = QString::fromStdString(m);
    msg.remove("\0");
    QRegExp exp("\r\n");
    QRegExp n_exp("N:");
    QRegExp t_exp("T:");
    QStringList list = msg.split(exp);
    QStringList n_list;
    QStringList t_list;

    for(auto i: list)
    {
        if(i.contains("N:"))
        {
            n_list = i.split(n_exp);
            if(n_list.at(1).length() == 6)
            {
                _data->nrfAppend(n_list.at(1).toInt());
            }
            else
            {
                // If data is not correctly received then repeat the last value
                _data->nrfRepeatLast();
            }
            //_gui->receiveNrfData(n_list.at(1).toInt());
        }
        else if (i.contains("T:"))
        {
            t_list = i.split(t_exp);
            if(t_list.at(1).length() == 6)
            {
                _data->telitAppend(t_list.at(1).toInt());
            }
            else
            {
                // if data is not correctly received then repeat last value received
                _data->telitRepeatLast();
            }
            //_gui->receiveTelitData(t_list.at(1).toInt());
        }
    }

//    std::cout << m;
}

PowerData *PowerControl::data() const
{
    return _data;
}

void PowerControl::setData(PowerData *data)
{
    _data = data;
}

PowerGUI *PowerControl::gui() const
{
    return _gui;
}

void PowerControl::setGui(PowerGUI *gui)
{
    _gui = gui;
}

void PowerControl::_wait(int time)
{
    QTimer timer;
    QEventLoop loop;
    timer.setInterval(time);
    timer.setSingleShot(true);
    QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.start();
    loop.exec();
}
