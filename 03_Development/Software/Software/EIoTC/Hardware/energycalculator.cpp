#include "energycalculator.h"

#include "hardware_constants.h"

EnergyCalculator::EnergyCalculator()
{

}

void EnergyCalculator::addNrfValue(float val)
{
    _nrfAccumulator += _calculateEnergy(val, X_AVG_INCREMENT);
}

void EnergyCalculator::addTelitValue(float val)
{

    _telitAccumulator += _calculateEnergy(val, X_AVG_INCREMENT);
}

double EnergyCalculator::nrfAccumulator() const
{
    return _nrfAccumulator;
}

double EnergyCalculator::telitAccumulator() const
{
    return _telitAccumulator;
}

double EnergyCalculator::_calculateEnergy(double value, double time)
{
    double absolute_value = value>0?value:-value;

    // Filter out noise
    if(absolute_value < NOISE_TOLERANCE)
    {
        return 0.0;
    }

    // time is in miliseconds
    return value * time/1000.0;
}
