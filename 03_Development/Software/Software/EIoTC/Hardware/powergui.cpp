#include "powergui.h"

#include "hardware_constants.h"

//using namespace QtCharts;

PowerGUI::PowerGUI(QWidget *parent) : QMainWindow(parent), _started(false), _nrfCount(0), _telitCount(0)
{

}

void PowerGUI::build()
{
//    _nrfSeries = new QLineSeries();
//    _telitSeries = new QLineSeries();
//    _chartView = new QChartView();

//    _chartView->chart()->addSeries(_nrfSeries);
//    _chartView->chart()->addSeries(_telitSeries);

}

void PowerGUI::updateChart()
{
/*
    _nrfSeries->clear();
    _telitSeries->clear();

    _nrfCount = 0;
    _telitCount = 0;

    for(auto i: _data->nrfData())
    {
        _nrfSeries->append(_nrfCount, i);
        _nrfCount += X_ONE_INCREMENT;
    }
    for(auto i: _data->telitData())
    {
        _telitSeries->append(_telitCount, i);
        _telitCount += X_ONE_INCREMENT;
    }
*/

}

EnergyCalculator *PowerGUI::energyCalculator() const
{
    return _energyCalculator;
}

void PowerGUI::setEnergyCalculator(EnergyCalculator *energyCalculator)
{
    _energyCalculator = energyCalculator;
}

PowerData *PowerGUI::data() const
{
    return _data;
}

void PowerGUI::setData(PowerData *data)
{
    _data = data;
}
