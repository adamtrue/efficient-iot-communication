#ifndef NRFSTATEMACHINE_H
#define NRFSTATEMACHINE_H

#include "XF/genericstatemachine.h"
#include "Modem/nrf91modem.h"

#include <QQueue>

typedef enum
{
    ST_NRF_DEFAULT,
    ST_NRF_OFF,
    ST_NRF_CONNECT,
    ST_NRF_MQTT_CONNECT,
    ST_NRF_MQTT_RETRY,
    ST_NRF_MQTT_SUB,
    ST_NRF_MQTT_SUB_CONF,
    ST_NRF_RUNNING,
    ST_NRF_MQTT_HANDLE
} NrfState;

class NrfStateMachine: public GenericStateMachine
{
public:
    NrfStateMachine();

    virtual void processEvent(XFEventEnum event);

    void setModem(NRF91Modem *modem);

private:
    NrfState _currentState;
    NRF91Modem* _modem;

    QQueue<NrfState> _stateStackForDebug;
};

#endif // NRFSTATEMACHINE_H
