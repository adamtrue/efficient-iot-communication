#ifndef TELITSTATEMACHINE_H
#define TELITSTATEMACHINE_H

#include "XF/genericstatemachine.h"

#include "Modem/tc4modem.h"

typedef enum
{
    ST_TELIT_DEFAULT,
    ST_TELIT_OFF,
    ST_TELIT_CONNECT,
    ST_TELIT_STUN_REQ,
    ST_PUBLISH_PERIPHERAL_FACE,
    ST_UDP_START,
    ST_UDP_HOLE_PUNCH,
    ST_UDP_HOLE_PUNCH_ACK,
    ST_UDP_AVAILABLE,
    ST_UDP_OFF,
    ST_TELIT_SHUTDOWN
} TelitState;

class TelitStateMachine: public GenericStateMachine
{
public:
    TelitStateMachine();

    virtual void processEvent(XFEventEnum event);

    void setModem(TC4Modem *modem);

private:
    TelitState _currentState;

    TC4Modem* _modem;
};

#endif // TELITSTATEMACHINE_H
