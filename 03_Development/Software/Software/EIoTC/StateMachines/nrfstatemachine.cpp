#include "nrfstatemachine.h"

#include "XF/xf.h"

#include <QDebug>

NrfStateMachine::NrfStateMachine(): _currentState(ST_NRF_DEFAULT)
{

}

void NrfStateMachine::processEvent(XFEventEnum event)
{
    NrfState oldState = _currentState;

    // Double switch pattern

    switch(event)
    {
    case evDefault:
        if(_currentState == ST_NRF_DEFAULT)
        {
            _currentState = ST_NRF_OFF;
        }
        break;

    case evNrfWakeup:
        if(_currentState == ST_NRF_OFF)
        {
            _currentState = ST_NRF_CONNECT;
        }
        break;

    case evMqttConnect:
        if(_currentState == ST_NRF_CONNECT)
        {
            _currentState = ST_NRF_MQTT_CONNECT;
        }
        break;

    case evMqttError:
        if(_currentState == ST_NRF_MQTT_CONNECT)
        {
            _currentState = ST_NRF_MQTT_RETRY;
        }
        break;

    case evMqttConnected:
        if(_currentState == ST_NRF_MQTT_CONNECT)
        {
            _currentState = ST_NRF_MQTT_SUB;
        }
        break;

    case evMqttDisconnected:
        if(_currentState == ST_NRF_MQTT_RETRY)
        {
            _currentState = ST_NRF_MQTT_CONNECT;
        }
        break;

    case evMqttSubscribeConfirm:
        if(_currentState == ST_NRF_MQTT_SUB)
        {
            _currentState = ST_NRF_MQTT_SUB_CONF;
        }
        break;

    case evMqttSubscribedDone:
        if(_currentState == ST_NRF_MQTT_SUB_CONF)
        {
            _currentState = ST_NRF_RUNNING;
        }
        else if(_currentState == ST_NRF_MQTT_SUB)
        {
            _currentState = ST_NRF_RUNNING;
        }
        break;

    case evMqttSubscribedNotDone:
        if(_currentState == ST_NRF_MQTT_SUB_CONF)
        {
            _currentState = ST_NRF_MQTT_SUB;
        }
        break;

    case evMqttTimeout:
        if(_currentState == ST_NRF_MQTT_CONNECT)
        {
            _currentState = ST_NRF_MQTT_RETRY;
        }
        else if(_currentState == ST_NRF_RUNNING)
        {
            _currentState = ST_NRF_MQTT_HANDLE;
        }
        break;

    case evMqttHandled:
        if(_currentState == ST_NRF_MQTT_HANDLE)
        {
            _currentState = ST_NRF_RUNNING;
        }

    default:
        break;
    }

    // TODO Actions on entry
    if(_currentState != oldState)
    {
        _stateStackForDebug.enqueue(_currentState);
        switch(_currentState)
        {
        case ST_NRF_DEFAULT:
            break;

        case ST_NRF_OFF:
            break;

        case ST_NRF_CONNECT:
            _modem->connectLTE();
            break;

        case ST_NRF_MQTT_CONNECT:
            _modem->mqtt_connectRequest("xx3_CelineDion", "broker.hivemq.com", 1883);
            _modem->turnOnTimerForConnection();
            break;

        case ST_NRF_MQTT_RETRY:
            _modem->mqtt_disconnectRequest();
            break;

        case ST_NRF_MQTT_SUB:
            _modem->popAndSubscribeToTopic();
            break;

        case ST_NRF_MQTT_SUB_CONF:
            _modem->checkSubscriptionsDone();
            break;

        case ST_NRF_RUNNING:
            _modem->turnOnTimer();
            _modem->emitNrfRunning();
            break;

        case ST_NRF_MQTT_HANDLE:
            _modem->handleMqttData();
            break;

        default:
            break;
        }
        qDebug() << "Nrf state: " << _currentState;
    }
}

void NrfStateMachine::setModem(NRF91Modem *modem)
{
    _modem = modem;
}
