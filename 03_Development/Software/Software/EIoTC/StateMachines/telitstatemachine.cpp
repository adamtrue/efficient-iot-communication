#include "telitstatemachine.h"

#include <QDebug>

#include "XF/xf.h"

TelitStateMachine::TelitStateMachine(): _currentState(ST_TELIT_DEFAULT)
{

}

void TelitStateMachine::processEvent(XFEventEnum event)
{
    TelitState oldState = _currentState;

    // Double switch pattern

    switch(event)
    {
    case evDefault:
        if(_currentState == ST_TELIT_DEFAULT)
        {
            _currentState = ST_TELIT_OFF;
        }
        break;

    case evTelitWakeup:
        if(_currentState == ST_TELIT_OFF)
        {
            _currentState = ST_TELIT_CONNECT;
        }
        break;
    case evTelitConnected:
        if(_currentState == ST_TELIT_CONNECT)
        {
            _currentState = ST_TELIT_STUN_REQ;
        }
        break;

    case evTelitUdpOpen:
        if(_currentState == ST_UDP_START)
        {
            _currentState = ST_UDP_HOLE_PUNCH;
        }
        break;

    case evTelitOk:
        if(_currentState == ST_TELIT_SHUTDOWN)
        {
            _currentState = ST_TELIT_OFF;
        }
        break;

    case evStunResponse:
        if(_currentState == ST_TELIT_STUN_REQ)
        {
            _currentState = ST_PUBLISH_PERIPHERAL_FACE;
        }
        break;

    case evCentralFaceReceived:
        if(_currentState == ST_PUBLISH_PERIPHERAL_FACE)
        {
            _currentState = ST_UDP_START;
        }

    case evHolePunchReceived:
        if(_currentState == ST_UDP_HOLE_PUNCH)
        {
            _currentState = ST_UDP_HOLE_PUNCH_ACK;
        }
        break;

    case evHolePunchAckReceived:
        if(_currentState == ST_UDP_HOLE_PUNCH_ACK)
        {
            _currentState = ST_UDP_AVAILABLE;
        }
        break;

    case evUdpOff:
        if(_currentState == ST_UDP_AVAILABLE)
        {
            _currentState = ST_UDP_OFF;
        }
        break;

    case evTelitShutdown:
        _currentState = ST_TELIT_SHUTDOWN;
        break;

    default:
        break;
    }

    // TODO Actions on entry
    if(_currentState != oldState)
    {
        switch(_currentState)
        {
        case ST_TELIT_DEFAULT:
            break;

        case ST_TELIT_OFF:
            _modem->setUdpSocketOpen(false);
            break;

        case ST_TELIT_CONNECT:
            _modem->connectLTE();
            break;

        case ST_TELIT_STUN_REQ:
            _modem->sendStunRequest();
            break;

        case ST_PUBLISH_PERIPHERAL_FACE:
            _modem->publicPeripheralFace();
            break;

        case ST_UDP_START:
            _modem->openUdpSocketWithCentral();
            break;

        case ST_UDP_HOLE_PUNCH:
            _modem->holePunch();
            break;

        case ST_UDP_HOLE_PUNCH_ACK:
            _modem->holePunchAck();
            break;

        case ST_UDP_AVAILABLE:
            _modem->udpSocketAvailable();
            break;

        case ST_UDP_OFF:
            _modem->closeUdpConnection();
            XF::getInstance().pushEvent(evTelitShutdown);
            break;

        case ST_TELIT_SHUTDOWN:
            _modem->shutdown();
            break;

        default:
            break;
        }

        qDebug() << "Telit state: "<<_currentState;
    }

}

void TelitStateMachine::setModem(TC4Modem *modem)
{
    _modem = modem;
}
