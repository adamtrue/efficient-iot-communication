# TODO these 4 parameters should be set by the script call
if($args.Count -eq 4){
$wantedIp = $args[0]
$wantedPrefixLength = $args[1] 
$wantedGW = $args[2]
$wantedDNS = $args[3]

Write-Output "telit_setup.ps1 $wantedIp, $wantedPrefixLength, $wantedGW, $wantedDNS" 

Write-Output " " "Finding IP interface for the Telit Modem" " "

$adapterOutput = Get-NetAdapter -InterfaceDescription "CDC NCM"
$index = $adapterOutput.ifIndex
$macAddress = $adapterOutput.MacAddress
$ifName = $adapterOutput.Name

Write-Output "name: $ifName, index: $index, MAC: $macAddress"

$currentIpConfig = Get-NetIPConfiguration -InterfaceIndex $index
$currentIp = $currentIpConfig.IPv4Address.IPAddress
#$currentPL = $currentIpConfig.IPv4Address.PrefixLength
$currentGW = $currentIpConfig.IPv4DefaultGateway.NextHop
$currentDNS = $currentIpConfig.DNSServer.ServerAddresses

#Write-Output " " "Current configuration: " "   IP: $currentIp" "   Prefix Length: $currentPL" "   GW: $currentGW" "   DNS: $currentDNS" " "
Write-Output " " "Current configuration: " "   IP: $currentIp" "   GW: $currentGW" "   DNS: $currentDNS" " "

# Indicate that the Gateway needs resetting if it's not the same
if($currentGW -ne $wantedGW){
Write-Output " " "  Wanted GW: $wantedGW, Current GW: $currentGW"
$resetGW = 1
}
else{
$resetGW = 0  
}

# Indicate that the IP needs resetting
if($currentIp -ne $wantedIp){
Write-Output "  Wanted IP: $wantedIp, Current IP: $currentIp"
$resetIP = 1
}
else{
$resetIP = 0    
}

# Indicate that the DNS needs resetting
if($currentDNS -ne $wantedDNS){
Write-Output "  Wanted DNS: $wantedDNS, Current DNS: $currentDNS" " "
$resetDNS = 1
}
else{
$resetDNS = 0    
}

Write-Output "Reconfiguration status: " "    Reset-IP: $resetIP" "    Reset-GW: $resetGW" "    Reset-DNS: $resetDNS"


# Remove GW if it needs
if($resetIP)
{
    Write-Output "1"

    if($resetGW)
    {
        Write-Output "2"

        Remove-NetRoute -InterfaceIndex $index -NextHop $currentGW -Confirm:$false
    }
    
    Remove-NetIPAddress -InterfaceIndex $index $currentIP -Confirm:$false
    
    if($resetGW)
    {
        Write-Output "3"
        New-NetIPAddress -InterfaceIndex $index -IPAddress $wantedIp -DefaultGateway $wantedGW -PrefixLength $wantedPrefixLength
    }
    else
    {
        Write-Output "4"
        New-NetIPAddress -InterfaceIndex $index -IPAddress $wantedIp -PrefixLength $wantedPrefixLength
    }
}
else
{
    Write-Output "5"
    if($resetGW)
    {
        Write-Output "6"

        # TODO get correct DestinationPrefix

        Remove-NetRoute -InterfaceIndex $index -NextHop $currentGW -Confirm:$false
        New-NetRoute -InterfaceIndex $index -DestinationPrefix "10.0.0.0/8" -NextHop $wantedGW
    }
}

if($resetDNS)
{
    Write-Output "7"
    Set-DNSClientServerAddress -InterfaceIndex $index -ServerAddresses $wantedDNS
}

Write-Output "8"
netsh interface ip delete arpcache
netsh interface ip add neighbor $ifName $wantedGW $macAddress
}
else{
    Write-Output " " "Telit Interface Setup: USE" " " " " 
    Write-Output "    telit_setup.ps1 <wanted_ip> <wanted_prefix_length> <wanted_gw_ip> <wanted_dns_ip>" " " " "
}