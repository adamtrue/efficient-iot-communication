#include <QApplication>
#include <QObject>
#include <QString>
#include <QHostAddress>
#include <QProcess>

#include "iostream"

#include "publisher.h"
#include "subscriber.h"
#include "myudpclient.h"

#include "centralappui.h"
#include "centralfactory.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
/*    CentralAppUI ui;
    ui.constructUI();
    ui.show();

    QHostAddress host("35.156.182.231");
    Subscriber subscriber(host , 1883);

    MyUdpClient udpClient;
    udpClient.setSourcePort(59468);

    QObject::connect(&subscriber, &Subscriber::publicFaceReceived, &udpClient, &MyUdpClient::receivedPublicFace);

    QProcess process;
    QStringList myArgs;

    myArgs.append("stun.solnet.ch");
    myArgs.append(QString::number(3478));

    process.start("stunclient.exe", myArgs);
*/

    CentralFactory factory;
    factory.build();
    factory.link();

    return app.exec();
}
