#ifndef MYTCPTESTCLASS_H
#define MYTCPTESTCLASS_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QTimer>

class MyUdpClient: public QObject
{
    Q_OBJECT
public:
    MyUdpClient();
    MyUdpClient(QString hostname, int port, QObject *parent = nullptr);

    void bindUdp();

    QString hostname() const;
    void setHostname(const QString &hostname);

    int sourcePort() const;
    void setSourcePort(int sourcePort);

    QString sourceAddress() const;
    void setSourceAddress(const QString &sourceAddress);

signals:
    void printStreamToUI(QString payload);
    void printToUI(QString payload);
    void udpReadyForMultimedia(QIODevice* device);
    void noMoreUdpTransmissions();

public slots:
    void readUdpData();
    void socketConnected();
    void sendRandomThing(QString mess);
    void receivedPublicFace(QString ip, int port);
//    void multimediaDone();
    void udpOver();

private:
    QString _hostname;
    int _port;
    int _sourcePort;
    QString _sourceAddress;
    QUdpSocket* _pSocket;
    QTimer* _timer;

    QIODevice* _ioAudioDevice;

    bool _mediaReading = false;
    void _wait(uint32_t miliseconds);
};


#endif // MYTCPTESTCLASS_H
