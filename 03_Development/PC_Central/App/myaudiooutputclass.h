#ifndef MYAUDIOOUTPUTCLASS_H
#define MYAUDIOOUTPUTCLASS_H

#include <QAudioOutput>
#include <QFile>
#include <QDebug>
#include <QTimer>

class MyAudioOutputClass: public QObject
{
    Q_OBJECT

public:
    MyAudioOutputClass();


    QFile destinationFile;
    QAudioOutput* audio;

    void initAudioPlaybackDevice();

signals:
    void multimediaStopped();

public slots:

    void startBuffer(QIODevice* device);
    void stopPlayback();
    void handleStateChanged(QAudio::State newState);

private:
    bool playbackOn = false;
    QIODevice* ioDevice;
};


#endif // MYAUDIOOUTPUTCLASS_H
