#include "centralappui.h"

CentralAppUI::CentralAppUI(QWidget *parent) : QMainWindow(parent)
{

}

void CentralAppUI::constructUI()
{
    // Construct layout
    _hbox1 = new QHBoxLayout();
    _vbox1 = new QVBoxLayout();
    _vbox2 = new QVBoxLayout();


    // Construct UI elements
    _txtData = new QTextEdit();
    _txtStream = new QTextEdit();

    _btnStart = new QPushButton("Start Connection");
    _btnControl = new QPushButton("Control");
    _btnStreamOn = new QPushButton("Stream On");

    _vbox1->addWidget(_btnStart);
    _vbox1->addWidget(_btnControl);
    _vbox1->addWidget(_btnStreamOn);
    _vbox1->addWidget(_txtData);

    _vbox2->addWidget(_txtStream);

    _hbox1->addLayout(_vbox1);
    _hbox1->addLayout(_vbox2);


    // Central widget
    _centralWidget = new QWidget();
    _centralWidget->setLayout(_hbox1);
    setCentralWidget(_centralWidget);


    // Deal with signals and slots
    QObject::connect(_btnStart, SIGNAL(pressed()), this, SIGNAL(startSystem()));
    QObject::connect(_btnStreamOn, SIGNAL(pressed()), this, SIGNAL(startStream()));
}

void CentralAppUI::printData(QString data)
{
    _txtData->append(data);
}

void CentralAppUI::printStreamData(QString data)
{
    _txtStream->append(data);
}
