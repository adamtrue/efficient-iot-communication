#include "centralfactory.h"

#include <QString>
#include <QEventLoop>
#include <QProcess>
#include <QHostInfo>

CentralFactory::CentralFactory()
{

}

void CentralFactory::build()
{
    // Default MQTT address (broker.hivemq.com)
    QHostInfo info = QHostInfo::fromName("broker.hivemq.com");
    QHostAddress mqtt_host = info.addresses().at(0);
//    QHostAddress mqtt_host("35.156.182.231");
    uint16_t mqtt_port = 1883;

    // Build UI
    _ui = new CentralAppUI();
    _ui->constructUI();

    // Build MQTT elements
    _subscriber = new Subscriber(mqtt_host, mqtt_port);
    _publisher = new Publisher(mqtt_host, mqtt_port);

    // Buil UDP socket
    _udpClient = new MyUdpClient();

    // Build multimedia device
    _audioOut = new MyAudioOutputClass();
}

void CentralFactory::link()
{
    QObject::connect(_subscriber, SIGNAL(subscriptionReceived(QString, QString)), this, SLOT(mqttMessageReceived(QString, QString)));
    QObject::connect(_ui, SIGNAL(startSystem()), this, SLOT(start()));
    QObject::connect(_ui, SIGNAL(startStream()), this, SLOT(startStream()));

    QObject::connect(_subscriber, SIGNAL(print(QString)), _ui, SLOT(printData(QString)));
    QObject::connect(_publisher, SIGNAL(print(QString)), _ui, SLOT(printData(QString)));
    QObject::connect(this, SIGNAL(print(QString)), _ui, SLOT(printData(QString)));
    QObject::connect(_udpClient, SIGNAL(printToUI(QString)), _ui, SLOT(printData(QString)));
    QObject::connect(_udpClient, SIGNAL(printStreamToUI(QString)), _ui, SLOT(printStreamData(QString)));

    QObject::connect(this, &CentralFactory::mqttPublicFaceReceived, _udpClient, &MyUdpClient::receivedPublicFace);

    QObject::connect(_udpClient, &MyUdpClient::udpReadyForMultimedia, _audioOut, &MyAudioOutputClass::startBuffer);
//    QObject::connect(_audioOut, &MyAudioOutputClass::multimediaStopped, _udpClient, &MyUdpClient::multimediaDone);
    QObject::connect(_udpClient, &MyUdpClient::noMoreUdpTransmissions, _audioOut, &MyAudioOutputClass::stopPlayback);

    _ui->show();
}

void CentralFactory::start()
{
    _publisher->modifyClientId("EIOTC_Publisher17");
    _subscriber->modifyClientId("EIOTC_Subscriber17");

    _publisher->connectToHost();
    _subscriber->connectToHost();
    _audioOut->initAudioPlaybackDevice();

    QEventLoop loop;
    QObject::connect(_subscriber, SIGNAL(connected()), &loop, SLOT(quit()));
    loop.exec();

    _subscriber->subscribe("adamtopic/peripheral/data");
    _subscriber->subscribe("adamtopic/peripheral/stream");
    _subscriber->subscribe("adamtopic/peripheral/public_face");
    _subscriber->subscribe("adamtopic/peripheral/control_ack");
}

void CentralFactory::startStream()
{
    _publisher->publishToTopic("adamtopic/peripheral/control","STREAM1");
}

void CentralFactory::changeMqttHost(QHostAddress addr)
{
    if(_subscriber->connectedToHost())
    {
        _subscriber->changeHost(addr);
    }
    else
    {
        _subscriber->setHost(addr);
    }

    if(_publisher->connectedToHost())
    {
        _publisher->changeHost(addr);
    }
    else
    {
        _publisher->setHost(addr);
    }
}

void CentralFactory::mqttMessageReceived(QString topic, QString payload)
{
    emit print(topic + ": " + payload);

    if(topic == "adamtopic/peripheral/public_face")
    {
        _makeSTUNRequest();
        QStringList separated = payload.split(":");
        int portParse = separated.at(1).toInt();
        emit mqttPublicFaceReceived(separated.at(0), portParse);//+1);
    }

}

void CentralFactory::_makeSTUNRequest()
{
    QProcess process;
    QStringList myArgs;

    myArgs.append("stun.solnet.ch");
    myArgs.append(QString::number(3478));

    process.start("stunclient.exe", myArgs);
    process.waitForFinished();
    QString output(process.readAllStandardOutput());
    output.squeeze();
    QRegExp separator("\n|\r\n|:");
    QStringList separated = output.split(separator);
    uint16_t local_port = separated[4].toInt();

    QString mapped_address = separated[6];
    uint16_t mapped_port = separated[7].toInt();

    _udpClient->setSourcePort(local_port);

    _publisher->publishToTopic("adamtopic/central/public_face", mapped_address+":"+QString::number(mapped_port));
}
