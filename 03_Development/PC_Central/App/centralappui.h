#ifndef CENTRALAPPUI_H
#define CENTRALAPPUI_H

#include <QObject>
#include <QMainWindow>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QTextEdit>
#include <QPushButton>

class CentralAppUI : public QMainWindow
{
    Q_OBJECT
public:
    explicit CentralAppUI(QWidget *parent = nullptr);

    void constructUI();

signals:
    void startSystem();
    void startStream();

public slots:
    void printData(QString data);
    void printStreamData(QString data);

private:
    // Layouts
    QHBoxLayout* _hbox1;
    QVBoxLayout* _vbox1;
    QVBoxLayout* _vbox2;

    // UI elements
    QTextEdit* _txtData;
    QTextEdit* _txtStream;
    QPushButton* _btnStart;
    QPushButton* _btnControl;
    QPushButton* _btnStreamOn;

    // Central widget to force layout
    QWidget* _centralWidget;
};

#endif // CENTRALAPPUI_H
