#include "subscriber.h"

#include <QString>
#include <QEventLoop>

Subscriber::Subscriber(QHostAddress &host, quint16 port, QObject *parent): QMQTT::Client(host, port, parent)
  , _qout(stdout)
{
  QObject::connect(this, &QMQTT::Client::connected, this, &Subscriber::onConnected);
  QObject::connect(this, &QMQTT::Client::disconnected, this, &Subscriber::onDisconnected);
  QObject::connect(this, &QMQTT::Client::subscribed, this, &Subscriber::onSubscribed);
  QObject::connect(this, &QMQTT::Client::received, this, &Subscriber::onReceived);

//  std::cout << "My host is " << host.toIPv4Address();
}

Subscriber::~Subscriber()
{

}

void Subscriber::changeHost(QHostAddress addr)
{
    disconnectFromHost();
    QEventLoop wait;
    QObject::connect(this, SIGNAL(connected()), &wait, SLOT(quit()));
    wait.exec();
    setHost(addr);
//    connectToHost();
}

void Subscriber::onConnected()
{
//    std::cout << "connected" << std::endl;
    emit print("Subscriber connected");
    _connectedToHost = true;
}

void Subscriber::onDisconnected()
{
    emit print("Subscriber disconnected");
    _connectedToHost = false;
}

void Subscriber::onSubscribed(const QString &topic)
{
    emit print("Subscribed to topic " + topic);
//    std::cout << "subscribed " << topic.toStdString() << std::endl;
}

void Subscriber::onReceived(const QMQTT::Message &message)
{
//    std::cout << "publish received: \"" << QString::fromUtf8(message.payload()).toStdString()
//          << "\"" << std::endl;

    emit subscriptionReceived(message.topic(), message.payload());

    // TODO here
/*    if(message.topic() == "adamtopic/device/public_face")
    {
        QString payload = QString::fromUtf8(message.payload());
        QStringList separated = payload.split(":");
        int portParse = separated.at(1).toInt();
        emit publicFaceReceived(separated.at(0), portParse);
    }
*/
}

bool Subscriber::connectedToHost() const
{
    return _connectedToHost;
}

void Subscriber::modifyClientId(QString id)
{
    setClientId(id);
}

