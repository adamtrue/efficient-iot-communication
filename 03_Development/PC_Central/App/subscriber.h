#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include <QObject>
#include <QVector>

#include <qmqtt.h>

class Subscriber : public QMQTT::Client
{
    Q_OBJECT
public:
    explicit Subscriber(QHostAddress& host,
                        quint16 port,
                        QObject* parent = NULL);
    virtual ~Subscriber();

    QTextStream _qout;

//    QString topic() const;
//    void setTopic(const QString &topic);

    void changeHost(QHostAddress addr);
    bool connectedToHost() const;

    void modifyClientId(QString id);

public slots:
    void onConnected();
    void onDisconnected();
    void onSubscribed(const QString& topic);
    void onReceived(const QMQTT::Message& message);

signals:
    void print(QString message);
    void subscriptionReceived(QString topic, QString message);
//    void publicFaceReceived(QString ip, int port);

private:
    bool _connectedToHost = false;
};

#endif // SUBSCRIBER_H
