#ifndef PUBLISHER_H
#define PUBLISHER_H

#include <QObject>
#include <QTimer>

#include <qmqtt.h>

class Publisher : public QMQTT::Client
{
    Q_OBJECT
public:
    explicit Publisher(const QHostAddress& host = QHostAddress::LocalHost,
                       const quint16 port = 1883,
                       QObject* parent = NULL);
    virtual ~Publisher();

    void publishToTopic(QString topic, QString message);
    void changeHost(QHostAddress addr);
    bool connectedToHost() const;

    void modifyClientId(QString id);

signals:
    void print(QString message);

public slots:
    void onConnected();
    void onDisconnected();

private:
    bool _connectedToHost = false;
};

#endif // PUBLISHER_H
