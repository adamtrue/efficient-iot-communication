#include "publisher.h"
#include <QString>
#include <QEventLoop>

Publisher::Publisher(const QHostAddress &host, const quint16 port, QObject *parent): QMQTT::Client(host, port, parent)
{
    QObject::connect(this, SIGNAL(connected()), this, SLOT(onConnected()));
    QObject::connect(this, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

Publisher::~Publisher()
{

}

void Publisher::publishToTopic(QString topic, QString message)
{
    QMQTT::Message mqttMessage(1, topic, message.toUtf8());
    publish(mqttMessage);
}

void Publisher::changeHost(QHostAddress addr)
{
    disconnectFromHost();
    QEventLoop wait;
    QObject::connect(this, SIGNAL(connected()), &wait, SLOT(quit()));
    wait.exec();
    setHost(addr);
//    connectToHost();
}

void Publisher::onConnected()
{
/*    subscribe(_topic, 0);
    _timer.setSingleShot(true);
    _timer.start(1000);
*/
    emit print("Publisher connected");
   _connectedToHost = true;
}


void Publisher::onDisconnected()
{
    // TODO just put something else here
//    QTimer::singleShot(0, qApp, &QCoreApplication::quit);
    emit print("Publisher disconnected");
    _connectedToHost = false;
}

bool Publisher::connectedToHost() const
{
    return _connectedToHost;
}

void Publisher::modifyClientId(QString id)
{
    setClientId(id);
}

