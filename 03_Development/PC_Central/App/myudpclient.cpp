#include "myudpclient.h"

#include <QEventLoop>
#include <QTimer>
#include "iostream"

MyUdpClient::MyUdpClient()
{

}

MyUdpClient::MyUdpClient(QString hostname, int port, QObject *parent) : QObject(parent)
{
    _hostname = hostname;
    _port = port;
}

void MyUdpClient::bindUdp()
{
    // Create the client socket
    _pSocket = new QUdpSocket();
    _timer = new QTimer();

    QObject::connect( _pSocket, SIGNAL(readyRead()), this, SLOT(readUdpData()) );
    QObject::connect( _pSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    //QObject::connect(_pSocket, )
    QObject::connect(_timer, SIGNAL(timeout()), this, SLOT(udpOver()));

    _pSocket->bind(QHostAddress(_sourceAddress), _sourcePort);
    _pSocket->connectToHost(_hostname, _port);
}

void MyUdpClient::readUdpData()
{   
    if(_mediaReading == false)
    {   
        QByteArray data = _pSocket->readAll();
        QString datas = QString(data);

        if(datas.contains("HOLEPUNCH_ACK"))
        {
            sendRandomThing("HOLEPUNCH_ACK");

//            _mediaReading = true;
//            emit udpReadyForMultimedia(_pSocket);
       }
        else if(datas.contains("HOLEPUNCH"))
        {
            sendRandomThing("HOLEPUNCH");
        }
    }
    else
    {
        // If we haven't received any media for 20 seconds, end the connection
        _timer->stop();
        _timer->start(20000);
    }

}

void MyUdpClient::socketConnected()
{
    QByteArray data = "HOLEPUNCH";
    //for(int i=0; i<50; i++)
    //{
        _pSocket->write( data );
    //    _wait(1000);
    //}
    emit printStreamToUI(data);
}

void MyUdpClient::sendRandomThing(QString mess)
{
    QByteArray data = QByteArray::fromStdString(mess.toStdString());
    _pSocket->write(data);
    _pSocket->flush();

    emit printToUI(QString(data));

//    std::cout<<"Sent: " << mess.toStdString() << std::endl;
}

void MyUdpClient::receivedPublicFace(QString ip, int port)
{
    _hostname = ip;
    _port = port;

    bindUdp();
}

/*
void MyUdpClient::multimediaDone()
{
    //    _pSocket->blockSignals(false);
}*/

void MyUdpClient::udpOver()
{
    emit noMoreUdpTransmissions();

    _mediaReading = false;

    _pSocket->close();
    _timer->stop();

    delete _pSocket;
    delete _timer;
}

void MyUdpClient::_wait(uint32_t miliseconds)
{
    QEventLoop loop;
    QTimer timer;

    QObject::connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);
    timer.setSingleShot(true);
    timer.start(miliseconds);
    loop.exec();
}

QString MyUdpClient::sourceAddress() const
{
    return _sourceAddress;
}

void MyUdpClient::setSourceAddress(const QString &sourceAddress)
{
    _sourceAddress = sourceAddress;
}

int MyUdpClient::sourcePort() const
{
    return _sourcePort;
}

void MyUdpClient::setSourcePort(int sourcePort)
{
    _sourcePort = sourcePort;
}

QString MyUdpClient::hostname() const
{
    return _hostname;
}

void MyUdpClient::setHostname(const QString &hostname)
{
    _hostname = hostname;
}
