#include "myaudiooutputclass.h"

MyAudioOutputClass::MyAudioOutputClass()
{

}

void MyAudioOutputClass::initAudioPlaybackDevice()
{
    QAudioFormat format;
    // Set up the desired format, for example:
    format.setSampleRate(11025);
    format.setChannelCount(1);
    format.setSampleSize(8);
    format.setCodec("audio/pcm");
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::UnSignedInt);

    const auto deviceInfos = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    QAudioDeviceInfo myDevice;


    for (const QAudioDeviceInfo &deviceInfo : deviceInfos)
    {
        qDebug() << "Device name: " << deviceInfo.deviceName();
        qDebug() << "Device format: " << deviceInfo.preferredFormat();

        if(deviceInfo.deviceName().contains("Speaker"))
        {
            myDevice = deviceInfo;

            if (!deviceInfo.isFormatSupported(format)) {
                qWarning() << "Default format not supported, trying to use the nearest.";
                format = deviceInfo.nearestFormat(format);
            }

        }
    }

//    audio = new QAudioOutput(myDevice, myDevice.preferredFormat(), this);
    audio = new QAudioOutput(myDevice, format, this);
    connect(audio, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleStateChanged(QAudio::State)));

//    emit audioIoDeviceReady(audio->)

    //QTimer::singleShot(10000, this, SLOT(stopRecording()));
    //audio->start(&destinationFile);
    // Records audio for 3000ms

//    audio->
}

void MyAudioOutputClass::startBuffer(QIODevice* device)
{
//    destinationFile.setFileName("C:\\Users\\adamt\\Desktop\\Test\\AUDIO\\buffer.raw");
//    destinationFile.open( QIODevice::WriteOnly );

//    QTimer::singleShot(30000, this, SLOT(stopPlayback()));
    ioDevice = device;
    audio->start(device);
    playbackOn = true;
}

void MyAudioOutputClass::stopPlayback()
{
    playbackOn = false;
    audio->stop();
//    emit multimediaStopped();
}

void MyAudioOutputClass::handleStateChanged(QAudio::State newState)
{
    qDebug() << "Warning: " << newState;

    if(newState != QAudio::ActiveState && playbackOn)
    {
        audio ->start(ioDevice);
    }
/*
    switch (newState)
    {
        case QAudio::StoppedState:
            if (audio->error() != QAudio::NoError) {
                // Error handling
            } else {
                // Finished recording
            }
            break;

        case QAudio::ActiveState:
            // Started recording - read from IO device
            break;

        case QAudio::IdleState:
//            audio->resume();
            break;
        case QAudio::SuspendedState:
//            audio->resume();
            break;
        case QAudio::InterruptedState:
//            audio->resume();
            break;

        default:
            // ... other cases as appropriate
            break;
    }*/
}
