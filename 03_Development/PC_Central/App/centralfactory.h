#ifndef CENTRALFACTORY_H
#define CENTRALFACTORY_H

#include <QObject>

#include "subscriber.h"
#include "publisher.h"
#include "centralappui.h"
#include "myudpclient.h"
#include "myaudiooutputclass.h"


class CentralFactory: public QObject
{
    Q_OBJECT
public:
    CentralFactory();

    void build();
    void link();

signals:
    void print(QString message);
    void mqttPublicFaceReceived(QString ip, uint16_t port);

public slots:
    void start();
    void startStream();
    void changeMqttHost(QHostAddress addr);
    void mqttMessageReceived(QString topic, QString payload);

private:
    CentralAppUI* _ui;
    Publisher* _publisher;
    Subscriber* _subscriber;
    MyUdpClient* _udpClient;
    MyAudioOutputClass* _audioOut;

    bool _mqttConnected = false;

    bool _streamOn = false;

    void _makeSTUNRequest();
};

#endif // CENTRALFACTORY_H
