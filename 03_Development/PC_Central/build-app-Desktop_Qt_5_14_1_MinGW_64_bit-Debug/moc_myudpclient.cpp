/****************************************************************************
** Meta object code from reading C++ file 'myudpclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../App/myudpclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'myudpclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MyUdpClient_t {
    QByteArrayData data[17];
    char stringdata0[194];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MyUdpClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MyUdpClient_t qt_meta_stringdata_MyUdpClient = {
    {
QT_MOC_LITERAL(0, 0, 11), // "MyUdpClient"
QT_MOC_LITERAL(1, 12, 15), // "printStreamToUI"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 7), // "payload"
QT_MOC_LITERAL(4, 37, 9), // "printToUI"
QT_MOC_LITERAL(5, 47, 21), // "udpReadyForMultimedia"
QT_MOC_LITERAL(6, 69, 10), // "QIODevice*"
QT_MOC_LITERAL(7, 80, 6), // "device"
QT_MOC_LITERAL(8, 87, 22), // "noMoreUdpTransmissions"
QT_MOC_LITERAL(9, 110, 11), // "readUdpData"
QT_MOC_LITERAL(10, 122, 15), // "socketConnected"
QT_MOC_LITERAL(11, 138, 15), // "sendRandomThing"
QT_MOC_LITERAL(12, 154, 4), // "mess"
QT_MOC_LITERAL(13, 159, 18), // "receivedPublicFace"
QT_MOC_LITERAL(14, 178, 2), // "ip"
QT_MOC_LITERAL(15, 181, 4), // "port"
QT_MOC_LITERAL(16, 186, 7) // "udpOver"

    },
    "MyUdpClient\0printStreamToUI\0\0payload\0"
    "printToUI\0udpReadyForMultimedia\0"
    "QIODevice*\0device\0noMoreUdpTransmissions\0"
    "readUdpData\0socketConnected\0sendRandomThing\0"
    "mess\0receivedPublicFace\0ip\0port\0udpOver"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MyUdpClient[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       4,    1,   62,    2, 0x06 /* Public */,
       5,    1,   65,    2, 0x06 /* Public */,
       8,    0,   68,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,   69,    2, 0x0a /* Public */,
      10,    0,   70,    2, 0x0a /* Public */,
      11,    1,   71,    2, 0x0a /* Public */,
      13,    2,   74,    2, 0x0a /* Public */,
      16,    0,   79,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   14,   15,
    QMetaType::Void,

       0        // eod
};

void MyUdpClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MyUdpClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->printStreamToUI((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->printToUI((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->udpReadyForMultimedia((*reinterpret_cast< QIODevice*(*)>(_a[1]))); break;
        case 3: _t->noMoreUdpTransmissions(); break;
        case 4: _t->readUdpData(); break;
        case 5: _t->socketConnected(); break;
        case 6: _t->sendRandomThing((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->receivedPublicFace((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->udpOver(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QIODevice* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MyUdpClient::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyUdpClient::printStreamToUI)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MyUdpClient::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyUdpClient::printToUI)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MyUdpClient::*)(QIODevice * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyUdpClient::udpReadyForMultimedia)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (MyUdpClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyUdpClient::noMoreUdpTransmissions)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MyUdpClient::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_MyUdpClient.data,
    qt_meta_data_MyUdpClient,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MyUdpClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MyUdpClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MyUdpClient.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int MyUdpClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void MyUdpClient::printStreamToUI(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MyUdpClient::printToUI(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MyUdpClient::udpReadyForMultimedia(QIODevice * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MyUdpClient::noMoreUdpTransmissions()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
