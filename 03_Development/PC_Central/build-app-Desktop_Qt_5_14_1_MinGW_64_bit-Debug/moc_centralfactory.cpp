/****************************************************************************
** Meta object code from reading C++ file 'centralfactory.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../App/centralfactory.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'centralfactory.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CentralFactory_t {
    QByteArrayData data[16];
    char stringdata0[155];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CentralFactory_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CentralFactory_t qt_meta_stringdata_CentralFactory = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CentralFactory"
QT_MOC_LITERAL(1, 15, 5), // "print"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 7), // "message"
QT_MOC_LITERAL(4, 30, 22), // "mqttPublicFaceReceived"
QT_MOC_LITERAL(5, 53, 2), // "ip"
QT_MOC_LITERAL(6, 56, 8), // "uint16_t"
QT_MOC_LITERAL(7, 65, 4), // "port"
QT_MOC_LITERAL(8, 70, 5), // "start"
QT_MOC_LITERAL(9, 76, 11), // "startStream"
QT_MOC_LITERAL(10, 88, 14), // "changeMqttHost"
QT_MOC_LITERAL(11, 103, 12), // "QHostAddress"
QT_MOC_LITERAL(12, 116, 4), // "addr"
QT_MOC_LITERAL(13, 121, 19), // "mqttMessageReceived"
QT_MOC_LITERAL(14, 141, 5), // "topic"
QT_MOC_LITERAL(15, 147, 7) // "payload"

    },
    "CentralFactory\0print\0\0message\0"
    "mqttPublicFaceReceived\0ip\0uint16_t\0"
    "port\0start\0startStream\0changeMqttHost\0"
    "QHostAddress\0addr\0mqttMessageReceived\0"
    "topic\0payload"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CentralFactory[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    2,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   52,    2, 0x0a /* Public */,
       9,    0,   53,    2, 0x0a /* Public */,
      10,    1,   54,    2, 0x0a /* Public */,
      13,    2,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 6,    5,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   14,   15,

       0        // eod
};

void CentralFactory::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<CentralFactory *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->print((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->mqttPublicFaceReceived((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2]))); break;
        case 2: _t->start(); break;
        case 3: _t->startStream(); break;
        case 4: _t->changeMqttHost((*reinterpret_cast< QHostAddress(*)>(_a[1]))); break;
        case 5: _t->mqttMessageReceived((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (CentralFactory::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CentralFactory::print)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (CentralFactory::*)(QString , uint16_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CentralFactory::mqttPublicFaceReceived)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject CentralFactory::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_CentralFactory.data,
    qt_meta_data_CentralFactory,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *CentralFactory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CentralFactory::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CentralFactory.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int CentralFactory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void CentralFactory::print(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CentralFactory::mqttPublicFaceReceived(QString _t1, uint16_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
