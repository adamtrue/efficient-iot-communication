How to setup a Network Interface on Windows with PowerShell
----

## Connecting the Telit Modem

So to start we need to plug in and switch on our Telit modem. Once it's on send the following commands to the Telit modem:

```
AT#NCM=2,1
AT+CGCONTRDP=1
```


We're telling it to switch on NCM for interface 1. Then we're requesting the IP address, GW IP address and DNS IP address with the ``CGCONTRDP`` command. The response will look something like this:

```
+CGCONTRDP: 1,5,"shared.m2m.ch.mnc001.mcc228.gprs","10.1.5.243.255.0.0.0","10.1.
5.244","193.5.23.1","193.5.23.1","0.0.0.0","0.0.0.0"

                        
                                                                                
OK
```

This means:

* The IP address is **10.1.5.243/8**
* The gateway's IP adress is **10.1.5.244**
* The DNS' IP adress is **193.5.23.1**

## Setting up the Windows interface

After executing these commands, our Windows PC should have detected a new network interface. This is visible by using the following command in PowerShell:

```
PS C:\Users\adamt> Get-NetAdapter -InterfaceDescription "CDC NCM"

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Ethernet 8                CDC NCM                                      97 Up           00-00-11-12-13-14       480 Mbps
```


We're asking it which device (or devices) has "CDC NCM" as a descriptor. We only have one Telit modem switched on, so we have one response here.

What really interests us in this part is the interface index: **97**, this is what allows us to configure this interface in the following steps. We also need to know the MAC address so we can configure the ARP setup later on.

Let's start by seeing if there is already an IP configuration for this interface:

```
C:\WINDOWS\system32> Get-NetIPConfiguration -InterfaceIndex 97


InterfaceAlias       : Ethernet 8
InterfaceIndex       : 97
InterfaceDescription : CDC NCM
NetProfile.Name      : Network 3
IPv4Address          : 10.64.223.155
IPv4DefaultGateway   : 10.64.223.156
DNSServer            : 193.5.23.1
```

We can see that there is already some data here. This needs to all be removed and reset to our new values.

We use this to remove the Gateway address that was already configured:

```
C:\WINDOWS\system32>Remove-NetRoute -InterfaceIndex 97 -NextHop 10.64.223.156
```

Then we use this to remove the current IP address:

```
C:\WINDOWS\system32>Remove-NetIPAddress -InterfaceIndex 97 10.64.223.155
```

Now let's add our new one back in:

```
C:\WINDOWS\system32>New-NetIPAddress -InterfaceIndex 97 –IPAddress 10.64.223.155 -DefaultGateway 10.64.223.156 -PrefixLength 8 
```

Let's set the DNS IP address:

```
C:\WINDOWS\system32> Set-DNSClientServerAddress -InterfaceIndex 97 -ServerAddresses 193.247.204.1
```


Let's check that everything has worked:

```
C:\WINDOWS\system32> Get-NetIPConfiguration -InterfaceIndex 97


InterfaceAlias       : Ethernet 8
InterfaceIndex       : 97
InterfaceDescription : CDC NCM
NetProfile.Name      : Network 3
IPv4Address          : 10.64.223.155
IPv4DefaultGateway   : 10.64.223.156
DNSServer            : 193.247.204.1
```

Now we can clear ARP cache and add the gateway to the IP neighbours:

```
netsh interface ip delete arpcache
netsh interface ip add neighbor "Ethernet 8" 10.64.223.156 00-00-11-12-13-14
```


## Disconnecting the Telit Modem

To disconnect the modem is very simple: 

```
AT#NCMD=0
```

