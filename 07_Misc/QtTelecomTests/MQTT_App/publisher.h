#ifndef PUBLISHER_H
#define PUBLISHER_H

#include <QObject>
#include <QTimer>

#include <qmqtt.h>

class Publisher : public QMQTT::Client
{
    Q_OBJECT
public:
    explicit Publisher(const QHostAddress& host = QHostAddress::LocalHost,
                       const quint16 port = 1883,
                       QObject* parent = NULL);
    virtual ~Publisher();

    QTimer _timer;
    quint16 _number;

    QString topic() const;
    void setTopic(const QString &topic);

public slots:
    void onConnected();
    void onTimeout();
    void onDisconnected();

private:
    QString _topic;
};

#endif // PUBLISHER_H
