#include "publisher.h"
#include <QString>

Publisher::Publisher(const QHostAddress &host, const quint16 port, QObject *parent): QMQTT::Client(host, port, parent), _number(0)
{
    QObject::connect(this, SIGNAL(connected()), this, SLOT(onConnected()));
    QObject::connect(&_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    QObject::connect(this, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}

Publisher::~Publisher()
{

}

void Publisher::onConnected()
{
    subscribe(_topic, 0);
    _timer.start(1000);
}

void Publisher::onTimeout()
{
    QMQTT::Message message(_number, _topic,
                           QString::fromStdString("Number is %1").arg(_number).toUtf8());
    publish(message);
    _number++;
    if(_number >= 10)
    {
        _timer.stop();
        disconnectFromHost();
    }
}

void Publisher::onDisconnected()
{
    // TODO just put something else here
//    QTimer::singleShot(0, qApp, &QCoreApplication::quit);
}

QString Publisher::topic() const
{
    return _topic;
}

void Publisher::setTopic(const QString &topic)
{
    _topic = topic;
}
