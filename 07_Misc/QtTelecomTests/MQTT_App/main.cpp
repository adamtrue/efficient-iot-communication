#include <QCoreApplication>
#include <QObject>
#include <QString>

#include "iostream"

#include "publisher.h"
#include "subscriber.h"

int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    QHostAddress host("52.29.60.198");
    Subscriber subscriber(host , 1883);
    subscriber.setTopic("adamtopic/hello");
    subscriber.connectToHost();

    Publisher publisher(host , 1883);
    publisher.setTopic("adamtopic/goodbye");
    publisher.connectToHost();

    return app.exec();
}
