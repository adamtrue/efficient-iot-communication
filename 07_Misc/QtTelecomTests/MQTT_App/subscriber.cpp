#include "subscriber.h"

#include <iostream>
#include <QString>


Subscriber::Subscriber(QHostAddress &host, quint16 port, QObject *parent): QMQTT::Client(host, port, parent)
  , _qout(stdout)
{
  QObject::connect(this, &QMQTT::Client::connected, this, &Subscriber::onConnected);
  QObject::connect(this, &QMQTT::Client::subscribed, this, &Subscriber::onSubscribed);
  QObject::connect(this, &QMQTT::Client::received, this, &Subscriber::onReceived);

  std::cout << "My host is " << host.toIPv4Address();
}

Subscriber::~Subscriber()
{

}

void Subscriber::onConnected()
{
    std::cout << "connected" << std::endl;
    subscribe(_topic, 0);
}

void Subscriber::onSubscribed(const QString &topic)
{
    std::cout << "subscribed " << topic.toStdString() << std::endl;
}

void Subscriber::onReceived(const QMQTT::Message &message)
{
    std::cout << "publish received: \"" << QString::fromUtf8(message.payload()).toStdString()
          << "\"" << std::endl;
}

QString Subscriber::topic() const
{
    return _topic;
}

void Subscriber::setTopic(const QString &topic)
{
    _topic = topic;
}
