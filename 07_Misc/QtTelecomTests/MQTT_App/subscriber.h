#ifndef SUBSCRIBER_H
#define SUBSCRIBER_H

#include <QObject>

#include <qmqtt.h>

class Subscriber : public QMQTT::Client
{
    Q_OBJECT
public:
    explicit Subscriber(QHostAddress& host,
                        quint16 port,
                        QObject* parent = NULL);
    virtual ~Subscriber();

    QTextStream _qout;

    QString topic() const;
    void setTopic(const QString &topic);

public slots:
    void onConnected();
    void onSubscribed(const QString& topic);
    void onReceived(const QMQTT::Message& message);

private:
    QString _topic;
};

#endif // SUBSCRIBER_H
