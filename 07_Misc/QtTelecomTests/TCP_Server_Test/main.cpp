#include <QCoreApplication>
#include <QObject>

#include "mytcpserver.h"

#include "iostream"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyTCPServer myboy(nullptr);

    std::cout << "SERVER\r\n";

    myboy.connectTcp();

    return a.exec();
}
