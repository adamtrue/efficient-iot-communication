#include "mytcpserver.h"

#include "iostream"

MyTCPServer::MyTCPServer(QObject *parent) : QObject(parent)
{

}

void MyTCPServer::connectTcp()
{
    QByteArray data = "kebwejbwkejbf"; // <-- fill with data

    _pServer = new QTcpServer(); // <-- needs to be a member variable: QTcpSocket * _pSocket;
    QObject::connect( _pServer, SIGNAL(newConnection()), this, SLOT(newConnectionMade()) );

    _pServer->listen(QHostAddress(), 9000);
}

void MyTCPServer::newConnectionMade()
{
    _pClient = _pServer->nextPendingConnection();
    QObject::connect(_pClient, SIGNAL(readyRead()), this, SLOT(readTcpData()));
}

void MyTCPServer::readTcpData()
{
    QByteArray data = _pClient->readAll();
    std::cout << data.toStdString();
    _pClient->write("Thank you for connecting");
}
