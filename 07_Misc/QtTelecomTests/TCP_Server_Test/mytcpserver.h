#ifndef MYTCPTESTCLASS_H
#define MYTCPTESTCLASS_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class MyTCPServer: public QObject
{
    Q_OBJECT
public:
    MyTCPServer(QObject *parent);

    QTcpServer* _pServer;
    QTcpSocket* _pClient;

    void connectTcp();

public slots:
    void newConnectionMade();
    void readTcpData();
};


#endif // MYTCPTESTCLASS_H
