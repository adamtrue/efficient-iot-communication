#include "mytcpclient.h"

#include "iostream"

MyTCPClient::MyTCPClient(QString hostname, int port, QObject *parent) : QObject(parent)
{
    _hostname = hostname;
    _port = port;
}

void MyTCPClient::connectTcp()
{
    // Create the client socket
    _pSocket = new QTcpSocket();
    QObject::connect( _pSocket, SIGNAL(readyRead()), this, SLOT(readTcpData()) );
    QObject::connect( _pSocket, SIGNAL(connected()), this, SLOT(socketConnected()));

    _pSocket->connectToHost(_hostname, _port);
}

void MyTCPClient::readTcpData()
{
    QByteArray data = _pSocket->readAll();
    std::cout << data.toStdString();
}

void MyTCPClient::socketConnected()
{
    QByteArray data = "My name is MyTCPClient";
    std::cout << "Client connected to socket" << std::endl;
/*

    // Really abusive method for forming a bytewise message

                                 // binding request
    const uint8_t data_uint8[] = {0x00,0x01,
                                  // magic cookie (always 0x2112a442
                                  0x21,0x12,0xA4,0x42,
                                  // length TODO
                                  0xaa,0xaa,
                                  // transaction id TODO
                                  0xaa,0xaa
                                  // data TODO
                                 };
    const char *data_chr = (char*)data_uint8;
    QByteArray data(data_chr);
*/
    _pSocket->write( data );
}

QString MyTCPClient::hostname() const
{
    return _hostname;
}

void MyTCPClient::setHostname(const QString &hostname)
{
    _hostname = hostname;
}
