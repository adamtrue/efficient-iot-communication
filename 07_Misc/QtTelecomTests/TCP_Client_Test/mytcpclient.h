#ifndef MYTCPTESTCLASS_H
#define MYTCPTESTCLASS_H

#include <QObject>
#include <QTcpSocket>

class MyTCPClient: public QObject
{
    Q_OBJECT
public:
    MyTCPClient(QString hostname, int port, QObject *parent = nullptr);

    void connectTcp();

    QString hostname() const;
    void setHostname(const QString &hostname);

public slots:
    void readTcpData();
    void socketConnected();

private:
    QString _hostname;
    int _port;
    QTcpSocket* _pSocket;
};


#endif // MYTCPTESTCLASS_H
