#include <QCoreApplication>
#include <QObject>
#include <QTcpSocket>

#include "mytcpclient.h"

#include "iostream"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyTCPClient myboy("127.0.0.1",  9000);

    std::cout << "CLIENT\r\n";

    myboy.connectTcp();

    return a.exec();
}
