#include <QCoreApplication>
#include <QObject>
#include <QTcpSocket>
#include <QRandomGenerator>

#include "myudpclient.h"

#include "iostream"


#include <QTimer>
#include <QEventLoop>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    MyUdpClient myboy("stun.solnet.ch",  3478);

    myboy.setSourceAddress("153.109.2.89");
    myboy.setSourcePort(62100);

    std::cout << "CLIENT\r\n";

    QEventLoop loop;

    QObject::connect(&myboy,SIGNAL(readyToReady()),&loop, SLOT(quit()));

    myboy.bindUdp();
    loop.exec();

    QByteArray bytes;

    // CONSTRUCT A STUN REQUEST

    // Message type 2 bytes
    bytes.append(int(0x00));
    bytes.append(int(0x01));

    // Message length 2 bytes
    bytes.append(int(0x00));
    bytes.append(int(0x08));

    // Message cookie always 0x2112a442
    bytes.append(int(0x21));
    bytes.append(int(0x12));
    bytes.append(int(0xa4));
    bytes.append(int(0x42));

    // Transaction id 12 bytes random
    bytes.append(int(0x11));
    bytes.append(int(0x20));
    bytes.append(int(0x23));
    bytes.append(int(0x54));
    bytes.append(int(0x04));
    bytes.append(int(0x02));
    bytes.append(int(0x01));
    bytes.append(int(0x09));
    bytes.append(int(0x09));
    bytes.append(int(0x00));
    bytes.append(int(0x00));
    bytes.append(int(0x00));


    // Attributes always the same
    bytes.append(int(0x00));
    bytes.append(int(0x03));
    bytes.append(int(0x00));
    bytes.append(int(0x04));
    bytes.append(int(0x00));
    bytes.append(int(0x00));
    bytes.append(int(0x00));
    bytes.append(int(0x00));


    myboy.sendRandomThing(bytes);

    return a.exec();
}
