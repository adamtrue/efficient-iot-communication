#include "myudpclient.h"

#include "iostream"

MyUdpClient::MyUdpClient(QString hostname, int port, QObject *parent) : QObject(parent)
{
    _hostname = hostname;
    _port = port;
}

void MyUdpClient::bindUdp()
{
    // Create the client socket
    _pSocket = new QUdpSocket();
    QObject::connect( _pSocket, SIGNAL(readyRead()), this, SLOT(readTcpData()) );
    QObject::connect( _pSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
/*
    _pSocket->bind(QHostAddress(_hostname), _port);
    _pSocket->open( );
*/
    _pSocket->bind(QHostAddress(_sourceAddress), _sourcePort);
    _pSocket->connectToHost(_hostname, _port);
/*    QByteArray data = "My name is MyUdpClient";
    _pSocket->write( data );
*/}

void MyUdpClient::readTcpData()
{
    QByteArray data = _pSocket->readAll();
    std::cout << data.toStdString();
}

void MyUdpClient::socketConnected()
{
    QByteArray data = "My name is MyUdpClient";
    std::cout << "Client connected to socket" << std::endl;
    emit readyToReady();
/*

    // Really abusive method for forming a bytewise message

                                 // binding request
    const uint8_t data_uint8[] = {0x00,0x01,
                                  // magic cookie (always 0x2112a442
                                  0x21,0x12,0xA4,0x42,
                                  // length TODO
                                  0xaa,0xaa,
                                  // transaction id TODO
                                  0xaa,0xaa
                                  // data TODO
                                 };
    const char *data_chr = (char*)data_uint8;
    QByteArray data(data_chr);
*/
    //    _pSocket->write( data );
}

void MyUdpClient::sendRandomThing(QString mess)
{
    QByteArray data = QByteArray::fromStdString(mess.toStdString());
    _pSocket->write(data);
    _pSocket->flush();

    std::cout<<"Sent: " << mess.toStdString() << std::endl;
}

void MyUdpClient::sendRandomThing(QByteArray mess)
{
    _pSocket->write(mess);
    _pSocket->flush();
    std::cout<<"Sent a byte array" << std::endl;
}

QString MyUdpClient::sourceAddress() const
{
    return _sourceAddress;
}

void MyUdpClient::setSourceAddress(const QString &sourceAddress)
{
    _sourceAddress = sourceAddress;
}

int MyUdpClient::sourcePort() const
{
    return _sourcePort;
}

void MyUdpClient::setSourcePort(int sourcePort)
{
    _sourcePort = sourcePort;
}

QString MyUdpClient::hostname() const
{
    return _hostname;
}

void MyUdpClient::setHostname(const QString &hostname)
{
    _hostname = hostname;
}
