#ifndef MYTCPTESTCLASS_H
#define MYTCPTESTCLASS_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>

class MyUdpClient: public QObject
{
    Q_OBJECT
public:
    MyUdpClient(QString hostname, int port, QObject *parent = nullptr);

    void bindUdp();

    QString hostname() const;
    void setHostname(const QString &hostname);

    int sourcePort() const;
    void setSourcePort(int sourcePort);

    QString sourceAddress() const;
    void setSourceAddress(const QString &sourceAddress);

signals:
    void readyToReady();

public slots:
    void readTcpData();
    void socketConnected();
    void sendRandomThing(QString mess);
    void sendRandomThing(QByteArray mess);

private:
    QString _hostname;
    int _port;
    int _sourcePort;
    QString _sourceAddress;
    QUdpSocket* _pSocket;
};


#endif // MYTCPTESTCLASS_H
