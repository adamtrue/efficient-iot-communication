#include <QCoreApplication>
#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>

#include "myudpclient.h"

#include "iostream"


#include <QTimer>
#include <QEventLoop>

int main(int argc, char *argv[])
{
    uint16_t startPort = 59000;
    QCoreApplication a(argc, argv);
    QString hostaddress = "10.0.218.73";
    MyUdpClient myboy("153.109.68.63",  startPort);

    myboy.setSourceAddress(hostaddress);
    myboy.setSourcePort(61423);

    std::cout << "CLIENT\r\n";

    myboy.bindUdp();

    QTimer timer;
    timer.setInterval(100);
    timer.setSingleShot(true);
    QEventLoop loop;

    QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));

    int i = 0;
    std::string data;
    while(1)
    {
//        std::cin >> data;
        myboy.sendRandomThing("Hello "+QString::number(i));
        i++;
//        timer.start();
//        loop.exec();
        myboy.disconnectMyClient();
        startPort ++;
        myboy.setPort(startPort);
        myboy.bindUdp();
        timer.start();
        loop.exec();
    }

    return a.exec();
}
