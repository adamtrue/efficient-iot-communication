# Arbitration method

Some methods for arbitration are the following:


## Energy - Main Criteria

Make energy measurements during the transmission to see if the switch is necessary. 


## Data Size - Main Criteria

Determine the size of the data transmission before the dialogue. Send over either network according to either protocol.


## Buffering

This system would be based on buffering the data and sending many discrete measurements in one packet.


## Compression for Streams

The audio or video could be compressed into MP3 or MPEG respectively, this would make the transmission time greatly reduced. We could send the compressed version over LTE-M.

The only problem is the energy budget:

* Compression of raw data
* Sending the compressed file / stream

Obviously this is strongly related to the **Buffering** section. We would buffer together the data then create a file from this, only then would it be transferred. We need to find a balance with the data size between the energy it takes to compress and the latency of the operation. If the data packets are too large, it would take a long time to transmit and more energy to compress. 