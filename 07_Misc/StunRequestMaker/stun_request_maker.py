import random

class StunRequest:
    def __init__(self):
        # 2 bytes
        self.message_type = [0x00,0x01]
        # 2 bytes
        self.message_length=[0x00,0x08]
        # always this magic number for STUN 0x2112a442
        self.message_cookie=[0x21,0x12,0xa4,0x42]
        # 12 bytes
        self.transaction_id=[0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c]
        # CHANGE REQUEST 8 bytes
        self.attr = [0x00,0x03,0x00,0x04,0x00,0x00,0x00,0x00]

    def randomise_trans_id(self):
        for i in range(0, len(self.transaction_id)):
            self.transaction_id[i] = random.randint(0,255)

    def get_message(self):
        char_array = []
        for b in self.message_type:
            char_array.append(b)
        for b in self.message_length:
            char_array.append(b)
        for b in self.message_cookie:
            char_array.append(b)
        for b in self.transaction_id:
            char_array.append(b)
        for b in self.attr:
            char_array.append(b)
        return char_array


request = StunRequest()
request.randomise_trans_id()

f = open("stunreq.bin",'wb')

my_array = bytes(request.get_message())
f.write(my_array)

#for b in request.get_message():
#    bb = bytearray(b)
#    f.write(bb)

f.close()