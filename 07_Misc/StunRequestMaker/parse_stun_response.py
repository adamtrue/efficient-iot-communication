# This is the structure of a mapped address in IPv4 for the STUN protocol
'''
class MappedAddress:
    def __init__(self):
        # 2 bytes
        self.attr_type = [0x00,0x01]
        # 2 bytes
        self.attr_length = [0x00,0x08]
        # 1 byte
        self.reserved = [0x00]
        # 1 byte
        self.protocol_family = [0x01]   # IPv4
        # 2 bytes
        self.port = [0x00,0x00]
        # 4 bytes
        self.ip = [0x00,0x00,0x00,0x00]
'''

OFF = 0

class StunReponse:
    def __init__(self):
        '''
        # 2 bytes
        self.message_type = [0x01,0x01]
        # 2 bytes
        self.message_length=[0x00,0x44]
        # always this magic number for STUN 0x2112a442
        self.message_cookie=[0x21,0x12,0xa4,0x42]
        # 12 bytes
        self.transaction_id=[0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c]
        self.mapped_address = MappedAddress()
        '''
        self.message_type = 0x0101
        self.message_length = 0
        self.magic_cookie = 0x2112a442
        self.transaction_id = []
        self.port = 0
        self.ip = [0,0,0,0]


    def parse_file(self, filename):
        f = open(filename, 'rb')
        my_bytes = f.read()

        # Checking if the message is a stun message
        if my_bytes[OFF+0]!=0x01 or my_bytes[OFF+1]!=0x01:
        #if my_bytes[OFF+1]!=0x01:
            print('sorry this is not a STUN response')
            return

        self.message_length = my_bytes[OFF+2]*16*16+my_bytes[OFF+3]
        '''
        # Checking if the message is the correct length
        if self.message_length != 0x44:
            print('sorry this message is an incorrect length')
        '''

        # Check the magic Stun cookie
        if my_bytes[OFF+4]!=0x21 or my_bytes[OFF+5]!=0x12 or my_bytes[OFF+6]!=0xa4 or my_bytes[OFF+7]!=0x42:
            print('The magic cookie is wrong!')
            return

        # Get the transaction id
        for i in range(OFF+8,OFF+20):
            self.transaction_id.append(my_bytes[i])

        # Check that the attribute type is MAPPED_ADDRESS
        if my_bytes[OFF+20] == 0x00 and my_bytes[OFF+21] == 0x01:
            print('mapped address')

            # Check length
            if my_bytes[OFF+22] != 0x00 and my_bytes[OFF+23] != 0x08:
                print('sorry attribute length incorrect')
                return

            # Check protocol is IPv4 (byte [0x24] is reserved)
            if my_bytes[OFF+25] != 0x01:
                print('sorry the protocol is not IPv4!')
                return

            self.port = my_bytes[OFF+26]*16+my_bytes[OFF+27]
            self.port = my_bytes[OFF+26] * 16 + my_bytes[OFF+27]

            self.ip[0] = my_bytes[OFF+28]
            self.ip[1] = my_bytes[OFF+29]
            self.ip[2] = my_bytes[OFF+30]
            self.ip[3] = my_bytes[OFF+31]

            print("Mapped ip address: "+str(self.ip[0])+"."+str(self.ip[1])+"."+str(self.ip[2])+"."+str(self.ip[3])+":"+str(self.port))

        # for b in request.get_message():
        #    bb = bytearray(b)
        #    f.write(bb)
        f.close()

response = StunReponse()
response.parse_file("stunresp.bin")
response.parse_file("stunresp1.bin")
response.parse_file("stunresp2.bin")
response.parse_file("stunresp3.bin")