#include <QCoreApplication>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QEventLoop>
#include <QTimer>

#include "iostream"

void listAvailablePorts()
{
    for (auto i: QSerialPortInfo().availablePorts())
    {
        qDebug() << i.portName();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSerialPort serial;
    QEventLoop loop;
    QTimer timer;

    // Set a timer to allow signals and slots in infinite loop
    timer.setInterval(500);
    timer.setSingleShot(true);

    // Parameters of the com with the LTE modem
    serial.setPortName("COM5");
    if(!serial.setBaudRate(QSerialPort::Baud115200))
        std::cout << serial.errorString().toStdString() << std::endl;
    if(!serial.setDataBits(QSerialPort::Data8))
        std::cout << serial.errorString().toStdString() << std::endl;
    if(!serial.setParity(QSerialPort::NoParity))
        std::cout << serial.errorString().toStdString() << std::endl;
    if(!serial.setFlowControl(QSerialPort::NoFlowControl))
        std::cout << serial.errorString().toStdString() << std::endl;
    if(!serial.setStopBits(QSerialPort::OneStop))
        std::cout << serial.errorString().toStdString() << std::endl;
    if(!serial.open(QIODevice::ReadWrite))
        std::cout << serial.errorString().toStdString() << std::endl;

    // Connect the serial port with a read
    QObject::connect(&serial, &QSerialPort::readyRead, [&]
    {
        //this is called when readyRead() is emitted
        //qDebug() << "New data available: " << serial.bytesAvailable();
        QByteArray datas = serial.readAll();
        std::cout << datas.toStdString() << std::endl;
    });

    // Connect an error with a port close
    QObject::connect(&serial, static_cast<void(QSerialPort::*)(QSerialPort::SerialPortError)> (&QSerialPort::error), [&](QSerialPort::SerialPortError error)
    {
        //this is called when a serial communication error occurs
        std::cout << "An error occured: " << error << std::endl;
        a.quit();
    });

    // A loop is added to the big while to ensure that signals and slots can execute
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));


    while(1)
    {
        // Read data in
        std::string data;
        std::cin >> data;
        data += "\r\n";
        serial.write(data.c_str());

        // Allow slots and signals to function
        timer.start();
        loop.exec();
    }

    return a.exec();
}
