\section{Network Architecture}

The architecture of the network is actually somewhat more complex than previously presented. The reason for this is there needs to be some sort of infrastructure that lets the modems communicate over the internet. 

For the nRF modem, there are several custom commands for MQTT, making this an easy protocol to use directly out of the box. MQTT is a distributed protocol meaning that there is a broker through which all of the messages are sent. There is a system of topics to which to publish messages and with subscribe to. When a message is published to a topic, it is relayed to all of the devices which are subscribed to the topic in question.

For the Telit modem it is a bit more complicated. Because of the large data packets (or streaming) it is important that there is no broker through which to send the data, this would likely overload the broker. However because the Telit modem is in a private network behind a NAT and the Central PC is also hidden behind a NAT, we need a way for them to discover their own public IP addresses and ports. This is done with the STUN protocol. The device makes a \texttt{STUN REQUEST} to a STUN server, which responds with its public IP address and public port. Once these public faces are obtained, they can be exchanged via MQTT with the nRF modem. Now a high speed data transfer can be established between the Telit modem and the Central PC as shown in figure \ref{fig:newarc}.


\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/stun_mqtt_arch.pdf}
    \caption{Network Architecture}
    \label{fig:newarc}
\end{figure}

\subsection{MQTT Protocol}

MQTT (Message Queuing Telemetry Transport) is a distribution protocol, used in IoT architectures. In practice it is used over TCP/IP, but in theory it could be used over any protocol with the same basic functions as TCP/IP (ordered packets, lossless and bidirectional communication). There are several mechanisms which make it efficient for low power systems. It is based on a publish/subscribe architecture, where devices can publish messages to a ``topic". Devices can subscribe to a topic and they will receive any message published to said topic. In the figure~\ref{fig:mqtt_example}, there is an example taken from \url{mqtt.org}. In this example we can see that there are two types of devices:

\begin{itemize}
    \item MQTT Client
    \item MQTT Broker
\end{itemize}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/mqtt_example.png}
    \caption{Example of how MQTT works, \url{mqtt.org}}
    \label{fig:mqtt_example}
\end{figure}

Any client can subscribe and publish to any topic. When a client publishes to a topic, the broker will distribute it to every client that is subscribed. 

This is a very simple yet very effective principle. Let's look at how we can use this for \textbf{EIoTC}.

Each \textbf{EIoTC} peripheral will have the following MQTT topics assigned to it:

\begin{itemize}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/device/data}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/device/control}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/device/control\_ack}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/device/stream}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/device/public\_face}
\end{itemize}

Each central will have the following topics assigned:

\begin{itemize}
    \item $<$ \texttt{eiotc\_peripheral\_id} $>$\texttt{/central/public\_face}
\end{itemize}

The peripheral topics are in order to have the control plane and the data plane of the device. The stream topic is for the peripheral to inform the central that it wishes to open a stream channel. 

For an explanation on the ``public face" topics see section~\ref{double nat}, but basically it is to create a shared UDP connection between the peripheral's LTE modem and the central. You may notice that even though the topic is for the central's public face, the peripheral's ID features in the topic name. This is to avoid opening a UDP with every device to the same UDP port at once; the central's public face is only published to the peripheral in question. 

For the MQTT broker, I have made the selection to use HiveMQ's public broker. The reason for this is that it is free and there is very little set up time. In the future it can be easily replaced without any code modifications.

\subsection{Network Address Translation}

In IPv4 there are a limited number of addresses to assign in the world; 4'294'967'296 to be exact. All because the computer scientists in 1981 who wrote the standard decided to fix the address size at 32 bits\cite{rfc_791}. (This means there are a total of $2^{32}$ addresses, thus 4'294'967'296). At the time this was more than enough. In 1981, the internet was something for professionals, it certainly wasn't made for everyone. In each IP packet there is a source address and a destination address, so in a way the address size was a trade-off to reduce message overhead. Nowadays the majority of the planet uses the internet and not necessarily just on one device. At the time 4 billion may have seemed like a large amount but in reality there are close to 8 billion people on planet earth\cite{world_pop}. Over half of the world's population has access to the internet\cite{half_internet} and a lot of those people would like to use more than one device. Not to mention we haven't even started to enumerate IoT devices. You can see how IPv4's limited address size doesn't allow for this.

There are methods to rectify this lack of addresses:

\begin{itemize}
    \item \textbf{Use IPv6} which has 128 bit addresses, meaning there are is a total possible $2^{128}=3.4\cdot 10^{38}$ addresses. This means everyone currently alive would have to use around $4.25\cdot 10^{28}$ devices each to fill the address space. We can assume this is far from likely.
    \item \textbf{Use NATs}: Network Address Translation is a system which groups devices together under one ``public address". An example of how this works can be seen in figure~\ref{fig:nat}.
\end{itemize}

I am going to present a dynamic NAT in this case because it is what we are confronted with in our own network. There is such thing as a static NAT but it is not used here. The NAT has a Network Address Translation Table, which is basically a routing device. Whenever a request from the private side is made it assigns a port to this device to be used with the public address. When the NAT receives a message on this port it replaces the address with the corresponding internal one. 

In our example (figure~\ref{fig:nat}, table~\ref{tab:nat}) the PC with address \texttt{10.0.0.42}  makes a request to a public server (\texttt{8.8.8.8}). When the message reaches the NAT, it replaces the private (or internal) IP address of the PC with the public IP address. In order to know which PC it corresponds to it assigns an external port (\texttt{8000}). When the public server responds to the \texttt{234.14.52.90:8000}, the NAT replaces this address and port with \texttt{10.0.0.47:3000}. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/nat.pdf}
    \caption{How a NAT works}
    \label{fig:nat}
\end{figure}

\begin{table}[!ht]
\begin{tabular}{|l|l|l|l|}
\hline
\textbf{internal IP address} & \textbf{internal port} & \textbf{public IP address} & \textbf{external port} \\ \hline
10.0.0.42                    & 3000                   & 234.14.52.90               & 8000                   \\ \hline
10.0.0.37                    & 55123                  & 234.14.52.90               & 64220                  \\ \hline
\end{tabular}
\caption{NAT table for figure~\ref{fig:nat}}
\label{tab:nat}
\end{table}

\newpage
\subsection{Double NAT Problem}
\label{double nat}
Network address translation is a workaround for IPv4 and it works remarkably well considering how much of a workaround it is. However, a problem arises when devices on separate private networks attempt to communicate.

In our project the Central PC and the LTE modem don't know their respective public addresses or ports for that matter but they need to establish a communication with one-another. The central PC is behind a NAT before connecting to internet, and the LTE modem is also hidden behind a NAT which is integrated in the eNodeB in the E-UTRAN system, see section~\ref{research}. When the peripheral and the central require a high speed communication protocol to connect, they simply can't because their public addresses and ports aren't readily available. This is visible in figure~\ref{fig:double_nat}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/double_nat.pdf}
    \caption{Representation of the double NAT problem, public addresses and ports unknown so the connection can't be made (marked with the dotted red line)}
    \label{fig:double_nat}
\end{figure}

This issue can be resolved in many ways. One protocol to establish multi-media communication between two mobile devices is to use SIP and RTP, a smaller part of IMS (IP-Multimedia Subsystem). These require a very large and complex network architecture to work, visible in figure~\ref{fig:ims}\cite{3gpp_ims}. They are perfect for mobile phone and fixed telephone communication but it is overkill in terms of this project. So many different physical elements are introduced into the network that it is not foreseeable to implement anything resembling new IP architectures. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/ims.png}
    \caption{IMS Architecture taken from 3GPP Specification 23.228\cite{3gpp_ims}}
    \label{fig:ims}
\end{figure}

This is good for changeover during roaming but for our we need a much simpler way to setup this communication. Thankfully there is a protocol known as STUN. 

\subsection{STUN Protocol}

The STUN protocol (Session Traversal Utilities for NAT) is a workaround to resolve the double NAT problem in networking. The central PC is behind a NAT before connecting to internet, but the LTE modem is also hidden behind a NAT which is integrated in the eNodeB in the E-UTRAN system.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/stun.pdf}
    \caption{Architecture of STUN protocol and double NAT problem}
    \label{fig:stun_double}
\end{figure}

The device must make a STUN request to the STUN server. The server can see its public address and port so it can reply with these values. Once the device knows its public data it can be transmitted to the other device it wishes to connect with. Normally this would be an issue because they can't directly connect but in the case of this project the exchange of ``public faces" is done with the MQTT public face topics. Once they each know their addresses, a ``UDP hole punch" must be performed. Each device transmits a message to the other at least once. That way both NATs can add the addresses and ports to their translation tables.

STUN works with UDP, generally on port 3478. Unfortunately it is only possible with UDP and not TCP. This is the main disadvantage of STUN. The reason it has been chosen is that it only relies on one server and has very little overhead.\cite{rfc_stun}

In the case where we need to control the elements in the network ourselves, it is important that we make it as simple as possible. With SIP for example, this would start to become a real problem.

The only external network element we need is a STUN server. I have decided to use ``stun.solnet.ch" because it is a Swiss server that is available. The advantage is that if Solnet decide to shut down their server, we only need to set-up one server to make the system work again.

\subsection{Network Sequence Views}

\subsubsection{Connection Establishment}

The way the two devices establish a connection is seen in figure~\ref{fig:seq_mqtt}. First each device will connect with the MQTT broker. The peripheral subscribes to the topics:

\begin{itemize}
    \item ``peripheral/control"
    \item ``peripheral/public\_face"
\end{itemize}

The central subscribes to the topics:

\begin{itemize}
    \item ``peripheral/data"
    \item ``peripheral/stream"
    \item ``peripheral/public\_face"
    \item ``peripheral/control\_ack" 
\end{itemize}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/sequence_mqtt_con.pdf}
    \caption{Sequence diagram of connection}
    \label{fig:seq_mqtt}
\end{figure}

\newpage
\subsubsection{Control Plane}

Whenever the user on the central PC wants to control a peripheral. The device is commanded through the control plane MQTT topics.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/sequence_mqtt_control.pdf}
    \caption{Sequence diagram of the control plane, excluding data requests}
    \label{fig:seq_mqtt_control}
\end{figure}

First the Central publishes to the control topic. When the Peripheral completes the command given, it publishes an acknowledgement with the respective channel. This puts the responsibility of checking whether the task has been completed to the Central. If the Central doesn't receive an acknowledgement, it can simply resend the command.

\subsubsection{Simple Data Transmissions}

As stated in the scope statement~\ref{scope}, simple data transmissions are transferred using the LTE-M modem. There can be two specific types of data transmission:

\begin{itemize}
    \item \textbf{Data indication} - the peripheral transmits this of its own accord
    \item \textbf{Data request} - the central makes a demand for data (done over the control channel) and the peripheral responds
\end{itemize}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/sequence_mqtt_data_simple.pdf}
    \caption{Sequence diagram of Data indication and Data request}
    \label{fig:mqtt_data_simple}
\end{figure}

\subsubsection{Data Switch}

In order to be able transmit data over LTE, there needs to be a switch mechanism. There are two different cases where LTE is activated:

\begin{itemize}
    \item The Arbiter algorithm of the Peripheral realises that the data channel needs to switch to high speed, figure~\ref{fig:data_switch}
    \item The Central PC requests a stream, figure~\ref{fig:high_speed_req}
\end{itemize}

It is fairly simple when the Peripheral needs to open a high speed socket, all it needs to do is request then publish its public face. The Central will understand this as a high speed open request, so it will then also make a STUN request. Once it publishes this public face to the broker, the peripheral will attempt a ``hole punch". The Central also needs to this to make sure that both NATs on either side are forced to add these new addresses to their table. Once this is done, an ACK is sent either way to make sure the connection is open.

When a high speed request is made from the Central, it is slightly more complicated. It is possible that the Peripheral already has a stream open. If this is the case, then it needs to send a message on the ``perpiheral/control\_ack" topic to inform the Central of this. 

Also if the Central opens the stream, it should also be able to shut the stream down. But the Peripheral cannot wait forever for this to happen so it needs a timeout mechanism to shut itself down.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/sequence_mqtt_data_switch.pdf}
    \caption{Data switch sequence view}
    \label{fig:data_switch}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/sequence_mqtt_request_high_speed_data.pdf}
    \caption{High speed data request sequence view}
    \label{fig:high_speed_req}
\end{figure}