\section{Energy Consumption}
\label{power model}

\subsection{LTE Cat 4 and LTE-M Energy Model}

The idea of this project is to prove (or not) that it is viable to create a hybrid protocol of LTE-M and LTE (more specifically Category 4 in our scenario). For it to be of any use we need to able to modelise the energy consumption of the modems. 

More specifically we need to modelise how much energy it takes to transmit a message of given packet size, so we are looking to model this function:

$$ E(s) $$

where
\begin{itemize}
    \item \textbf{E(s)} is the energy per packet size
    \item \textbf{s} is the packet size in bits or bytes (see further)
\end{itemize}

The \textbf{packet size} will be the determining element in the arbiter for the simple intuitive reason that LTE-M has a much \textbf{lower bitrate} than LTE-Cat 4. LTE-M has an uplink rate of \textbf{1 Mbps} \cite{3gpp_rel13}, whereas LTE-Cat4 has an uplink rate of \textbf{50 Mbps} \cite{3gpp_rel8}.  

So if the modems need to send a very long message, the radio of the LTE-M modem will be switched on for longer. Actually consuming more than the high bit rate modem. 

The energy per packet size will be modelised according to the following OSI-ISO\cite{osi} protocol layers:

\begin{itemize}
    \item \textbf{Physical layer (layer 1)}: the overhead of the protocols are not taken into account because we are quantizing bits sent with the radio directly
    \item \textbf{Link Layer (layer 2)}: the link layer is defined by the E-UTRAN network architecture for LTE and LTE-M 
    \item \textbf{Network Layer (layer 3)}: the next model should summarise how much energy it costs to send a message if we include the overhead of the transport layer: IP
    \item \textbf{Transport Layer (layer 4)}: UDP, the LTE modem  transmits messages to the central PC over UDP
    \item  \textbf{Session Layer (layer 5)}: MQTT, the LTE-M modem uses MQTT to communicate with the central PC
    \item \textbf{Application Layer (layer 7)}: EIoTC - dynamic connections and modem management taken into account
\end{itemize}

\subsubsection{Physical Layer - Radio}

There are a few things that are important for calculating the current in these modems:

\begin{itemize}
    \item Modem uplink current: $I_{tx}$
    \item Uplink bitrate: $r_{tx}$
    \item Radio ``on-time": $t_{tx}$
    \item Modem UART-on current: $I_{uart}$
    \item UART baudrate: $r_{uart}$
\end{itemize} 

There are two different powers to take into account: $P_{tx}$ and $P_{uart}$. The reason for this is that the PC must communicate with the modem via UART. This means the UART peripherals of the modems will be on and functioning during a message transmission. 

It is also important to note that the transmission time of the UART is not the same as the radio message transmission time. This is because of the different bit rates. In the figure~\ref{fig:phy_timing}, there is a timing diagram of the transmission of one message.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/phy_layer_timing.pdf}
    \caption{Timing diagram of one message transmission with a modem}
    \label{fig:phy_timing}
\end{figure}

This means we can estimate the energy it would take to send different packet sizes:

\begin{align*}
    E(s) &= E_{uart}(s) + E_{tx}(s) \tag{1} \\
    &= P_{tx} \cdot t_{tx} + P_{uart} \cdot t_{uart} \\
    &= V \cdot I_{tx} \cdot \frac{s}{r_{tx}} + V \cdot I_{uart} \cdot \frac{s}{r_{pc}} \\
    &= V \cdot ( I_{tx} \cdot \frac{s}{r_{tx}} + I_{uart} \cdot \frac{s}{r_{uart}})
\end{align*}
\newpage
where 

\begin{itemize}
    \item E(s) is the energy in Joules [J] to transmit a packet of size s
    \item s is the package size in bits
    \item P is the power consumed in Watts [W]
    \item t is the time in seconds [s]
    \item V is the supply voltage of the modem in Volts [V]
\end{itemize}


Consulting the data-sheets of both modems, we can collect the necessary data to complete the energy calculation, seen in table~\ref{tab:modem_data}.

\begin{table}[!ht]
\begin{tabular}{|l|l|l|l|l|l|}
\hline
\textbf{Modem} & \textbf{V [V]} & \textbf{$I_{uart}$ [A]} & \textbf{$I_{tx}$ [A]} & \textbf{$r_{uart}$ [bps]} & \textbf{$r_{tx}$ [bps]} \\ \hline
nRF91          & 5              & 0.8                 & 0.1               & 115'200               & $180\cdot 10^3$             \\ \hline
Telit          & 3.9            & 0.233                 & 0.8               & $1\cdot10^9$               & $50\cdot 10^6$           \\ \hline
\end{tabular}
\caption{Necessary data for modelising the energy consumption taken from the modem's data-sheets. The Telit provides NCM which as a throughput of 1Gbit/s over USB}
\label{tab:modem_data}
\end{table}

Using this data we can now calculate the functions from equation (1):

\begin{align*}
    E_{nrf}(s) &= 5 \cdot ( 0.1 \cdot \frac{s}{180\cdot10^3} + 0.8 \cdot \frac{s}{115.2\cdot10^3}) \tag{2} \\ 
    &= 7.5\cdot10^{-6}\cdot s 
\end{align*}

\begin{align*}
  E_{telit}(s) &= 3.9 \cdot ( 0.8 \cdot \frac{s}{50\cdot10^6} + 0.233 \cdot \frac{s}{1\cdot10^9}) \tag{3} \\
  &\approx 1.6233\cdot10^{-8}\cdot s 
\end{align*}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/energy_chart_1.pdf}
    \caption{E(s) for the physical layer of each modem, eqs: (2), (3)}
    \label{fig:energy_chart}
\end{figure}

These equations are graphed in figure~\ref{fig:energy_chart}.
As expected, the energy for the LTE-M modem climbs faster. This is because the low power modem has a lower uplink bit-rate, so it's switched on for longer.

\clearpage
\newpage
\subsubsection{Link Layer and Network Layer}

Our previous analysis was only concerning the hardware layer of the modems. That is to say bits directly transferred over the radio. In reality, this model is not fully representative because we can't just send the application data over the air on its own. The packet must contain information such as the destination, source, ... All of this extra information is contained within either a header or a trailer.

A representation of packet structure can be found in figure~\ref{fig:packet_oh}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/packet_oh.pdf}
    \caption{Packet overhead representation}
    \label{fig:packet_oh}
\end{figure}

The network layer in our specific case is IP (internet protocol), the packet structure of this protocol is represented in figure~\ref{fig:ip_packet}, as we can see there is only a header and no trailer.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/ip_packet.pdf}
    \caption{IP packet structure - RFC 791\cite{rfc_791}}
    \label{fig:ip_packet}
\end{figure}

The packet overhead is a static value of 20 bytes, if the packets where transmitted like this to the radio. However this is not the case; in section~\ref{research} we discover that IP header compression is performed in accordance to RFC 2507\cite{rfc_ip_compress} and that a CRC of 24 bits is appended to the end of the packet. With ideal compression, the header ends up being greatly reduced. In figure~\ref{fig:ip_compress}, we can see that the header size is reduced from 20 bytes to 12 bytes.

This means we have static elements of 12 bytes for the header and 3 bytes for the CRC, a total of \textbf{15 bytes}. When the eNodeB receives a packet it reverses the IP header compression, this means that the following rule still applies: 

In the IP protocol, the total package length is defined by a 16 bit value. This means that the maximum total length is 65'535. Because 20 bytes will always be reserved for the header, that means the maximum data length for one packet is 65'515. The implications of this are simple: if we have more than 65'515 bytes of data to transmit then they need to be sent over \textbf{multiple packets}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/ip_header_compression.JPG}
    \caption{IP Header Compression, RFC 2507\cite{rfc_ip_compress}}
    \label{fig:ip_compress}
\end{figure}

However it is not wise to send one big packet of 65'515 bytes. This is because the Maximum Transmission Unit is defined as 576 bytes in RFC 1122\cite{rfc_mtu}, referred to as the ``reassembly buffer size". This means the full message size should avoid going over this value. So the Packet Data Unit (PDU) size should be equal to or lower than \textbf{508 bytes} (maximum 60-byte IP header and 8-byte UDP header, RFC 768 \cite{rfc_udp}).  

In the AT Commands data-sheet for the Telit modem we can see that the default for PDU size is 300 bytes, figure~\ref{fig:pdu_size}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/socket.PNG}
    \caption{Default maximum PDU length for Telit Modem}
    \label{fig:pdu_size}
\end{figure}

It is probably best practice to not work directly with border values, so we can safely set our maximum PDU size as 300 bytes.

If we end up sending multiple packets then the static elements will be in each packet. To know how many fragmented messages there are, we can simply divide the TX packet size by the maximum PDU size, then floor this value. When we know how many messages there are in total we can multiply this by the static elements.

Taking this information we can insert it into our previous formulas (2) and (3), in order to calculate the new functions. The formulas (2) and (3) are used to calculate Joules per bit, so the values in bytes must be multiplied by 8:

\begin{align*}
  E_{nrf}(s) &= 7.5 \cdot 10^{-6} \cdot \lbrack s + q \cdot (1 + m_{frag})\rbrack \tag{4} \\
  &= 7.5\cdot10^{-6}\cdot \lbrack s + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300 \cdot 8} \rfloor) \rbrack 
\end{align*}

\begin{align*}
  E_{telit}(s) &= 1.6233\cdot10^{-8}\cdot \lbrack s + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack \tag{5}
\end{align*}

where
\begin{itemize}
    \item $q$ is the amount of static bits
    \item $m_{frag}$ is the amount of messages that result from the fragmentation
\end{itemize}

The equations (4) and (5) are graphed in figure~\ref{fig:energy_chart_2}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/energy_chart_2.pdf}
    \caption{E(s) for the network layer of each modem, eqs: (4), (5)}
    \label{fig:energy_chart_2}
\end{figure}

This is where it starts to become interesting because we can see the repercussions of fragmentation. When the message size surpasses 300 bytes (on the graph it's in bits so $300\cdot 8 = 2400$ bits), more energy is consumed because two messages must now be sent. Idem for 600 bytes. 

\subsubsection{Transport Layer}

For the transport layer, both modems use different protocols:

\begin{itemize}
    \item nRF Modem: \textbf{TCP} because it uses \textbf{MQTT} over \textbf{TCP}
    \item Telit Modem: \textbf{UDP} because it uses \textbf{STUN}
\end{itemize}

\subsubsection{nRF Transport Layer - TCP}

In order to modelise the overhead of the TCP protocol, we must understand how this protocol works. The TCP protocol is described in full in RFC 675\cite{rfc_tcp}. The figure~\ref{fig:tcp_seq} presents a simple sequence diagram containing a TCP connection.  

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/tcp_seq.pdf}
    \caption{Sequence diagram of a simple TCP connection}
    \label{fig:tcp_seq}
\end{figure}

When sending the TCP primitives SYN, SYN\_ACK, ACK and FIN\_ACK, we assume message length is 0 and only the header is sent. A diagram of a TCP packet header is visible in figure~\ref{fig:tcp_header}. It is 16 bytes long.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/tcp_header.JPG}
    \caption{TCP Header, courtesy of \url{https://en.wikipedia.org/wiki/Transmission_Control_Protocol}}
    \label{fig:tcp_header}
\end{figure}

Let's assume we only wish to send one message per TCP connection. This is actually reasonable because an IoT device is not going to be constantly transmitting all day and a TCP connection won't stay open forever. For example the default timeout in Windows is 72 seconds; which is rather short in the IoT world where we might only want to send one message per hour. It will be doing so at regular intervals for a short period of time so the battery lasts. 

With one message per connection, then the overhead is simple to calculate:

\begin{itemize}
    \item Overhead transmitted: 1 SYN, 2 ACK, 1 FIN,ACK
    \item Overhead received: 1 SYN,ACK, 1 ACK, 1 FIN,ACK 
\end{itemize}

Overhead is 7 primitives + the header of the actual data: $8\cdot16 = 128$ bytes. 

In order to simplify the power consumption model, we shall assume the connection is established without issue and there are no transmission errors.

Now we can insert these new findings into our equation (4):

\begin{align*}
E_{nrf} &= 7.5\cdot10^{-6}\cdot \lbrack s+128\cdot8 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300 \cdot 8} \rfloor) \rbrack  \tag{6}
\end{align*}

We don't include this new static overhead into the message fragmentation part of the equation because these primitives are all 16 bytes long and don't risk being fragmented, also they are all transmitted as separate messages. 

\subsubsection{Telit Transport Layer - UDP}

In the RFC 768\cite{rfc_udp}, we can see that there is very little overhead for UDP. There is also no ACK mechanism because the protocol is stateless unlike TCP. So it is merely a matter of adding some bytes to the static overhead! In UDP the header size is 8 bytes, so now we can determine the energy equation using equation(5):

\begin{align*}
E_{telit}(s) &= 1.6233\cdot10^{-8}\cdot \lbrack s + 8 \cdot 8 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack \tag{7}
\end{align*}

\subsubsection{Transport Layer Graphs}

Now that we have modelised the energy consumption on the transport layer for both modems in equations (6), (7), they can be graphed, figure~\ref{fig:energy_chart_3}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/energy_chart_3.pdf}
    \caption{E(s) for the transport layer of each modem, eqs: (6), (7)}
    \label{fig:energy_chart_3}
\end{figure}

Modelising the transport layer has borne some interesting results; notably how the use of TCP/IP by the LTE-M modem is penalising it on a consumption level. This is expected because of the connection setup required by TCP/IP. 
\subsubsection{Session Layer - MQTT}

Only the LTE-M modem uses this protocol so the Telit modem is \textbf{not} concerned by this part. 

MQTT sequence diagrams can be found in the previous subsection. 
The principle is fairy simple, an MQTT connection request is made to the broker by the IoT device. When successful, this broker responds with a ``CONNACK" message. 

The device must make some subscription requests, also detailed in the previous subsection.

Now that the device is connected and subscribed, it no longer needs to send these messages. This is because we never switch off the nRF modem, due to its PSM mode\cite{optimised_lte}. It will remain connected to the MQTT broker on the session level despite being in power saving mode. 
That means these connection and subscription messages are ideally only sent once in the whole time that the device is functioning. It doesn't make sense to add them to the equation (6), because if the device is running for years then the energy that was spent at the start is now negligible in terms of ``energy per packet size". 

However what does make sense is to model the MQTT publications that will be made by the device. MQTT actually has a very low overhead, the protocol header contains the following:

\begin{itemize}
    \item Control header (determines the type of MQTT message): 1 byte
    \item Packet length: maximum 4 bytes
\end{itemize}

We can insert this overhead into equation (6):

\begin{align*}
    E_{nrf} &= 7.5\cdot10^{-6}\cdot \lbrack s+(128+5)\cdot8 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300 \cdot 8} \rfloor) \rbrack  \tag{8} \\
    &= 7.5\cdot10^{-6}\cdot \lbrack s+ 1064 + 120 \cdot (1 + \lfloor \frac{s}{2400} \rfloor) \rbrack
\end{align*}

These 5 bytes barely make a difference compared to the previous equation. So there is no need to graph this part, it will be more interesting in the next section.

\subsubsection{Application Layer - EIoTC}

There remains only one layer left to modelise: the EIoTC layer itself. There is one crucial element left to make the consumption model valid:

\begin{itemize}
    \item The LTE modem must make a STUN request \textbf{every time it wishes to transmit data}
\end{itemize}

This is because we dynamically switch the LTE modem on and off. It is not like the nRF modem which goes into PSM. The Telit modem is actually switched off when not used. This means it must request its public data via STUN every time it wishes to transmit. 

The STUN protocol has a few different primitives, its definition is found in RFC 3489\cite{rfc_stun}.

The only messages that are useful in the case of this project are the two following:

\begin{itemize}
    \item BIND\_REQUEST - 28 bytes long
    \item BIND\_RESPONSE - 88 bytes long for \url{stun.solnet.ch}
\end{itemize}

An example can be found in figure~\ref{fig:stun_bois}, measured with Wireshark.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/stun_proof.JPG}
    \caption{STUN Protocol in action, Wireshark}
    \label{fig:stun_bois}
\end{figure}


It appears that the overhead is not that much, after all there are only 2 messages to handle. However the modem \textbf{must be switched on} while waiting for the STUN response. This whole stun scenario generally takes about some time to perform. This means the LTE modem is switched on idly for a short period. This adds to the overall overhead. 

We know that there is not just one case where the Telit modem is waiting on a UDP reply. During the hole-punching process, the modem sends \textit{and} waits for a hole punch message and a hole punch ``ACK". Respectively:

\begin{itemize}
    \item ``HOLEPUNCH" - 8 bytes
    \item ``HOLEPUNCH\_ACK" - 13 bytes
\end{itemize}

Meaning there is a total byte overhead of $28+88+8+13 = 137$ bytes

In total for this STUN and hole punch process to complete it takes about 3 seconds. We can include these elements in equation (7):

\begin{align*}
\\ \tag{9} \\
E_{telit}(s) &= 1.6233\cdot10^{-8}\cdot \lbrack s + 137\cdot8 + 8 \cdot 8 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack + V\cdot I \cdot t_{rx} \\
&= 1.6233\cdot10^{-8}\cdot \lbrack s + 1160 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack + 3.9\cdot 50\cdot 10^{-3}\cdot 3\\
&= 1.6233\cdot10^{-8}\cdot \lbrack s + 1160 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack + 0.585
\end{align*}


Now that we have the full energy modelisations of both protocols, we can graph them out to see if there are energy considerations that can be made directly from the visual, figure~\ref{fig:energy_chart_4}. Graphically, this has become a problem because the point of crossover is so far away. However both lines still \textbf{converge}. This means there is a point at which it is more viable to use the Telit modem.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/energy_chart_4.pdf}
    \caption{E(s) for the application layer of each modem, eqs: (8), (9)}
    \label{fig:energy_chart_4}
\end{figure}

\clearpage
\newpage 
\subsection{Solving the Energy Equation}

In order to know which packet size starts to become more viable for the LTE modem, we need to solve the following equation:

\begin{align*}
    (9) &     \hspace{0.4cm} (8) \\
    E_{telit}(s) &= E_{nrf} \\
    1.6233\cdot10^{-8}\cdot \lbrack s + 1160 + 15 \cdot 8 \cdot (1 + \lfloor \frac{s}{300\cdot 8} \rfloor) \rbrack + 0.585 &= 7.5\cdot10^{-6}\cdot \lbrack s+ 1064 + 120 \cdot (1 + \lfloor \frac{s}{2400} \rfloor) \rbrack \\
    1.6233\cdot10^{-8}\cdot s + 0.00261 + 0.585 &= 7.5\cdot10^{-6}\cdot s+0.00889\\
    s &\approx 77330
\end{align*}

We now know that in order to use the Telit modem, it would only become more profitable if we have a packet size that is over \textbf{77'330} bits, that corresponds to a package size of \textbf{9'667 bytes}. This may seem like a ridiculously large value. Let us put it into some perspective.

\subsubsection{Example 1}

The slowest of these modems has a UART interfaces with a baudrate of 115'200 bits per second. This corresponds to 14'400 bytes per seconds. So this 9'667 bytes packet size is not even one second of streaming!

This at least proves that if you have an IoT device that needs to sleep most of the time, but on rare occasions needs to stream, the EIoTC protocol would be the go-to solution.

\subsubsection{Example 2}

Let's develop some more here. If we have a device that needs to log some data, let's keep it simple: \textbf{a data logger}.

For example our logger could register a 32 byte piece of data every 2 hours. If the logger needed to transmit this directly to the Central PC, it would need to transmit these relatively small packets via the LTE-M modem. 

However, what happens when we accumulate these data in order to send a big burst in one go with the LTE modem? It is essential to calculate the period between bursts, to know if this is a viable solution. It is only possible if this information is not time sensitive, a logger is a perfect example of this. 
First we must check how many samples we can fit into our packet threshold of 13'250 bytes:

$$ n = \frac{h}{p} = \frac{9667}{32} = 302.09 \approx 303 $$

where
\begin{itemize}
    \item n is the amount of samples until the threshold is reached
    \item h is the threshold in bytes
    \item p is the amount of bytes in one sample
\end{itemize}

Now we can calculate the minimum time between data bursts:\\

\vspace{0.1cm}
$ t_{burst period} = n \cdot t_{sample} = 303 \cdot 2 \cdot 3600 = 2181600 s = 25.25 $ days


\subsubsection{Example 3}

Let's take a sensor as an example now. Using the same burst principle as the previous logger device. A sensor could register a 4 byte piece of data every 10 minutes. If this data were urgent, then it would need to transmit it to the Central PC immediately. For example a heat sensor could detect a fault in an electrical grid or something along those lines. This would need to be done with the LTE-M modem.

However, if the data is not urgent we can use the same ``logging" principle as before:

$$ n = \frac{h}{p} = \frac{9667}{4} \approx 2417 $$

Now we can calculate the minimum time between data bursts:\\

\vspace{0.1cm}
$ t_{burst period} = n \cdot t_{sample} = 2417 \cdot 10 \cdot 60 = 1450200 s \approx 17 $ days

