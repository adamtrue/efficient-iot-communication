\section{Energy Measurements}
\label{measurements_chapchap}
In this section, a method for measuring the energy consumption of the modems will be presented. The idea is that this section will either back up the theory presented in the calculations section~\ref{power model} or the theory will be disproved by practice. We know from this previous section mentioned that there are theoretical instances where the LTE modem is more efficient than the LTE-M (more specifically for relatively large message sizes). However in reality it's possible that this is not the case.

Now it needs to be put to the test in practice.

To obtain the energy used by the modems, we first measure the power. Then, we calculate the energy by integrating the power. This is done by measuring the voltage of a shunt resistor. The measurements are made with a Digilent Analog Discovery 2 (visible in figure~\ref{fig:digilent}).

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.3\textwidth]{figures/digilent.jpg}
    \caption{Digilent Analog Discovery 2}
    \label{fig:digilent}
\end{figure}

Let's take our diagram from before and modify it to be able to perform measurements, in figure~\ref{fig:measure_montage}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/measure_montage.pdf}
    \caption{Assembly of different elements and connections for the measurement system}
    \label{fig:measure_montage}
\end{figure}

During this process, the Digilent device must be connected to the PC and in logging mode. This is done with the Logging function in the \textbf{Waveforms} software. We activate it by hitting the ``Run" button and at the end we hit ``Stop". Once the data has been logged by the software it must be exported so that it can be handled with Excel and Octave afterwards, see subsection~\ref{data handling} on this. An example of how the Waveforms software functions is available in figure~\ref{fig:waveforms}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Inkedlogger.jpg}
    \caption{Digilent Waveforms - in red the button to export the logged data}
    \label{fig:waveforms}
\end{figure}


\subsection{Library Instrumentation for Tests}
\label{instrumentation}
In order to make measurements, the EIoTC Peripheral should not function as the protocol intends. What is really needed for measurements is a system with which to send messages of an increasing size at regular intervals. 

This means \textbf{disabling the arbiter} and performing a different set of tasks.

This is essentially very simple:

\begin{itemize}
    \item Start measuring the power consumption of the modems
    \item Then both modems are switched on and connected to the network
    \item Send a message of 100 bytes with the two, record the times at which they were sent
    \item Wait a bit and then do the same with a message of 200 bytes
    \item Wait a bit and then do the same with a message of 300 bytes
    \item etc.
    \item When the different necessary packets have been sent, save the packet times into a file.
\end{itemize}

This is why a \texttt{ModemTest} class is implemented. This class implements the \texttt{GenericStateMachine} interface. Its state machine is visible in figure~\ref{fig:test_state_machine}. This follows the basic structure explained before. It waits for the nRF modem to switch on and connect, after which it will do the same for the Telit modem. When they are both ready a message of a given size will be transmitted with each modem. The message size is increased by a predefined amount and then this is repeated. Each time a message is sent the time at which it finishes transmitting is registered. When the message length eventually reaches the maximum size defined in the \texttt{ModemTest} class, the program stops and saves all of the time logs to a file. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/modem_test.pdf}
    \caption{State Machine for the \texttt{ModemTest} class}
    \label{fig:test_state_machine}
\end{figure}

\clearpage
\subsection{Data Handling Script}
\label{data handling}
After the test program has finished, the data needs to be handled. Essentially the only things we have are the power curves over time and the times at which messages were sent. We need a way to transform this into energy per message size. These tasks will be performed by a scripting language. Generally in the industry, Matlab is preferred for this type of calculation. Matlab has a high price range, so in my case I have opted to use Octave; simply because it is free to use. But at a fundamental level such as this there is practically no difference between the two. The script in question is available in Annexe~\ref{an:octave}.

With the current, we can calculate the power consumption using the \textit{Power Law formula}:

$$ P = V\cdot I$$

where
\begin{itemize}
    \item P is the power in Watts [W]
    \item V is the voltage in Volts [V]
    \item I is the current in Amperes [A]
\end{itemize}

What is actually important to this project is not the power itself, it is the energy that certain transmissions take. 

The energy consumption of the IoT device is measured by integrating the power over time. This is because power is defined as ``the work done per unit of time". This means energy per unit of time. If the power is constant then that means:

$$ E = P\cdot t$$

where 
\begin{itemize}
    \item E is the energy in Watt-seconds or Joules [Ws] [J]
    \item P is the power in Watts [W]
    \item t is the time in seconds [s]
\end{itemize}

If the power changes over time, then this formula becomes:

$$ E(t) = \int P(t)dt $$

In fact the power is not a continuous value, it is discrete. Meaning that it has been sampled and quantised. To calculate the integral, we must use a known method for dealing with this. In our instance this is solved with the trapezoidal rule:

$$ E[k] \cong \sum_{i=0}^{k-1}\frac{P[i] + P[i+1]}{2} \cdot t_{log} $$ 

Intuitively this means a straight line is drawn between each sample, creating a series of trapezoids. Then we just calculate the surface of each of these trapezoids and add them together. A demonstration is presented in figure~\ref{fig:integration_method}. In the Octave script, this is achieved with the \texttt{cumtrapz} method.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/integration_method.pdf}
    \caption{Integration method for a quantized signal, the red signal is the analog signal, the blue is the quantized signal, the different times are points where the signal was sampled}
    \label{fig:integration_method}
\end{figure}

Waveforms' logging system can perform calculations on measurements and log the results. This means we don't need to export the voltage and then handle this data. We can adjust the logger to directly save the power value by entering its equation:


$$ P[k] = V_{supply} \cdot I = V_{supply} \cdot \frac{V_{shunt}[k]}{R_{shunt}}$$

where
\begin{itemize}
    \item $P[k]$ is the power calculated at point k in Watts
    \item $V_{supply}$ is the voltage of the power supply:
    \begin{itemize}
        \item 5V for the nRF modem
        \item 3.9 V for the Telit modem
    \end{itemize} 
    \item $V_{shunt}[k]$ is the measured voltage of the shunt resistor at point k
    \item $R_{shunt}$ is the value of the shunt resistor, both $0.5\Omega$
\end{itemize}

Now that the script has successfully calculated the energy, we must calculate the energy per packet. In the test code, we register the times at which packets are sent. 

It is simply a case of evaluating the energy at these different times and performing a subtraction. However we only have a discrete representation of the energy. If the times at which packets are sent are not in sync with the sample rate (which is highly likely) then there is no way to know the energy at these specific points. A function must therefore be interpolated from the discrete samples. A spline method is used to modelise the function so as to become continuous for this purpose. This is achieved with the Octave command \texttt{splinefit}. 

$$ E_{fit}(t) = splinefit(E)$$

where:
\begin{itemize}
    \item $E_{fit}(t)$ is the interpolated function of energy over time
    \item $E$ is the series of samples
\end{itemize}

Then we use the information on when the messages were sent and perform the difference.

$$ E(s) = E_{fit}(t_{end}) - E_{fit}(t_{start}) $$

where:
\begin{itemize}
    \item $E(s)$ is the energy spent for transmitting a given packet size s
    \item $t_{start}$ and $t_{end}$ are respectively the start and the end of the transmission of each message
\end{itemize}

\subsection{Measurement Results}

In this section, I will present to you the energy measurements taken using the \texttt{ModemTest} class in the software.

A series of different test situations were created and for each two graphs are presented. The first contains the power and energy measurements for both modems over time. It also presents the spline functions fitted to the energy samples (which are visually indistinguishable from the measurements). On the energy graph the times of sent messages are visible in the form of vertical dotted lines: the red ones for the Telit modem and the blue ones for the nRF modem.

In the second graph, there is a representation of the energy that has been measured to send messages of different sizes.

In the following figures, the measurements of different test situations are presented:

\begin{itemize}
\item Figures \ref{fig:mes1_1} and ~\ref{fig:mes1_2} - Message size ranges from 100 bytes to 1'000 bytes with an increment of 100 bytes every 0.5s
\item Figures \ref{fig:mes2_1} and ~\ref{fig:mes2_2} - Message size ranges from 500 bytes to 15'000 bytes with an increment of 500 bytes every 0.5s
\item Figures \ref{fig:mes3_1} and ~\ref{fig:mes3_2} - Message size ranges from 2000 bytes to 20'000 bytes with an increment of 2000 bytes every 0.5s
\item Figures \ref{fig:mes4_1} and ~\ref{fig:mes4_2} - Message size ranges from 2000 bytes to 30'000 bytes with an increment of 2000 bytes every 0.5s
\item Figures \ref{fig:mes5_1} and ~\ref{fig:mes5_2} - Message size ranges from 10'000 bytes to 50'000 bytes with an increment of 10'000 bytes every 0.5s
\item Figures \ref{fig:mes6_1} and ~\ref{fig:mes6_2} - Message size ranges from 20'000 bytes to 100'000 bytes with an increment of 20'000 bytes every 0.5s

\end{itemize}


\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes9_1.pdf}
    \caption{Power and Energy Measurements: 100 to 1'000 bytes, 100 bytes increase each 0.5s}
    \label{fig:mes1_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes9_2.pdf}
    \caption{Energy Cost per Message Size: 100 to 1'000 bytes, 100 bytes increase each 0.5s}
    \label{fig:mes1_2}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes10_1.pdf}
    \caption{Power and Energy Measurements: 500 to 15'000 bytes, 500 bytes increase each 0.5s}
    \label{fig:mes2_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes10_2.pdf}
    \caption{Energy Cost per Message Size: 500 to 15'000 bytes, 500 bytes increase each 0.5s}
    \label{fig:mes2_2}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes11_1.pdf}
    \caption{Power and Energy Measurements: 2'000 to 20'000 bytes, 2'000 bytes increase each 0.5s}
    \label{fig:mes3_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes11_2.pdf}
    \caption{Energy Cost per Message Size: 2'000 to 20'000 bytes, 2'000 bytes increase each 0.5s}
    \label{fig:mes3_2}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes12_1.pdf}
    \caption{Power and Energy Measurements: 2'000 to 30'000 bytes, 2'000 bytes increase each 0.5s}
    \label{fig:mes4_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes12_2.pdf}
    \caption{Energy Cost per Message Size: 2'000 to 30'000 bytes, 2'000 bytes increase each 0.5s}
    \label{fig:mes4_2}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes13_1.pdf}
    \caption{Power and Energy Measurements: 10'000 to 50'000 bytes, 10'000 bytes increase each 0.5s}
    \label{fig:mes5_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes13_2.pdf}
    \caption{Energy Cost per Message Size: 10'000 to 50'000 bytes, 10'000 bytes increase each 0.5s}
    \label{fig:mes5_2}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/Measurements/mes14_1.pdf}
    \caption{Power and Energy Measurements: 20'000 to 100'000 bytes, 20'000 bytes increase each 0.5s}
    \label{fig:mes6_1}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/Measurements/mes14_2.pdf}
    \caption{Energy Cost per Message Size: 20'000 to 100'000 bytes, 20'000 bytes increase each 0.5s}
    \label{fig:mes6_2}
\end{figure}

\clearpage
\subsection{Energy to Establish Connection}

Measurements were also made for how much energy it takes for each modem to connect. In the case of the nRF modem that means the energy from when it's switched on until when it has finished subscribing to the MQTT topics. For the Telit modem that means from when it is switched on until it has successfully passed the hole punching procedure. These measurements are available in table~\ref{tab:con_energy}.

\begin{table}[!ht]
\begin{tabular}{|l|l|l|}
\hline
\textbf{Test Index} & \textbf{\begin{tabular}[c]{@{}l@{}}nRF Modem\\ Connection Energy [J]\end{tabular}} & \textbf{\begin{tabular}[c]{@{}l@{}}Telit Modem\\ Connection Energy [J]\end{tabular}} \\ \hline
1                   & 0.67104                                                                            & 2.80714                                                                              \\ \hline
2                   & 0.722465                                                                           & 2.760395                                                                             \\ \hline
3                   & 0.88966                                                                            & 2.64227                                                                              \\ \hline
4                   & 0.705225                                                                           & 2.71035                                                                              \\ \hline
\end{tabular}
\caption{Energy it takes for each modem to connect}
\label{tab:con_energy}
\end{table}

\clearpage
\subsection{Measurement Conclusion}

There are a series of interesting points to note in these measurements. One is that the times to send messages grows visibly larger as the packet size increases, revealed by the pattern of widening vertical lines. This is most visible in figures \ref{fig:mes2_1} and \ref{fig:mes3_1}.

Another thing to note is that the power consumption of the nRF modem is always very low. Several orders of magnitude lower than that of the Telit modem. This was somewhat expected by the datasheets' description of the current consumption of both devices. However for it to be \textit{this} different is a real shock. 

The measurements do look realistic in that there is a more or less linear tendency in the amount of energy spent for message sizes, most visible in figure~\ref{fig:mes2_2}. Sometimes there are a few elements that do not respect this linear modelisation like in figures \ref{fig:mes3_2} and \ref{fig:mes4_2}. This is due to the quality of the network shifting causing more or less energy to be spent per byte. This is more visible in the power diagrams where the Telit modem's power consumption drops drastically because the quality of service has gone up. 

\textbf{The measurements presented in these figures do not support the theory} presented in section~\ref{power model}. The LTE modem consumes a greater amount of energy than previously assumed. As we can see we have largely surpassed the previously suggested threshold and yet the LTE modem is still less efficient. Not only that but by analysing the results, we can actually see that the slope of the Telit modem's ``energy per message size" curve is higher than that of the nRF modem. This is the most unexpected element in the measurements' results. Normally the theory stated that due to the low bit-rate of the nRF modem it would consume more energy to send longer messages.

\clearpage
\subsection{Measurement Divergence from Theory}

As stated the measurements diverge greatly from the theory. Now the question is why.

To ensure proper functioning, using a browser I launched a speed test on the modem, the results of which are in figure~\ref{fig:speed_test}. We can see that we are far from the maximum capacity of 50 Mbit/s upstream. This means we only have 15\% of maximum capacity, with an upstream of 7 Mbits/s. This largely contradicts our theory because this theory \textit{counts} on LTE having a much higher bit rate than LTE-M. This low bit rate could be due to a lot of different factors. Most notably:

\begin{enumerate}
    \item Tests are being done from an office inside a building so physical structure is blocking some of the signal
    \item The operator is capping the upstream
    \item Principles of ``work-at-home" and ``school-at-home" adapted to the current pandemic climate are putting a lot of strain on internet infrastructure, using a lot of bandwidth. 
    \item The NCM profile of the Telit modem does not accept maximum USB transmission rate (1Gbit/s)
    \item The PC is sending irrelevant data over the Telit modem because it is used as a network interface (irrelevant here meaning not relevant to the EIoTC application).
    \item The theory is based on a very idealistic situation that is not obtainable in real life.
\end{enumerate}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/speed_test.JPG}
    \caption{Speed test on Telit modem}
    \label{fig:speed_test}
\end{figure}

I am now going to approach each of these issues individually to determine which ones are legitimate risks for the project.

\subsubsection{Upstream data limited}

To tackle issue \textbf{number 1} and \textbf{number 2}, I decided to also launch the test on my smart phone to see if the results were similar, shown in figure~\ref{fig:smart_phone}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/data_rate.jpeg}
    \caption{Speed test on mobile phone}
    \label{fig:smart_phone}
\end{figure}

We can actually see that the download speed is perfectly fine. However the upstream is capped at 10 Mbit/s. My phone does not connect with LTE category 4 like the modem. It actually implements 4G advanced which is suited for higher bit rates than category 4 applications, meaning that it is more or less certain that the upstream is being capped by the operator. LTE advanced can allow speeds up to 300 Mbit/s and even 450 MBit/s in some cases\cite{ch_4g}. This is \textbf{not} conclusive proof but it is an indication at one of the problems. The reasoning behind it is reasonable: most people are using their phones to download data and not upload that much data in comparison. So the down stream is prioritised. We can safely assume that the up stream is capped. The downstream bit rate is far from the maximum value but it is high enough for multimedia and high bit rate applications, this means the fact we are indoors in an office is probably not the issue. 

\subsubsection{Network infrastructure strained}

Now issue \textbf{number 3} can be tackled by studying the literature. It is in fact the case that COVID-19 has negatively impacted the network. This is visible in figure~\ref{fig:covid_perf}, taken from the article ``How COVID-19 is affecting internet performance"\cite{covid_performance}. There is no data on Switzerland in the article. However, two of our biggest neighbours France and Italy are visibly heavily affected by the pandemic. Not to mention our other European counterparts in the study are all impacted negatively. 
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/covid-effect.JPG}
    \caption{How COVID-19 is affecting internet performance\cite{covid_performance}}
    \label{fig:covid_perf}
\end{figure}

Unfortunately only download speeds are mentioned. However I think we can conclude that up streams would be affected in a similar way. This also supports the theory that the operator would cap the speed. They have good reason to, with the high usage of the internet.

\subsubsection{Telit's NCM profile}

Issue \textbf{number 4} is that the Telit modem maybe does not respect the NCM profile fully. In the port configuration document\cite{telit_ncm}, there is a lot of information on how the NCM protocol is implemented. It lists also its compatibility with different drivers. So I don't think this issue is a possibility.

\subsubsection{PC transmitting irrelevant data}

Issue \textbf{number 5} states that there is reason to believe the PC could be transmitting data over the modem's interface when it's not supposed to. 

When running the tests, my PC was connected to internet via WiFi for work but it was also connected via LTE through the modem. The problem is that you cannot just configure one program to use one interface. That is not the way computers handle network traffic. Network traffic is dictated via ``IP routes". When an IP address needs to be accessed that the PC hasn't yet accessed, it requests its route. This is handled by a series of different protocols and essentially it can send this request through any of its accessible interfaces. This route will now be stored with the according interface. This is just a very simplified explanation. In reality, it is a lot more complex than this. However it still leaves us with a problem. Any app or process could decide it wants to use the Telit modem as its network interface. Causing packets to be sent that are not relevant to the EIoTC project. An example of this happening during a measurement is in figure~\ref{fig:shit_occuring}. In this figure the messages in white signalling ``Fragmented IP Protocol" and the single UDP message after these are transmitted in the EIoTC context. All of the other packets are irrelevant.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.2\textwidth]{figures/AddedBullshitToMeasurements3.JPG}
    \caption{Wireshark measurement of irrelevant messages being transmitted with the modem}
    \label{fig:shit_occuring}
\end{figure}

This is probably the biggest offender in the over consumption of the modem. With messages constantly being transmitted that are not of the EIoTC application, then the modem is consuming a lot of energy for nothing. This problem is actually easily solvable for embedded devices because they just don't have all of the automatic processes that Windows runs.

\subsubsection{Excessive Optimism}

The theory in section~\ref{power model} is overly optimistic about many things. Firstly it assumes no messages are lost in transmission. This is evidently impossible in real life. The quality of the service of the network is not perfect. Another problem is assuming that either of the modems would operate in prime conditions. For example, assuming that the bit rate of either modem would be at its peak is too good to be true. There are data rate drops due to many things. One last thing is that the theory does not consider moments when the modems are idling. This needs to be taken into account for the future of the project.

As seen in the section~\ref{research}, the Link Layer of the modem can perform fragmentation, concatenation and even has different error detection systems in place. There are systems in the link layer itself (ARQ and HARQ) that are used to request a retransmission if an error is detected, before it reaches the application layer. This means that if we are in a very noisy environment, some packages will have to be retransmitted many times meaning the energy consumption will be higher than a big packet in a non-noisy environment.


\subsubsection{Summary}

Having gone through all of the potential issues we can now summarise which ones are a threat: 

\begin{itemize}
    \item[\color{red}\faTimes] Tests from inside a building 
    \item[\color{green}\faCheck] Operator capping upstream
    \item[\color{green}\faCheck] Pandemic network strain, using a lot of bandwidth. 
    \item[\color{red}\faTimes] NCM profile of the Telit modem does not accept maximum USB transmission rate (1Gbit/s)
    \item[\color{green}\faCheck] The PC is sending irrelevant data over the Telit modem because it is used as a network interface
    \item[\color{green}\faCheck] The theory is based on a very idealistic situation that is not obtainable in real life.
\end{itemize}