\section{Scope Statement}
\label{scope}
\subsection{Justification}

This project is a collaboration with the team ECS (Embedded Communications Systems) at the HEI in Sion (part of the University of Applied Sciences Western Switzerland). This team is comprised of experts in low power wireless communications. This project for efficient IoT communication using LTE and LTE-M is hence a natural extension of their expertise.

\subsection{Context}
In telecommunications, LTE (commonly referred to as 4G by mobile users), is the most widely used cellular technology for mobile data transfers. The networks are deployed throughout the world, meaning that work with this protocol opens a lot of opportunities in the developed world.  

In recent years LTE communication infrastructures have been extended to LTE-M and NB-IoT: two protocols used in the context of IoT cellular communication (starting at LTE release 13). By dropping the bit rate and the bandwidth among other techniques, these protocols have become staples of cellular communication for low power devices. These solutions are ideal for devices that don't have to send too much information over the air. 

To reiterate on the example previously mentioned: a network of battery powered noise sensors deployed throughout a city. They need to send a message every 10 minutes to say how loud it is at their respective locations and their GNSS data so that they can be relocated dynamically, hence being independent in terms of its position. This requires a small quantity of data to be sent every 10 minutes by one device. It means that we can switch on its LTE-M radio for a fraction of a second every 10 minutes, meaning the energy consumption is \textit{very} low and the battery lasts for years. 

However, what if we wanted to extend the functionality of these sensors to stream sound over the air? We could send a command to an individual sensor asking for a live stream of the audio. This could be used when the sound is too loud to determine whether it is a concert polluting noise or just traffic. The issue is that this would require a much larger quantity of data to be sent. If we conserve the lower bit rate of LTE-M then the radio would be on for a long time causing a lot of power to be lost. Ideally, we would want small data packets transferred over the lower bit rate protocols (LTE-M) and large data packets to be transferred with the standard LTE network.

\subsection{Goals}

The goals of this thesis are currently defined as follows:

\begin{itemize}
    \item Develop a hybrid protocol of LTE and LTE-M, which is more energy efficient for the defined use cases when compared to just LTE-M
    \item The protocol should respect these guidelines: LTE-M for the control plane and small user data packages, LTE for large user data packages
    \item Develop an arbiter algorithm to decide which of the protocols to use at any given time
\end{itemize}

\subsubsection{Use Cases}
\label{chap:use_case}
\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/use_case.pdf}
    \caption{Use Case View for different elements within the protocol}
    \label{fig:use_case}
\end{figure}

There is one main system in the diagram in figure~\ref{fig:use_case}, which is the IoT infrastructure. Within this system there are 3 subsystems:

\begin{itemize}
    \item The LTE network
    \item The Things
    \item The interface to communicate with these Things
\end{itemize}

Essentially we have a network of sensors or devices, which we call ``Things". The Thing Company Employee can register and deregister these as they wish. A standard cloud user would be able to make remote requests for simple data or a stream from a Thing. They can also make control requests (this would mean activating a switch remotely or others).

A Thing may have an interactive element to it. This could be a button, a voice activated command, any kind of user-machine interface. If the Thing User decides to activate these functions they would be using the ``Operate" action. This will activate a data transfer, which sends a message over the LTE network. 

The Thing should be unaware of which protocol is used it should only know ``Data Transfer", not whether this is small or big data. It will be the protocol itself that determines which is used.

\subsection{Task Description}
\label{sec:tasks}
\subsubsection{Administration}

Administration includes the planning and other aspects of the project that aren't inherently related to the technological side of the project. This includes:

\begin{itemize}
    \item \textbf{Scope statement:} Writing and verifying this scope statement with the professor involved
    \item \textbf{Planning:} Planning the project
    \item \textbf{Risk Analysis:} Performing a risk analysis
\end{itemize}

\subsubsection{Research}

This part should be concerning with the aspect of how best to implement the protocol before proceeding. It should also be concerned with the current state and future state of affairs in the world of LTE and 3GPP. 

This will allow us to obtain an analysis of the market in order to know what is currently missing and also for future goals of the project. 

These tasks are separated into the following categories:

\begin{itemize}
    \item \textbf{Status Quo of LTE and LTE-M:} Research into LTE and LTE-M technologies
    \item \textbf{Competing Technologies:} Research into other similar technologies (LoRa, NB-IoT, 5G, ...)
    \item \textbf{Current Hybrid Technologies:} Research into other hybrid protocols, to better implement one
    \item \textbf{Modem Selection:} Selection of LTE modem and LTE-M modem
    \item \textbf{Antenna Selection:} Selection of antenna system
\end{itemize}

\subsubsection{Protocol Development}
    
This section concerns the development of the protocol itself. This means what role this protocol will play, which layer of the OSI Layer scheme it fills and what services are provided.

\begin{itemize}
    \item \textbf{Architecture:} Create an architecture constructed over the protocols LTE and LTE-M, but that still allows for easy modification if we wish to replace the aforementioned protocols
    \item \textbf{Sequence View:} Development of the sequence views that are required to explain the protocol
    \item \textbf{Arbitration Method:} Develop an arbitration technique with different modes and parameters for the test phase, to compare. Modes and parameters could be the following:
    \begin{itemize}
        \item LTE-M maximum transmission time
        \item LTE maximum transmission time
        \item Transmission vs sleep ratio
        \item The size of ``large data packets" (size at which data is sent over LTE instead of LTE-M)
        \item Buffering of data to send the same amount but less frequently
        \item etc.
    \end{itemize}
    \item \textbf{Software Model:} Develop a software model of the protocol for object oriented languages
\end{itemize}

\subsubsection{Hardware Prototype}

The next part of this project will be concerned with the conception of a prototype of en embedded device that uses the protocol to communicate. The development will entail creating a device with a processor such as an NRF91, STM32 or other micro-controller that interfaces with the selected modems in order to use the appropriate type of cellular communication. This undertaking includes integrating a SIM card and testing this on the deployed networks.  

\begin{itemize}
    \item \textbf{Material Selection:} Select an appropriate micro-controller and development kit for low power applications
    \item \textbf{Modem Switching:} Find a hardware method to easily switch between Modems 
    \item \textbf{Hardware Tests:} Connect the modems and test that the hardware functions correctly
\end{itemize}
    
\subsubsection{Prototype Firmware}

In this part of the project, the main goal is to take the software model from the section \textbf{Protocol Development} and implement it for the chosen embedded target, preferably in C or C++.

\begin{itemize}
    \item \textbf{Development of FW:} Write the firmware implementation of the protocol
    \item \textbf{Firmware Tests:} Run a series of different tests, checking that the different modes of operation of the protocol are all functional
\end{itemize}

\subsubsection{Energy and QoS Analysis}

In the world of IoT, these are the two most important factors to consider in protocol development. The protocol should hence be scrutinised over these factors. In order to so we will test the different input parameters such as modes of operation and transmission times.

\begin{itemize}
    \item \textbf{Test Scenario:} Create a test scenario which is repeatable for the different modes of operation, to effectively test the difference between the modes and parameters
    \item \textbf{Test Execution:} Run the tests over a period of time long enough to obtain sufficient samples for a good statistical analysis
    \item \textbf{Analysis Conclusion:} Use this to write an analysis report and conclude which of the modes is better for power conservation and which for latency reduction (if they differ)
\end{itemize}


\subsubsection{Documentation}

This part is fairly self explanatory, the project must be sufficiently documented.

\begin{itemize}
    \item \textbf{Report:} Write a project report (this will also contain the power and latency analysis)
    \item \textbf{Code Documentation:} Write the software implementation's documentation
    \item \textbf{Protocol Guide:} Write a user guide for the protocol
\end{itemize}

\subsection{Gantt Diagram}

To best represent the organisation of the different tasks, a Gantt diagram is visible in figure~\ref{fig:gantt}.

Each of the tasks listed under section~\ref{sec:tasks} (Task Description) is shown in the Gantt diagram as a \textbf{Main Task}. Each of these subsections are split into bullet points for different jobs, which are listed under \textbf{Task} in the Gantt diagram (as you can see each one has a corresponding title in bold to the graph).

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.2\textwidth]{figures/gantt.pdf}
    \caption{Gantt Diagram}
    \label{fig:gantt}
\end{figure}

\subsection{Deliverables}

\begin{itemize}
    \item A report containing the full development process and results. The results must state whether each task was completed to the full and a final report on the feasibility of the future of the project 
    \item A functioning prototype that implements the protocol
    \item An analysis of the power consumption for the different modes of operation of the protocol
    %\item An official documentation of the protocol and how to use it (preferably in Wiki form)
    %\item Programming documentation for the prototype's firmware
    %\item A log book of the meetings
\end{itemize}