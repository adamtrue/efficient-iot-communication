\section{Modem Selection}
\label{chap:modem}
\subsection{LTE-M Modem - nRF9160-DK}

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.2]{figures/nRF9160 DK.jpg}
    \caption{nRF9160-DK}
    \label{fig:nrf_dk_pic}
\end{figure}

The selected LTE-M modem, as previously mentioned, is the nRF9160-DK, shown in figure~\ref{fig:nrf_dk_pic}. This device is not actually a usable modem out of the box. It is, in fact, an experimentation board provided by Nordic Semiconductor for LTE-M and NB-IoT development. There is a software development kit that uses Zephyr and is built with ``west". We don't actually need to change any code here. All we need to do is compile the application ``serial\_lte\_modem". This application is just an AT modem for the nRF.

For this I had access to the Soprod tools for IoT development, this also works with Zephyr and nRF-SDK installed. These tools must be downloaded from the github. Once it's downloaded, the program must be compiled by executing the following command at this location \\\texttt{/soprod\_iot/nrf/applications/serial\_lte\_modem}:

\begin{minted}{bash}
$ west build --board=nrf9160dk_nrf9160ns --pristine
\end{minted}

This compiles the program and puts it into the file \texttt{./build/zephyr}. The program we need to take is \texttt{merged.hex}. This is not just the program, it is already merged with the bootloader. 

Then using the \textbf{Programmer} in the \textbf{nRF Connect} application, we can load this binary into the flash of the nRF91.

\subsubsection{MQTT Protocol}

In the documentation for the nRF modem:
\\\url{https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/applications/serial_lte_modem/doc/AT_commands_intro.html}
\\we can see that there are some custom MQTT commands:

\begin{itemize}
    \item MQTT Connect / Disconnect
    \item MQTT Subscribe / Unsubscribe
    \item MQTT Publish
\end{itemize}

The fact that MQTT is already implemented in the modem, that I know how to use it and that you can easily set up your own broker means that it is the best solution to communicate online for this modem.

\subsection{LTE Modem - Telit}

The next selection to make was that of the LTE modem. The requirements for this were that it was at least LTE category 4 compatible and it was compatible for European networks.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/skywire.png}
    \caption{Picture of the Nimbelink Skywire TC4EU with the embedded Telit LE910 v2}
    \label{fig:telit}
\end{figure}

The only easily available product on the market was the \textbf{Nimbelink Skywire TC4EU}, which is actually a product that integrates the \textbf{Telit LE910 V2} modem, visible in figure~\ref{fig:telit}.

The reason for selecting this modem is that it is easily available: on Digi-key for \$115, plus the fact that there is an extensive amount of documentation and manuals surrounding Telit modems.

This modem can not be directly connected to a PC due to its packaging (visible in figure~\ref{fig:telit}). However it does have USB pins, these will be connected in the Hardware section~\ref{chap:hardware}, visible in figure~\ref{fig:nimbelink_pinning}.

\subsection{Modem Interfaces}

\subsubsection{AT Interface}
\label{chap:at_int}

The main command interface for the modems is the ``Hayes command set". Also known as AT commands. These are commands that most modems implement so that an external PC can control connections and use the data channel. The command set provides mechanisms such as dialling, answering, hanging-up, etc. 

It is based over an ASCII format. Each instruction starting with the characters ``AT". For example 

\begin{itemize}
    \item Dial: \texttt{ATD}
    \item Answer: \texttt{ATA}
    \item Hang-up: \texttt{ATH}
    \item etc
\end{itemize}

By providing these mechanisms a PC can communicate over a telephone network. A modem implementing this interface can be in 2 different modes of operation:

\begin{itemize}
    \item Control mode, which is the default
    \item Data mode
\end{itemize}

In ``control mode" the modem will just interpret the instructions from the PC as commands and do what it is told to do. In data mode, the modem acts as a tunnel where it retransmits every message from the PC to the network. Data mode is entered by dialling a socket:

\begin{itemize}
    \item PC activates ATD command
    \item PC receives ``CONNECT" from modem
    \item Data mode is established
    \item Data mode ends if PC receives ``NO CARRIER" from modem
    or if PC transmits the escape sequence ``+++"
\end{itemize}

Now, despite the AT interface being a relatively old system in telecommunications, (dating from the 60s) these basic principles are still respected in the industry. As we can see in the datasheet of the Telit modem, visible in figure~\ref{fig:telit_at_commands}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/telit_at_example.JPG}
\caption{AT commands in modern modems are still respected - excerpt from the Telit LE910 Command Set document\cite{telit_commands}}
    \label{fig:telit_at_commands}
\end{figure}

\subsubsection{AT Interface and 3GPP}

The 3rd Generation Partnership Project has defined an extension for this Hayes interface to include useful mechanisms for the mobile telecommunications network\cite{3gpp_at}. 

The principle is that these extensions allow modems to provide even more useful mechanisms for a connected PC. Especially modems that are connected on a 3GPP network. The extension works in the following way: all of the new commands start with ``AT+". For example, to unlock the SIM card on a mobile modem, the command is ``AT+CPIN".

The device creators tend to implement custom commands as well. These are defined as starting with ``AT\#".

\subsection{USB Profiles}

\textbf{The nRF modem} is equipped with a USB port that can act as a J-Link programmer/debugger and a virtual COM port simultaneously. So for our project, we are merely going to use the COM port directly. Its AT interface is direclty available on this port, by sending the commands in ASCII format.

\textbf{The Telit modem} is equipped with USB pins, however there is no port directly on the board. This is why an adapter board must be created in order to connect the PC via USB. For more on this see section~\ref{chap:hardware}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/telit_port_arrangement.JPG}
    \caption{Example of port arrangement on Telit's LE920 Modem, where DTE is the PC}
    \label{fig:port_arrangement}
\end{figure}

The different possible USB configurations of the Telit modem are explained in the document ``LE910 V2 Series Ports
Arrangements"\cite{telit_port}. The figure~\ref{fig:port_arrangement} is taken from this document. The principle is as follows: the modem provides a variety of ``access points" which are customisable. These are basically different \textbf{P}acket \textbf{D}ata \textbf{P}rotocol (PDP) contexts; we will only need one. It also provides a list of different virtual COM ports. The connections between these access points and COM ports is also fully modifiable. This means we have to configure these elements.

Generally one of these COM ports will be used for the AT interface; the others are directly linked with the different access points.

\subsection{NCM}
\label{ncm_chap}

In the document\cite{telit_port}, we see that the Telit modem implements a USB profile known as \textbf{NCM} (Network Control Model). This is an interface that is generally used for external USB ethernet cards. Implementing it in the Telit modem means that the PC can be ``tricked" into thinking it is merely an ethernet board, and use it just as it would any other interface. 

To activate NCM, we must first set the USB configuration of the device to number 4. This is written in document and we reboot the device. We must also use the correct port configuration. In our case we use context ID number 1. This is just so we can configure multiple PDP contexts but we don't need this. These configurations are static so they will remain the same after a reboot. 

We follow Telit's NCM setup document\cite{telit_ncm} on how to set up NCM on the modem as a network interface:

\begin{itemize}
    \item The modem must be switched on
    \item We activate the NCM profile with Telit's custom \texttt{AT\#NCM} (figure~\ref{fig:ncm}) 
    \item Then we must request its connection parameters (IP address, IP mask, gateway IP address and default DNS IP address) with the command \texttt{AT+CGDCONTRDP} (figure ~\ref{fig:cgdcont})
    \item Then we must enter the network interface setup. (In Windows: Control Panel $ \rightarrow $ Network and Internet $ \rightarrow $ Network and Sharing Center $ \rightarrow $ Change Adapter Settings)
    \item We assign the previously mentioned connection parameters with the correct interface (figure ~\ref{fig:network_interface})
    \item After this we must add a static ARP route for this modem by first emptying the ARP cache. Then we check the MAC address of the interface manually (available in the same properties dialog as the previous step). And we add a route signalling the default gateway is available at this MAC address. This is done with the following commands in CMD or powershell:
    
    \begin{minted}{console}
    netsh interface ip delete arpcache
    netsh interface ip add neighbor <interface name> <gateway ip> <mac address>
    \end{minted}
\end{itemize}

\begin{figure}[!ht]
\vspace{0.3cm}
    \centering
    \includegraphics[width=0.3\textwidth]{figures/ncm.JPG}
    \caption{Activation of NCM on Telit modem}
    \label{fig:ncm}
\end{figure}

\begin{figure}[!ht]
\vspace{0.3cm}
    \centering
    \includegraphics[width=\textwidth]{figures/cgcontrdp.JPG}
    \caption{\texttt{CGCONTRDP} command on Context ID 1}
    \label{fig:cgdcont}
\end{figure}

\begin{figure}[!ht]
    \vspace{0.3cm}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/network_interface.JPG}
    \caption{Network Interface Setup on Windows}
    \label{fig:network_interface}
\end{figure}

The problem with this setup: most of it needs to be done manually. A device that willingly switches off and on this Telit modem must be capable of performing these same steps. There is a way to do this on Windows with a powershell script. The script developed for this project is available in annexe~\ref{an:octave}. This script \textbf{must} be launched by a system administrator. This implies that if our software needs to run this script, it must also be launched by an administrator. 

Once this script has been executed, the modem should become a functioning network interface. This is visible on my PC in figure~\ref{fig:pc_interface}, where \texttt{Network 3} is the modem in question.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.4\textwidth]{figures/pc_interface.png}
    \caption{Telit modem configured as NCM device}
    \label{fig:pc_interface}
\end{figure}