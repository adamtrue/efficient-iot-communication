\section{Hardware and Firmware}
\label{chap:hardware}
\subsection{Schematics}

An important part of this project is the hardware. As power measurements are to be made, we need to know values such as the current and the voltage. The hardware is represented in figure~\ref{fig:hardware_diag}. This circuit should provide the following functionalities:

\begin{itemize}
    \item An \textbf{Rx} channel to receive commands from the PC
    \item Ability to switch the Telit and nRF modems on and off directed by the PC
    \item Measurements of both currents consumed by these modems at regular high-speed intervals
    \item A \textbf{Tx} channel to transmit these measurements to the PC
\end{itemize}


\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/hardware_node.pdf}
    \caption{Hardware Node Diagram}
    \label{fig:hardware_diag}
\end{figure}

This is done by using an STM32 Nucleo as the brains of the hardware circuit. The reason for this is because the STM32 Nucleo is a very affordable development kit at only 9CHF. But it is also powerful for the price. Its system clock can run at 80 MHz, it has an Analog Digital Converter that can run at 3.2 MHz (for full ADC samples not just one cycle). Via its USB port you can simultaneously debug, use it as hardware storage and a COM port.

For the moment the IoT object is implemented on a PC with Qt. It communicates with both modems via USB. The nRF9160-DK provides a COM port with its USB interface, so we can plug it directly into the PC. 

An image of the NimbeLink Skywire device can be seen in figure~\ref{fig:nimbelink}. As we can see, there is no USB port to plug into. In the datasheet there are some pins to link the modem with USB, see figure~\ref{fig:nimbelink_pinning}. So, these are linked with a USB port on the board.

The communication with the Nucleo is also done with a COM port over USB.

There is a representation of all these elements in figure~\ref{fig:full_montage}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/full_montage.pdf}
    \caption{All of the elements in the node diagram represented with their image}
    \label{fig:full_montage}
\end{figure}


\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.2\textwidth]{figures/nimbelink.JPG}
    \caption{NimbeLink Skywire TC4EU}
    \label{fig:nimbelink}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/nimbelink_pinning.pdf}
    \caption{USB Pins for NL-SW}
    \label{fig:nimbelink_pinning}
\end{figure}

\clearpage
The only thing that needs to be developed is the EIoTC Power Circuit. This task was given to Alexandra Andersson of the electronics department with the following important elements:

\begin{itemize}
    \item It must be able to measure the current flowing through both modems [REDACTED see section~\ref{hardware_trouble}]
    \item The current measurements must be filtered for noise [REDACTED see section~\ref{hardware_trouble}]
    \item It must be able to switch on or off both modems
\end{itemize}


The basic model for the schematics looks as follows:

\begin{figure}[!ht]
    \centering
    \includegraphics[scale=0.3]{figures/schematic.pdf}
    \caption{Basic Schematics Model}
    \label{fig:basic_schem}
\end{figure}

The power supplies for each modem are first connected to a block named ``Power Control Block". For the Telit modem, this block merely contains a shunt resistor, the voltage of this shunt is measured, amplified and then connected to the Nucleo's ADC so we can read its value. 

The power control block for the nRF has one extra element: it contains a transistor that is controlled by the processor. This is so it can be switched off when needed. The reason it is not in the Telit's block is because the Telit provides an \texttt{ON\_OFF} pin, so we just connect this directly.

For the full schematics refer to annexe~\ref{schematic}.

\subsection{Firmware}

To program the Nucleo, we must use STM32CubeMX. This is a program that lets us configure the STM32's peripherals. The most important of these peripherals is the Analog Digital Converter. But all of the peripherals are configured this way; be it the GPIOs, ADC, DAC, UART, etc... 


\subsubsection{FreeRTOS and CMSIS-RTOS}

The firmware requires very simple tasks to be done:

\begin{itemize}
    \item Dealing with the RX channel
    \item Writing on the TX channel
    \item Switching pins on and off to switch the modems on and off
\end{itemize}

It is best practice to use some kind of real-time operating system when developing firmware for a device like this. Even though this program is very simple, in the future an RTOS will make it much easier to extend and modify. It brings many benefits like multitasking, task scheduling, inter-task communication and deterministic behaviour. 
STM32CubeMx actually provides us with a way to select an OS when building a project. The OS in question is the Real-Time Operating System FreeRTOS. 

Other than being directly integrated in STM32CubeMx, another reason to chose FreeRTOS is that it is the market-leading RTOS distributed freely under the MIT open source license. It is a reliable and easy to use OS with an active community and long-term support.

When selecting FreeRTOS in STM32CubeMx, the code generator adds a wrapping layer called CMSIS-RTOS. CMSIS (Cortex Microcontroller Software Interface Standard) is a hardware abstraction layer for microcontrollers that is based on Arm Cortex processors. It provides interfaces to the processor and peripherals, real-time operating systems, and middleware components. In our case, the CMIS-RTOS API allows calling standardised functions instead of direct FreeRTOS Kernel primitives as shown in figure \ref{fig:cmsis-rtos} extracted from the ST User manual ``Developing applications on STM32Cube with RTOS".

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/CMSIS-RTOS.png}
    \caption{CMSIS-RTOS architecture}
    \label{fig:cmsis-rtos}
\end{figure}


\subsection{Firmware Architecture}

The firmware architecture is exceedingly simple for the moment. There is a task that needs to perform basic actions when the PC sends it commands. This task is the RxTask which basically receives one character by interrupt and performs the following actions in accordance:

\begin{itemize}
    \item `P' - start power measurement
    \item `S' - stop power measurement
    \item `A' - switch on Telit modem
    \item `B' - switch on nRF modem 
    \item `C' - switch off nRF modem
\end{itemize}