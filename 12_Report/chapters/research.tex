\section{Research}
\label{research}
In order to correctly develop any kind of protocol, we must first research the current state of affairs in the world and in Switzerland. This project actually started during the rollout of 5G in the Swiss network. This means our project could actually become very interesting as more and more users migrate to 5G, there will be more bandwidth available in the 4G networks.

\subsection{Radio Transmissions in the World}

The long-term evolution (LTE) (commonly known as 4G) and its network architecture are defined by the 3rd Generation Partnership Project (3GPP). 

The 3GPP is an international partnership program uniting seven big telecommunications companies with the aim to standardise worldwide radio communications. It is comprised of the following partners:

\begin{itemize}
    \item \textbf{ARIB} - The Association of Radio Industries and Businesses, Japan 
    \item \textbf{ATIS} - The Alliance for Telecommunications Industry Solutions, USA
    \item \textbf{CCSA} - China Communications Standards Association
    \item \textbf{ETSI} - The European Telecommunications Standards Institute
    \item \textbf{TSDSI} - Telecommunications Standards Development Society, India
    \item \textbf{TTA} - Telecommunications Technology Association, Korea
    \item \textbf{TTC} - Telecommunication Technology Committee, Japan
\end{itemize}

The reason this is interesting is that it means our protocol has potential compatibility with all of the concerned regions: Japan, USA, China, Europe, India and Korea.

\subsection{Evolved Packet System}
\label{EPS}
The LTE link layer and network architecture are defined in 3GPP Release 8\cite{3gpp_rel8}. It is important to understand how this system works in order to create a protocol that will effectively use it. The standard provided by 3GPP is very dense technical documentation and can be difficult to decipher, that is why we are working with a reference document published in the IEEE Communications Magazine, The LTE Link-Layer Design\cite{lte_link}.

In this release the Evolved Packet System (EPS) is detailed for the first time. The EPS is a network architecture that consists of 2 parts:

\begin{itemize}
    \item The Evolved Packet Core (EPC)
    \item The Evolved UTRAN (E-UTRAN)
\end{itemize}

Devices that use LTE for internet access and communication are referred to as User Equipment (UE).

In order to get internet access, the UE will first connect with an enhanced NodeB. These are essentially radio base stations, with some extra capabilities. The most notable one being that they can dynamically handle hand-overs with one another, thanks to the X2 interface. This mechanism removes some of the connection overhead from the device itself. This architecture is visible in figure~\ref{fig:eutran}. 

The EPC part of the network is in charge of providing internet access to UEs via the eNodeBs. The Mobile Management Entity (MME) operates in the control plane. Whereas the nodes known as Packet Data Network Gateway (P-GW) and Serving Gateway (S-GW) operate in the data plane.

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/lte_link_layer_arc.pdf}
    \caption{E-UTRAN Architecture}
    \label{fig:eutran}
\end{figure}

In the article in question\cite{lte_link}, it presents the protocol stack for the UE, I have taken this in figure~\ref{fig:eutran_layers}. 

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{figures/lte_link_layer_stack.pdf}
    \caption{UE Protocol Stack}
    \label{fig:eutran_layers}
\end{figure}

Let's breakdown the different responsibilities of these protocol sublayers:

\begin{itemize}
    \item PDCP - Packet Data Convergence Protocol is mainly used to compress IP headers in order to reduce the packet overhead. The IP header compression technique is found in RFC 2507\cite{rfc_ip_compress}
    \item RLC - Radio Link Control provides concatenation and segmentation primitives. It also performs package re-ordering and provides ARQ, en error handling mechanism if any errors have made it through HARQ
    \item MAC - handles scheduling, random access and provides HARQ: an extremely lightweight system that handles retransmissions of transport packages with errors, so that applications with a low error tolerance still work (such as audio / multimedia codes)
\end{itemize}

It is important to demonstrate how each of these protocol sublayers work for the power consumption model in section~\ref{power model}. A demonstration from Ericsson Research's article\cite{lte_link} is represented in figure~\ref{fig:lte_link_packets}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/lte_link_layer_messages.pdf}
    \caption{E-UTRAN Protocol Sub-layers}
    \label{fig:lte_link_packets}
\end{figure}

This diagram represents the data flow from the IP layer down through the link layers. There are 2 payloads that need to be sent. In the PDCP layer the IP headers are compressed into the smaller headers ``H". These blocks are then transferred down to the RLC layer which segments or concatenates messages according to the ideal transmission size. The message that the UE wishes to transmit is in the ``transport block". The physical layer appends a CRC of 24 bits as a trailer. 

On reception LTE's error handling is very simple, if an error is detected the message is dropped thanks to this CRC, this is because most protocols on the IP level are not prepared for bit level errors. The HARQ in the MAC layer will request a retransmission from the sender if this is the case. If the HARQ doesn't detect an error, the packet is transmitted further up the chain. ARQ operates at the RLC level, with a similar principle to HARQ: if an error is detected it requests a retransmission from the sender.

This was an overview into how the protocol works on a packet level. Now let's delve into how it operates time-wise. Obviously the device cannot just transmit bits into the air and expect them to be forwarded to the correct receiver. It needs to establish a connection with the eNodeB and then they can exchange data. More specifically it needs a Radio Resource Control (RRC) connection to be established.

An RRC is a way for the eNodeB to control its resources. Basically once the RRC connection is established with a UE, this UE will keep this resource, but not indefinitely. If a device is inactive for a period of time ``$T_i$", the RRC will be released. A system to calcuate the best value for energy consumption can be found in an article from the Department of Signal Theory from the University of Granada\cite{optimised_lte}.

In order to create this RRC connection the UE initiates what is known as the Random Access Procedure, the sequence diagram is visible in figure~\ref{fig:seq_ra}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/random_access.pdf}
    \caption{Sequence diagram for Random Access Procedure}
    \label{fig:seq_ra}
\end{figure}

\begin{enumerate}
    \item UE sends random access preamble, which is a randomly selected sequence from a set of available sequences
    \item eNodeB detects the preamble and send back timing data and other parameters in a random access response
    \item UE then sends its unique identifier
    \item eNodeB echoes back this unique identifier. In the case where two UEs tried to connect using the same preamble, they would record the same connection parameters with the RA Response and both sent their ID in the RA Message. When the eNodeB echoes back the ID, the UEs will know if it has correctly registered them or if they must restart the RA procedure.
\end{enumerate}

\subsection{DRX and PSM}

LTE is first defined in 3GPP Release 8\cite{3gpp_rel8}, LTE-M is a version of LTE defined in 3GPP Release 13\cite{3gpp_rel13}. They use the same network architecture, so the principals explored in subsection~\ref{EPS} are valid for both protocols. 

There are two mechanisms that also need to be explained to understand the differences between the two protocols. In the article already previously mentioned\cite{optimised_lte}, they explore the concepts of Power Saving Mode (PSM) an Discontinuous Reception (DRX). 

\begin{itemize}
    \item Discontinuous Reception is a system used by the UE where it can enter low power mode during short intervals. This principle can help reduce its power consumption. This system has two modes: Connected Mode and Idle Mode.
    \item Power Saving Mode is a mechanism whereby the device can remain connected to the network but is not actually reachable. The energy consumed during this mode is close to being switched off altogether. Only when the device needs to transmit does it switch on. Then it leaves its radio on for a short moment in case it needs to receive. After this period it will return to PSM idle mode.
\end{itemize}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/drx_psm.pdf}
    \caption{LTE-M Modes (O: Off, C: Communication, I: Inactive)\cite{optimised_lte}}
    \label{fig:drx_psm}
\end{figure}

In the figure~\ref{fig:drx_psm}\cite{optimised_lte}, there is a timeline representing the here the state of the device using the DRX and PSM. 

\begin{itemize}
    \item Off: Device is in PSM idle mode
    \item Communication: Device needs to transmit a packet
    \item Inactive: Device is connected but not transmitting
\end{itemize}

We can see that PSM is used at the start. Then, when the device needs to transmit data, it switches on its radio and starts to transmit. Once the transfers are completed, it waits in full reception mode for time $T_{DRXi}$. It will then wait a certain number of DRX cycles in discontinuous reception mode, then it reverts back to PSM idle mode.

\subsection{LTE Cat-4 and LTE-M}

Now we need to establish the main differences between the two protocols. Using their definitions in the standards 3GPP Release 8\cite{3gpp_rel8} and 3GPP Release 13\cite{3gpp_rel13} respectively, we can list the differences, visible in table~\ref{tab:ulink}.

\begin{table}[!ht]
\centering
\begin{tabular}{|l|l|l|}
\hline
\textbf{Parameter} & \textbf{LTE Cat 4} & \textbf{LTE-M}   \\ \hline
Downlink rate      & 150 Mbit/s           & 1 Mbit/s       \\ \hline
Uplink rate        & 50 Mbit/s           & 1 Mbit/s        \\ \hline
DRX Implemented    & Yes                & yes (eDRX)       \\ \hline
PSM Implemented    & No                 & yes              \\ \hline
\end{tabular}
\caption{LTE and LTE-M uplink/downlink}
\label{tab:ulink}
\end{table}

The most notable differences are the bit rates, which are up to 150 times higher for LTE Cat 4. Other differences include the fact that Power Saving Mode is \textbf{not} implemented in LTE Cat 4. 

While both protocols implement DRX, the two versions are slightly different. With DRX the time that a device would sleep is normally dictated by the network (it's usually  1.28 or 2.56 seconds). Whereas with extended DRX (eDRX)
the sleep time is actually dictated by the device itself. In LTE-M this time can go be set to a maximum of 43 minutes (with a minimum of 320 miliseconds). The even better thing about this is that it is a dynamic value. That is to say that at \textit{any} point the application program can modify this time!

\subsection{IoT Protocols Worldwide}

As of today the most widely used wide access IoT protocols used are LTE-M and NB-IoT. On a deployment map provided by GSMA in figure~\ref{fig:gsma}, we can see that most countries have deployed one or both of these protocols.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/iot_deployed.JPG}
    \caption{World Deployment Map courtesy of \url{https://www.gsma.com/iot/deployment-map/} (Novemeber 2020)}
    \label{fig:gsma}
\end{figure}

\subsection{Current Situation in Switzerland}

In Switzerland at the moment there are several active protocols for mobile networks, most notably, with the recent roll out of 5G. Swisscom provide online visualisation for 3GPP protocol deployment geographically. In figure~\ref{fig:swiss_4g}, and figure~\ref{fig:swiss_5g}, we graph Swisscom coverage of 4G/4G advanced and 5G/5G-fast in Switzerland. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/swiss_4g.JPG}
    \caption{Swisscom 4G and 4G advanced coverage in Switzerland \url{https://scmplc.begasoft.ch/plcapp/pages/gis/netzabdeckung.jsf?netztyp=lte}}
    \label{fig:swiss_4g}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/swiss_5g.JPG}
    \caption{Swisscom 5G and 5G fast coverage in Switzerland \url{https://scmplc.begasoft.ch/plcapp/pages/gis/netzabdeckung.jsf?netztyp=lte}}
    \label{fig:swiss_5g}
\end{figure}
