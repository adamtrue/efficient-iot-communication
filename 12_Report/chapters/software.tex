\section{Software}

The software for EIoTC must be constructed around the principles from the ``Network Sequence Views" from before. 

In order to do this, the following packages were assigned: 

\begin{itemize}
    \item \texttt{Modem} - for the classes of the different modem types and their commands
    \item \texttt{XF} - for the classes of an execution framework
    \item \texttt{StateMachines} - for the state machines that function under the XF
    \item \texttt{Hardware} - classes that deal with the hardware controls and data
    \item \texttt{Stun} - everything related to the stun protocol
    \item \texttt{Multimedia} - used to test the streaming methods services provided by EIoTC
    \item \texttt{Test} - mechanisms used to instrument the code
\end{itemize}

\subsection{Qt Library}
When designing protocols, it is very important to have asynchronous callbacks and other mechanisms in your code. This is so it is ``event based". The reason it is so important is that this is not just a simple program that executes command after command and then it's done. A protocol must react to types of messages, have assigned state diagrams to never have any unpredictable behaviour. For these reasons, the program was developed using Qt, thanks to its signal/slot mechanism. A diagram of this mechanism can be found in figure~\ref{fig:qt_slot}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/qt_slot.png}
    \caption{Qt Signal-Slot system}
    \label{fig:qt_slot}
\end{figure}

This figure does not explain much at first sight but this mechanism is in fact very powerful. \texttt{QObject} is a class provided by Qt. It represents a generic object. These QObjects can have signals and/or slots which are special types of methods. A signal is connected to a slot using the \texttt{connect} method. When this signal is ``emitted", all of the connected slots are then called. To explain let's look at an example, see figure~\ref{fig:qt_example}. In this example there is a class diagram. There are 2 different classes: \texttt{Object1} and \texttt{Object2} that inherit from \texttt{QObject}. 

\texttt{Object1} has two signals: \texttt{hello} and \texttt{message}. These are respectively connected to \texttt{Object2}'s slots: \texttt{world} and \texttt{print}.

There is a \texttt{Factory} to build, link and start the system. How it works can be seen in the sequence diagram in figure~\ref{fig:qt_example_seq}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/qt_slots.pdf}
    \caption{Class diagram for the Qt example}
    \label{fig:qt_example}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/qt_slots_sequence.pdf}
    \caption{Sequence diagram for the Qt example}
    \label{fig:qt_example_seq}
\end{figure}

In this sequence diagram, we can see that the \texttt{Factory} creates the two elements, then links them by using \texttt{QObject}'s \texttt{connect} method to attach the slots to their respective signals.

Now whenever \texttt{Object1} emits one of the signals, the respective slot is called. This is an extremely powerful tool in protocol development and is the reason Qt was selected as the framework. \texttt{Object1} and \texttt{Object2} have any relationship between them, yet it is possible to dynamically like the two. It is even possible to pass arguments through this system.

\subsubsection{Notation}

A lot of the following software that needs to be presented uses this ``signal/slot" construction. So, in order to clarify the notation we will use the UML tools of ``required/provided" to represent these elements respectively. Let's take our previous example and show it using this new format: figure~\ref{fig:qt_ex_rp}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/qt_slots_rp.pdf}
    \caption{Class diagram of Qt example with UML ``required/provided" notation representing ``signal/slot" mechanism}
    \label{fig:qt_ex_rp}
\end{figure}

\subsection{Package Diagram}

In figure~\ref{fig:package} a package diagram of EIoTC is shown. The whole program is constructed by a \texttt{Factory}. The goal of which is to build all of the different elements and link them together.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/package.pdf}
    \caption{Package diagram of the software}
    \label{fig:package}
\end{figure}

Let's go through each of the packages in the diagram to see what they offer to the broader picture of the software.

\subsection{Modem}

In this package their is a generic interface for modems. Each modem class must ``implement" this abstract class. 

\newpage
\clearpage

%\begin{figure}[!htbp]
%    \centering
%    \savebox{\savefig}{\rule{10cm}{5cm}}
%    \rotatebox{90}{%
%        \begin{minipage}{\wd\savefig}
%%            \usebox{\savefig}
%\hspace{-9.5cm}            \includegraphics[height=1.1\textwidth]{figures/modem_class_diag.pdf}
%            \caption{Class Diagram for the Modem Package}
%            \label{fig:modem_class}
%        \end{minipage}
%        }
%\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/modem_class_compressed.pdf}
    \caption{Class Diagram for the Modem Package}
    \label{fig:modem_class}
\end{figure}

At first this class diagram may appear somewhat complex but it is actually quite simple. Essentially there are 5 main classes. There is a generic modem that implements some AT commands and has a specific serial port. This serial port is an \texttt{EIoTCSerialPort}. This class acts as an abstraction layer between this program and the Qt serial ports. Then the two modems are modelised with their respective classes. These are connected with signals and slots to communicate public faces. The rest is just details or functions used for the state machines.

\subsubsection{Arbiter}

Then there is the Arbiter. The Arbiter is the class that will decide which modem is switched on. It will also act as the element through which the user will interface with EIoTC. 

There are two specific cases where the EIoTC protocol can be used (the reason is clarified in section~\ref{power model}):  

\begin{itemize}
    \item Low power IoT devices that occasionally require a multimedia channel
    \item IoT sensors and loggers, that can buffer data if it is not urgent and burst it every once in a while 
\end{itemize}

The Arbiter must cater to these use cases. While the use cases may be narrow, they exist nonetheless and it is surely worthwhile to develop an Arbiter algorithm for dynamic modem switching. 

After the energy consumption models (view section~\ref{power model}), this part is relatively easy. The EIotC stack would need to provide an interface of the following form:

\begin{minted}{c}
sendMessage(string: QString, isUrgent: bool, isMultimedia: bool): void
\end{minted}

Essentially the Arbiter performs this primitive as presented in the Activity diagram in figure~\ref{fig:activity_arbiter}. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/arbiter.pdf}
    \caption{Activity diagram suggestion for the Arbiter's ``send" primitive}
    \label{fig:activity_arbiter}
\end{figure}

In section~\ref{power model}, we discuss the existence of a threshold value. Which is the message size from when it is more efficient to use LTE instead of LTE-M. 

When the application decides to send a message, it must first measure the message size. If this size is bigger than the threshold, the Arbiter will switch on the Telit modem and transmit the message via LTE. Once the message is transmitted, it will simply switch the LTE modem off.

If the message is smaller than the threshold, it must determine if it is urgent, multimedia or just normal data to be buffered. If it is urgent and not multimedia it is directly transferred to the LTE-M modem. If it is urgent and multimedia, the LTE modem would be switched on and streaming activated. If it is normal data, it is stored in a buffer. When the buffer size reaches its threshold, the LTE modem is switched on and burst activated. Which is where the Telit modem will transmit all of the messages in one go.

\subsection{XF}

An XF is an execution framework that allows a programmer to create state machines in a simple way. The basic pattern principle was developed by the ECS Team at the HES-So Valais/Wallis.  If you will it is a Scheduler or an Event Dispatcher. The class diagram can be seen in figure~\ref{fig:xf_class}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/xf_class_compressed.pdf}
    \caption{Class view of the XF package}
    \label{fig:xf_class}
\end{figure}

The way this one works is extremely simple. A \texttt{GenericStateMachine} interface is provided. This contains a method known as \texttt{processEvent}, which is called whenever the XF handles an event. These state machines are created in a Factory, then added to the vector of state machines.

The XF respects the Singleton pattern because there only needs to be one event dispatcher in this scenario.

It contains a \texttt{QTimer}. This is a timer by Qt that provides a timeout signal when the count is reached. We have set this timer to 100 miliseconds. This allows us to handle events 10 times per second. This does not mean we are limited to 10 events per second because multiple events can be processed in one time lapse. 

In the activity diagram in figure~\ref{fig:xf_act}, there is a more clear image of how it works. Events are stored in a queue, this is because the \texttt{QQueue} has a FIFO architecture. This means events can be pushed into the queue, and then they are popped out of the queue in this same order, like a waiting line (First In - First Out). 

Every time the XF is called to process events, it will pop them from the event queue if they are still available. Each event is processed by all of the state machines via the \texttt{processEvent} method.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/xf_activity.pdf}
    \caption{XF Activity Diagram}
    \label{fig:xf_act}
\end{figure}

\clearpage
\newpage
\subsection{State Machines}

The class diagram of the State Machines package is the following:

\begin{figure}[!ht]
    \centering
    \includegraphics[width=1.2\textwidth]{figures/state_machine_class.pdf}
    \caption{Class Diagram of the State Machine Package}
    \label{fig:state_machine_class}
\end{figure}

There are two state diagrams in the software, one for each modem. They are visible in the following diagrams: figure~\ref{fig:nrf_state_machine} and figure~\ref{fig:telit_state_machine}. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/nrf_modem_sm.pdf}
    \caption{State Machine of nRF91 Modem}
    \label{fig:nrf_state_machine}
\end{figure}

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/telit_modem.pdf}
    \caption{State Machine of the Telit Modem}
    \label{fig:telit_state_machine}
\end{figure}

The nRF state machine is quite simple, after the wakeup event, the modem is switched on and required to connect. When it has connected to the eNodeB, it makes an MQTT connection request. If this fails it attemps to disconnect and reconnect. When the MQTT connection is confirmed, it runs through its \texttt{QVector} of subscriptions to subscribe to each one. This vector is filled by the factory when building the elements. When that is over we enter the \texttt{ST\_NRF\_RUNNING} phase. In this state a timer is launched. When the timeout occurs, the nRF modem will run through all of the messages it has received and all of the messages it needs to transmit, handling them accordingly. Once this is done it returns to the running state. 

The Telit modem state machine looks more complicated, but it is in fact a lot more linear. When a wakeup is received, it is switched on with the hardware package. The Stun procedure is launched. When the Telit receives and parses its public address and public port, it passes them to the nRF modem (which will transmit it when it reaches the \texttt{ST\_NFR\_MQTT\_HANDLE} state). This forces the Central's Application to make the stun request and send it over MQTT also. The Telit modem then attempts to hole punch through the NATs. The Central PC will do the same. If they receive each others messages, they will be acknowledged and the real data communication can start. Once this is done the Telit modem will shutdown again and wait for another wakeup call. In this instance ``done" just means that the full message has been transmitted. 

\clearpage
\newpage
\subsection{Hardware}

Initially the firmware of the extension board was also meant to perform the power measurements, so this software was written with this in mind. Due to the issues with hardware (see section~\ref{hardware_trouble}), we opted to use an external measuring device: Digilent Analog Discovery 2, more on this in section~\ref{measurements_chapchap}.

The class diagram for the hardware package is based on a Model-View-Control pattern. The \texttt{PowerControl} establishes the serial link with the embedded power measurement device.

It is also in charge of switching off and on the modems, via this serial link. The \texttt{PowerData} class stores all of the samples and performs the necessary transformations on them. 

The \texttt{PowerGui}, graphs the energy of the two modems on the fly. It is connected with a timer so we can control  its refresh rate.

The \texttt{EnergyCalculator} basically just contains two values which are accumulators. These values are a sum of all the energy samples. When a power value is received, the energy transmitter calculates the energy using the sample time and then it adds it to the accumulators in order to integrate the value. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/hardware_class_compressed.pdf}
    \caption{Class View of the Hardware Package}
    \label{fig:hardware_class}
\end{figure}

\subsection{Stun}

Before the USB was functional on the Telit modem extension board, we had to use an Arduino to interface with this modem via UART (see section~\ref{hardware_trouble}). This meant all of the messags were sent through the Telit modem via COM port. In entering data mode, we had to construct the STUN requests and parse the STUN responses. For this, a whole package was developed for handling STUN messages. However, since the Telit functions as a network interface this task is greatly simplified. 

I simply downloaded a STUN client from this source: \url{https://sourceforge.net/projects/stun/}. Now we can just call this system process when making the request:

\begin{minted}{console}
stunclient.exe stun.solnet.ch 3478 --localaddr <telit IP address>
\end{minted}

Where \texttt{<telit IP address>} is the IP address found by performing the Network Interface setup in section~\ref{ncm_chap}.

\subsection{Multimedia}

To test the streaming service provided by the demo in this version of the project, I implemented a multimedia package. This only contains one class, its purpose is to connect to the microphone of the PC and stream its audio data through the Telit modem, which works. The class diagram is in figure~\ref{fig:mult_class}. If the Central PC launches a stream, the modem switching will occur. After the hole punching sequence, the idea is that the Telit modem signals that it is ready via the \texttt{udpSocketReadyForMultimedia()} method. The \texttt{MyAudioInputClass} receives this and there is a handover to be done. The Telit modem hands over its serial port interface. Over which the Audio class will stream the data. So the Telit socket is open for when the audio data arrives. After a timeout or a stop from the Central PC side, the audio interface will emit the \texttt{multimediaFinished} signal and perform a ``hand-back" with the modem's serial port. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{figures/multimedia_class.pdf}
    \caption{Class Diagram of Multimedia Package}
    \label{fig:mult_class}
\end{figure}

\subsection{Test}

The purpose of the Test package is to perform instrumentation of the library. There is only one class: \texttt{ModemTest}. If testing is not activated then this package does nothing. If testing is active though, the Arbiter is deactivated. Once this is the case the \texttt{ModemTest} class is in charge of switching on both modems. Then it will send a series of messages of increasing sizes over the network to allow power measurements to be made. This will allow us to establish the energy cost for different message sizes for both modems. There is more information on this in section~\ref{instrumentation}.

In figure~\ref{fig:test_class}, we can see that the Test package is not very stable. It depends on a lot of elements from other packages. This is not actually too much of a problem because as stated the Test package will not be active in releases of this protocol. The class \texttt{ModemTest} is linked to both modems so that it can directly transmit messages with them. The association is bidirectional so that the modems can both signal when the message has finished sending in order to log the power measurement times. 

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/testclass.pdf}
    \caption{Partial Class Diagram for Test Package}
    \label{fig:test_class}
\end{figure}
